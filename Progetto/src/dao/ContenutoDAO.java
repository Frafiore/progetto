package dao;

import java.util.ArrayList;
import java.util.Iterator;

import DbInterface.DbConnection;
import model.Contenuto;

public class ContenutoDAO {
	
	private static ContenutoDAO instance;

	//SINGLETON
	public static synchronized ContenutoDAO getInstance(){	
		if(instance == null)
			instance = new ContenutoDAO();
		return instance;
	}
	public ArrayList<Contenuto> visualizzaContenutoOrdini(int idOrdine){

		String query = "SELECT a.idArticoli ,p.Nome, c.Quantita , c.Evaso, a.Magazzino,"
				+ " c.Prezzo FROM ModelloER_mod.Articolo a, ModelloER_mod.Ordine o, ModelloER_mod.Contiene c,"
				+ " ModelloER_mod.Prodotto p WHERE c.idOrdine = o.idOrdine AND "
				+ "c.idArticoli = a.idArticoli AND a.Prodotto = p.idProdotto AND c.idOrdine = "+idOrdine+";";

		ArrayList<Contenuto> contenuto = new ArrayList<Contenuto>();

		Contenuto cont;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			cont = new Contenuto(Integer.parseInt(riga[0]), riga[1], Integer.parseInt(riga[2]), Integer.parseInt(riga[3]), Integer.parseInt(riga[4]), Float.parseFloat(riga[5]));
			contenuto.add(cont);
		}
		return contenuto;
	}

}
