package dao;

import java.util.ArrayList;

import java.util.Iterator;

import java.util.Vector;

import DbInterface.DbConnection;
import business.MD5;
import business.Sessione;
import business.UtenteBusiness;
import model.Ordine;
import model.Progetto;
import model.Utente;

public class UtenteDAO {

	private static UtenteDAO instance;

	public static synchronized UtenteDAO getInstance(){

		if(instance==null)
		{	
			instance = new UtenteDAO();
		}
		return instance;
	}

	public boolean userExists(Utente u)
	{
		int codiceUtente = u.getCodice_utente();
		String password = u.getPassword();

		String pwdMD5 = MD5.getInstance().getMD5(password); 

		//SINGLETON
		ArrayList<String[]> result=DbConnection.getInstance().eseguiQuery("SELECT * FROM ModelloER_mod.Utente WHERE codiceUtente = '" + codiceUtente+"' AND password = '"+pwdMD5+"';");

		if(result.size()!=0)
			return(true);
		else
			return(false);
	}

	public String[] getUtenteLoggato(int codice_utente, String pwd){
		int codiceUtente = codice_utente;
		String password = pwd;

		String pwdMD5 = MD5.getInstance().getMD5(password); 

		//SINGLETON
		ArrayList<String[]> result=DbConnection.getInstance().eseguiQuery("SELECT * FROM ModelloER_mod.Utente WHERE codiceUtente = '" + codiceUtente+"' AND password = '"+pwdMD5+"';");

		Iterator<String[]> i = result.iterator();

		return i.next();
	}

	public String getEmailFromDB(Utente u){

		int codiceUtente = u.getCodice_utente();

		String query ="SELECT email FROM ModelloER_mod.Utente WHERE codiceUtente = '" + codiceUtente+"';" ;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		try {
			String email = i.next()[0];
			return (email);
		} catch (Exception e) {
			// TODO: handle exception

			return null;
		}

	}
	public boolean verificaCodiceUtente(Utente u){
		int codiceUtente = u.getCodice_utente();

		String query ="SELECT * FROM ModelloER_mod.Utente WHERE codiceUtente = '" + codiceUtente+"';" ;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		if(result.size()!=0)
			return(true);
		else
			return(false);
	}

	public boolean cambiaPassword(Utente u, String newPassword, int idUtente){

		String pwdMD5 = MD5.getInstance().getMD5(newPassword);

		int codiceUtente = u.getCodice_utente();

		String query ="UPDATE ModelloER_mod.Utente SET Password = '"+pwdMD5+"' WHERE idUtente='"+idUtente+"';" ;

		boolean result = DbConnection.getInstance().eseguiAggiornamento(query);

		return result;
	}

	public int getIdFromDB(Utente u){

		int codiceUtente = u.getCodice_utente();
		String query ="SELECT idUtente FROM ModelloER_mod.Utente WHERE CodiceUtente = '" + codiceUtente+"';" ;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		String idUtente = i.next()[0];
		return Integer.parseInt(idUtente);
	}

	public String getSede(int idUtente){

		String query ="SELECT Paese FROM ModelloER_mod.Sede s, ModelloER_mod.Utente u WHERE u.Sede = s.idSede AND u.idUtente = '"+idUtente+"';" ;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		String Sede = i.next()[0];
		return Sede;
	}
	public int getIdMagazzino(Utente u){

		int codiceUtente = u.getCodice_utente();
		String query ="SELECT idMagazzino FROM ModelloER_mod.Magazzino m, ModelloER_mod.Sede s, ModelloER_mod.Utente u WHERE u.Sede = s.idSede AND m.Sede = s.idSede AND u.codiceUtente='"+codiceUtente+"';";
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		String idMagazzino = i.next()[0];
		return Integer.parseInt(idMagazzino);
	}
	public Utente caricaUtente(Utente ut){
		int idUtente = ut.getIdUtente();
		String query ="SELECT * FROM ModelloER_mod.Utente WHERE idUtente = "+ idUtente+";" ;
		
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		Utente u = new Utente(i.next());
		return u;
	}
	public ArrayList<Utente> caricaElencoDipendenti(int idCapo){
		String query = "SELECT DISTINCT(o.Dipendente) FROM modelloer_mod.ordine o, modelloer_mod.progetto p , modelloer_mod.lavora l, modelloer_mod.utente u WHERE o.Progetto=p.idProgetto AND l.idProgetto=p.idProgetto AND l.idUtente=u.idUtente AND u.idUtente = "+idCapo;

		ArrayList<Utente> utenti = new ArrayList<Utente>();

		Utente u;
		Utente dip;
		
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();

			u = new Utente(Integer.parseInt(riga[0]),5,false);
			dip = u.caricaDatiUtente();
			
			utenti.add(dip);
		}
		return utenti;
	}
	public boolean lavoraAlProgetto(Utente u, int idProgetto){
		
		String query = "SELECT u.* FROM modelloer_mod.utente u, modelloer_mod.lavora l, modelloer_mod.progetto p WHERE l.idProgetto = p.idProgetto AND l.idUtente = u.idUtente AND u.idUtente = "+u.getIdUtente()+" AND p.idProgetto = "+idProgetto+"";
		
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		
		if(result.size()!=0)
			return(true);
		else
			return(false);
		
	}
}
