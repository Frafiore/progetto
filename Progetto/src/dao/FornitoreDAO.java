package dao;

import java.util.ArrayList;
import java.util.Iterator;

import DbInterface.DbConnection;
import model.Fornitore;
import model.Magazzino;
import model.Utente;

public class FornitoreDAO {

	private static FornitoreDAO instance;

	//SINGLETON
	public static synchronized FornitoreDAO getInstance(){	
		if(instance == null)
			instance = new FornitoreDAO();
		return instance;
	}
	

	public ArrayList<Fornitore> caricaElencoFornitori(){

		String query = "SELECT f.idFornitore, f.Nome,f.Cognome,f.Email FROM ModelloER_mod.Fornitore f;";

		ArrayList<Fornitore> fornitore = new ArrayList<Fornitore>();

		Fornitore forn;

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		
		for(Iterator<String[]> iterator = result.iterator(); iterator.hasNext();){
			String[] riga = (String[]) iterator.next();
			forn = new Fornitore(Integer.parseInt(riga[0]), riga[1], riga[2], null);
			fornitore.add(forn);
		}
				
		return fornitore;
	}
	
	public Fornitore caricaFornitore(int idAricolo){
		
		String query = "SELECT d.* FROM modelloer_mod.articolo a , modelloer_mod.prodotto p , modelloer_mod.fornisce f, modelloer_mod.fornitore d "
				+ "WHERE a.Prodotto = p.idProdotto "
				+ "AND f.Fornitore = d.idFornitore "
				+ "AND f.Prodotto = p.idProdotto "
				+ "AND a.idArticoli = '"+idAricolo+"'";
		
		ArrayList<Fornitore> fornitore = new ArrayList<>();
		Fornitore forn;
		
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		for(Iterator<String[]> iterator = result.iterator(); iterator.hasNext();){
			String[] riga = (String[]) iterator.next();
			forn = new Fornitore(Integer.parseInt(riga[0]),riga[1], riga[2], riga[3]);
			fornitore.add(forn);
		}
		
		forn = fornitore.get(0);
		String nome = forn.getNome();
		String cognome = forn.getCognome();
		String email = forn.getEmail();
		
		return new Fornitore(idAricolo, nome, cognome, email);		
		
	}
}