package dao;

import java.util.ArrayList;
import java.util.Iterator;

import DbInterface.DbConnection;
import model.Articolo;
import model.Categoria;
import model.Produttore;

public class ProduttoreDAO {

	private static ProduttoreDAO instance;

	//SINGLETON
	public static synchronized ProduttoreDAO getInstance(){	
		if(instance == null)
			instance = new ProduttoreDAO();
		return instance;
	}

	public ArrayList<Produttore> caricaProduttore(){

		String query = "SELECT * FROM ModelloER_mod.Produttore;";

		ArrayList<Produttore> produttore = new ArrayList<Produttore>();

		Produttore prod;

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			prod = new Produttore(Integer.parseInt(riga[0]),riga[1],riga[2]);
			produttore.add(prod);
		}
		return produttore;
	}
	public Produttore prodottoDa(int idArt){
		String query = "SELECT p1.* FROM modelloer_mod.produttore p1 , modelloer_mod.prodotto p2, modelloer_mod.articolo a WHERE p1.idProduttore = p2.Produttore AND a.Prodotto = p2.idProdotto AND a.idArticoli = "+idArt+";";


		Produttore prod = new Produttore();

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			prod = new Produttore(Integer.parseInt(riga[0]),riga[1],riga[2]);
		}
		return prod;
		
	}
}