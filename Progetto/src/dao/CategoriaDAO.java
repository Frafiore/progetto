package dao;

import java.util.ArrayList;
import java.util.Iterator;

import DbInterface.DbConnection;
import model.Articolo;
import model.Categoria;

public class CategoriaDAO {

	private static CategoriaDAO instance;

	//SINGLETON
	public static synchronized CategoriaDAO getInstance(){	
		if(instance == null)
			instance = new CategoriaDAO();
		return instance;
	}

	public ArrayList<Categoria> caricaCategorie(){

		String query = "SELECT * FROM ModelloER_mod.categoria";

		ArrayList<Categoria> categorie = new ArrayList<Categoria>();

		Categoria cat;

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			cat = new Categoria(riga[0], Integer.parseInt(riga[1]));
			categorie.add(cat);
		}
		return categorie;
	}
	public int caricaIdCategorie(String nomeCategoria){

		String query = "SELECT idCategoria FROM ModelloER_mod.categoria WHERE Nome ='"+nomeCategoria+"';";

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();
		
		int idCat = Integer.parseInt(i.next()[0]);
		return idCat;
	}
}
