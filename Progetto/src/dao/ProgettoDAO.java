package dao;

import java.util.ArrayList;
import java.util.Iterator;

import DbInterface.DbConnection;
import model.Progetto;

public class ProgettoDAO {
	
	private static ProgettoDAO instance;

	//SINGLETON
	public static synchronized 	ProgettoDAO getInstance(){	
		if(instance == null)
			instance = new 	ProgettoDAO();
		return instance;
	}
	public ArrayList<Progetto> elencoProgetti(int idUtente){

		ArrayList<Progetto> elenco = new ArrayList<>();

		String query = "SELECT p.* FROM ModelloER_mod.Lavora l, ModelloER_mod.Progetto p, ModelloER_mod.Utente u WHERE l.idProgetto = p.idProgetto AND l.idUtente = u.idUtente AND u.idUtente = "+idUtente+" ORDER BY p.Nome;"; 

		Progetto prog;

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			prog = new Progetto (Integer.parseInt(riga[0]),riga[1], Float.parseFloat(riga[2]), Integer.parseInt(riga[3]));
			elenco.add(prog);
		}

		return elenco;

	}

	public int caricaIdProgetto(String progetto){

		String query = "SELECT p.idProgetto FROM ModelloER_mod.Progetto p WHERE p.Nome = '"+progetto+"';"; 

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		int idProg = Integer.parseInt(i.next()[0]);

		return idProg;
	}
	public float caricaBudgetProgetto(int progetto){

		ArrayList<Progetto> elenco = new ArrayList<>();

		String query = "SELECT p.Budget FROM ModelloER_mod.Progetto p WHERE p.idProgetto = '"+progetto+"';"; 

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		float budget = Integer.parseInt(i.next()[0]);

		return budget;
	}
	public float caricaBudgetRimanente(int progetto){

		ArrayList<Progetto> elenco = new ArrayList<>();

		String query = "SELECT Costo_totale FROM modelloer_mod.ordine WHERE Progetto="+progetto+";"; 

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		float budget = caricaBudgetProgetto(progetto);
		
		float budgetSpeso =0;
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			
			budgetSpeso+=Float.parseFloat((String)riga[0]);
			
		}
		float budgetRestante = budget - budgetSpeso;

		return budgetRestante;
	}
	public String caricaNomeProgetto(int idProgetto){

		ArrayList<Progetto> elenco = new ArrayList<>();

		String query = "SELECT p.Nome FROM ModelloER_mod.Progetto p WHERE p.idProgetto = '"+idProgetto+"';"; 

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		String idProg = i.next()[0];

		return idProg;
	}
	public float caricaBudgetSpesoDa(int progetto, int idUtente){

		ArrayList<Progetto> elenco = new ArrayList<>();

		String query = "SELECT Costo_totale FROM modelloer_mod.ordine WHERE Progetto="+progetto+" AND Dipendente = "+idUtente+";"; 

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		float budgetSpeso =0;
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			
			budgetSpeso+=Float.parseFloat((String)riga[0]);
			
		}
		return budgetSpeso;
	}
	public boolean updateProgetto(int idProgetto, String nome,String stato, String budget){
		String query = "UPDATE `modelloer_mod`.`progetto` SET `Nome`='"+nome+"', `Budget`='"+budget+"', `Stato`='"+stato+"' WHERE `idProgetto`='"+idProgetto+"';";
		boolean x = DbConnection.getInstance().eseguiAggiornamento(query);
		return x;
	}
}
