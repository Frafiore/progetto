package dao;

import java.util.ArrayList;
import java.util.Iterator;

import DbInterface.DbConnection;
import business.CatalogoBusiness;
import business.Sessione;
import model.Articolo;
import model.Contenuto;
import model.Ordine;
import model.Progetto;
import model.Utente;

public class OrdineDAO {


	private static OrdineDAO instance;

	//SINGLETON
	public static synchronized OrdineDAO getInstance(){	
		if(instance == null)
			instance = new OrdineDAO();
		return instance;
	}

	public ArrayList<Ordine> caricaElencoOrdini(){

		String query = "SELECT o.*, u.Nome, u.Cognome FROM ModelloER_mod.Utente u, ModelloER_mod.Ordine o WHERE o.Dipendente = u.idUtente AND o.Dipendente = u.idUtente;";

		ArrayList<Ordine> ordini = new ArrayList<Ordine>();

		Ordine ord;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();


			ord = new Ordine(Integer.parseInt(riga[0]), Float.parseFloat(riga[1]), riga[2], riga[3], Integer.parseInt(riga[4]), riga[9]+" "+riga[10], Integer.parseInt(riga[6]), Float.parseFloat(riga[7]), riga[8],Integer.parseInt(riga[5]));

			ordini.add(ord);
		}
		return ordini;
	}
	public ArrayList<Ordine> caricaElencoOrdiniPerCapoProgetto(int idCapo){

		String query = "SELECT o.* FROM modelloer_mod.ordine o, modelloer_mod.progetto p , modelloer_mod.lavora l, modelloer_mod.utente u WHERE o.Progetto=p.idProgetto AND l.idProgetto=p.idProgetto AND l.idUtente=u.idUtente AND u.idUtente = "+idCapo+";";

		ArrayList<Ordine> ordini = new ArrayList<Ordine>();

		Ordine ord;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();

			Utente u = new Utente(Integer.parseInt(riga[5]),5,false);
			Utente dip = u.caricaDatiUtente();
			ord = new Ordine(Integer.parseInt(riga[0]), Float.parseFloat(riga[1]), riga[2], riga[3], Integer.parseInt(riga[4]), dip.getNome()+" "+dip.getCognome(), Integer.parseInt(riga[6]), Float.parseFloat(riga[7]), riga[8],Integer.parseInt(riga[5]));

			ordini.add(ord);
		}
		return ordini;
	}

	public int caricaIdOrdine(String progetto){

		ArrayList<Progetto> elenco = new ArrayList<>();

		String query = "SELECT p.idProgetto FROM ModelloER_mod.Progetto p WHERE p.Nome = '"+progetto+"';"; 

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();

		int idProg = Integer.parseInt(i.next()[0]);

		return idProg;
	}
	public boolean inserisciOrdine(Ordine ordine, ArrayList<Contenuto> cont){


		String idOrd = Integer.toString(ordine.getIdOrdine());

		String prezzo = Float.toString(ordine.getPrezzo());
		String spedizione = Float.toString(ordine.getSpedizione());
		String data = ordine.getData();

		String evasione;

		if(ordine.isEvasione())
			evasione="1";
		else
			evasione ="0";

		String dipendente = Integer.toString(ordine.getIdDip());
		String progetto = Integer.toString(ordine.getProgetto());

		String totale = Float.toString(ordine.getCosto_totale());

		String note = ordine.getNote();

		String query = "INSERT INTO ModelloER_mod.Ordine (`Prezzo`, `Spedizione`, `Data`, `Evasione`, `Dipendente`, `Progetto`, `Costo_totale`, `Note`) VALUES ('"+prezzo+"', '"+spedizione+"', '"+data+"', '"+evasione+"', '"+dipendente+"', '"+progetto+"', '"+totale+"', '"+note+"');"; 

		boolean result = DbConnection.getInstance().eseguiAggiornamento(query);

		String idArticolo;
		String evaso;
		String quantita;
		String query2;
		String query3;
		String prezzoPagato;
		boolean result2 = false;
		boolean result3 = false;


		for (Iterator<Contenuto> i = cont.iterator(); i.hasNext();) {
			Contenuto contenuto = (Contenuto) i.next();

			idArticolo = Integer.toString(contenuto.getIdArticolo());


			if(contenuto.isEvaso())
				evaso = "1";
			else 
				evaso = "0";

			quantita = Integer.toString(contenuto.getQuantita());
			prezzoPagato = Float.toString(contenuto.getPrezzo());

			query2 = "INSERT INTO `ModelloER_mod`.`Contiene` (`idArticoli`, `idOrdine`, `Quantita`, `Evaso`, `Prezzo`) VALUES ('"+idArticolo+"', '"+idOrd+"', '"+quantita+"', '"+evaso+"', '"+prezzoPagato+"');";

			result2 = DbConnection.getInstance().eseguiAggiornamento(query2);

			query3 = "UPDATE modelloer_mod.articolo SET Quantita = Quantita - "+quantita+" WHERE idArticoli = "+idArticolo+";";

			result3 = DbConnection.getInstance().eseguiAggiornamento(query3);

			if(result2==false || result3 == false)
				break;

		}
		return (result2 && result3);
	}

	public boolean evadiOrdine(int idOrdine, int idMagazzino) {

		String query6 = "UPDATE ModelloER_mod.Contiene c, ModelloER_mod.Articolo a SET c.Evaso=1 WHERE a.idArticoli = c.idArticoli AND c.idOrdine="+idOrdine+" AND a.Magazzino="+idMagazzino+";";
		boolean result6 = DbConnection.getInstance().eseguiAggiornamento(query6);

		ArrayList<Contenuto> dati = ContenutoDAO.getInstance().visualizzaContenutoOrdini(idOrdine);

		int k=0;
		for (int i=0; i<dati.size(); i++) {			
			if(dati.get(i).isEvaso()==false){
				k++; break;
			}
		}
		if(k==0){
			String query5 = "UPDATE `ModelloER_mod`.`Ordine` SET `Evasione`='1' WHERE `idOrdine`='"+idOrdine+"';";
			boolean result5 = DbConnection.getInstance().eseguiAggiornamento(query5);

			return (result5);
		}


		return (result6);	
	}

	public ArrayList<Ordine> cercaData(String data){

		String query = "SELECT o.*, u.Nome, u.Cognome FROM ModelloER_mod.Utente u, ModelloER_mod.Ordine o WHERE o.Dipendente = u.idUtente AND o.Data ='"+data+"';";

		ArrayList<Ordine> ordini = new ArrayList<Ordine>();

		Ordine ord;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();

			ord = new Ordine(Integer.parseInt(riga[0]), Float.parseFloat(riga[1]), riga[2], riga[3], Integer.parseInt(riga[4]), riga[9]+" "+riga[10], Integer.parseInt(riga[6]), Float.parseFloat(riga[7]), riga[8], Integer.parseInt(riga[5]));

			ordini.add(ord);
		}
		return ordini;
	}
	public boolean modificaNota(String nuovaNota,int idOrdine){
		String query = "UPDATE `modelloer_mod`.`ordine` SET `Note`='"+nuovaNota+"' WHERE `idOrdine`='"+idOrdine+"';";
		return DbConnection.getInstance().eseguiAggiornamento(query);
	}
	public int controlloSuQuantita(int idArticolo, String data){

		String query = "SELECT SUM(c.Quantita) FROM modelloer_mod.ordine o, modelloer_mod.contiene c, modelloer_mod.articolo a WHERE c.idArticoli = a.idArticoli AND c.idOrdine = o.idOrdine AND a.idArticoli = "+idArticolo+" AND o.Data = '"+data+"'";

		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

//		Iterator<String[]> i = result.iterator();
//		
//		int quantita =0;
//		
//		if(i.next()[0] != null )
//			 quantita= Integer.parseInt(i.next()[0]);
//
//		return quantita;
		int quantita =0;
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] s = (String[]) iterator.next();
			
			if(s[0] != null)
				quantita= Integer.parseInt(s[0]);
			
		}
		return quantita;

	}

}
