package dao;

import java.util.ArrayList;
import java.util.Iterator;

import DbInterface.DbConnection;
import model.Magazzino;

public class MagazzinoDAO {

	private static MagazzinoDAO instance;
	
	//SINGLETON
	public static synchronized MagazzinoDAO getInstance(){	
		if(instance == null)
			instance = new MagazzinoDAO();
		return instance;
	}
	
	public int caricaIdMagazzino(String nomeMag){
		
		String query = "SELECT idMagazzino FROM ModelloER_mod.Magazzino m WHERE m.nome = '"+nomeMag+"';";
		
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);

		Iterator<String[]> i = result.iterator();
		
		int idMagazzino = Integer.parseInt(i.next()[0]);
		return idMagazzino;
	}
	
	public ArrayList<Magazzino> caricaMagazzini(){
		
		String query = "SELECT idMagazzino, Nome, s.Latitudine,s.Longitudine FROM ModelloER_mod.Magazzino m, ModelloER_mod.Sede s WHERE m.Sede = s.idSede;"; 
		
		ArrayList<Magazzino> magazzini = new ArrayList<>();
		
		Magazzino mag;
		
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			mag = new Magazzino (Integer.parseInt(riga[0]),riga[1], Float.parseFloat(riga[2]), Float.parseFloat(riga[3]));
			magazzini.add(mag);
		}
		
		return magazzini;
	}

	public Magazzino caricaDatiMagazzino(int idMag){
		
		String query = "SELECT idMagazzino, Nome, s.Latitudine,s.Longitudine FROM ModelloER_mod.Magazzino m, ModelloER_mod.Sede s WHERE m.Sede = s.idSede AND m.idMagazzino="+idMag+";"; 
		
		ArrayList<Magazzino> magazzini = new ArrayList<>();
		
		Magazzino mag ;
		
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			mag = new Magazzino (Integer.parseInt(riga[0]),riga[1], Float.parseFloat(riga[2]), Float.parseFloat(riga[3]));
			magazzini.add(mag);
		}
		
		mag = magazzini.get(0);
		int id= mag.getIdmag();
		String nome = mag.getNome();
		float lat=mag.getLat();
		float longi = mag.getLongit();
		
		return new Magazzino(id, nome, lat, longi);
	}

}
