package dao;

import java.util.ArrayList;
import java.util.Iterator;
import DbInterface.DbConnection;
import model.Articolo;

public class ArticoloProdottoDAO {

	private static ArticoloProdottoDAO instance;
	
	//SINGLETON
	public static synchronized ArticoloProdottoDAO getInstance(){	
		if(instance == null)
			instance = new ArticoloProdottoDAO();
		return instance;
	}
	
	public boolean caricaNuovo(int idMagazzino, Articolo a,int idForn){
		
		String query1 = "INSERT INTO `ModelloER_mod`.`Prodotto` (`Nome`, `Categoria`, `Produttore`) "
				+ "VALUES ('"+a.getNome()+"', '"+a.getCategoria()+"', '"+a.getIdProduttore()+"');";

		boolean x1= DbConnection.getInstance().eseguiAggiornamento(query1);
		
		String query2 = "SELECT idProdotto FROM ModelloER_mod.Prodotto WHERE nome='"+a.getNome()+"';";

		ArrayList<String[]> res = DbConnection.getInstance().eseguiQuery(query2);
		
		int idProdotto=0;
		
		for (Iterator iterator = res.iterator(); iterator.hasNext();) {
			String[] s = (String[]) iterator.next();
			idProdotto=Integer.parseInt(s[0]);
		}
		
		String query3 = "INSERT INTO `ModelloER_mod`.`Articolo` (`Descrizione`, `Prezzo`, `Quantita_max_ordinabile`,`Quantita`,`Magazzino`,`Prodotto`) "
				+ "VALUES ('"+a.getDescrizione()+"', '"+a.getPrezzo()+"', '"+a.getQuantita_max_ordi()+"', '"+a.getQuantita()+"', '"+idMagazzino+"', '"+idProdotto+"');";
		
		String query4 = "INSERT INTO `modelloer_mod`.`fornisce` (`Prodotto`, `Fornitore`) VALUES ('"+idProdotto+"', '"+idForn+"');";
		
		DbConnection.getInstance().eseguiAggiornamento(query4);
		boolean x2= DbConnection.getInstance().eseguiAggiornamento(query3);
		
		return x1 && x2;
				
	}
}