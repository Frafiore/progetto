package dao;

import java.util.ArrayList;
import java.util.Iterator;
import DbInterface.DbConnection;
import model.Articolo;
import model.Magazzino;
import model.Progetto;

public class CatalogoDAO {

	private static CatalogoDAO instance;
	
	//SINGLETON
	public static synchronized CatalogoDAO getInstance(){	
		if(instance == null)
			instance = new CatalogoDAO();
		return instance;
	}
	
	public ArrayList<Articolo> caricaRifornimento(int idMagazzino){
		
		String query = "SELECT p.Nome, a.Descrizione, a.Prezzo, a.Quantita, a.Quantita_max_ordinabile, a.Prodotto, p.Categoria , p.Produttore , a.idArticoli FROM ModelloER_mod.Articolo a , ModelloER_mod.Prodotto p WHERE a.Prodotto = p.idProdotto AND a.Magazzino = "+ idMagazzino +" ORDER BY a.Quantita ;";

		ArrayList<Articolo> articoli = new ArrayList<Articolo>();

		Articolo art;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			art = new Articolo (riga[0],riga[1],Float.parseFloat(riga[2]),Integer.parseInt(riga[3]), Integer.parseInt(riga[4]), Integer.parseInt(riga[6]), Integer.parseInt(riga[8]),Integer.parseInt(riga[5]), Integer.parseInt(riga[7]));
			articoli.add(art);
		}
		return articoli;
	}
	
	public boolean aggiornaRifornimento(Articolo a){
		
		String query = "UPDATE `ModelloER_mod`.`Articolo` "
				+ "SET `Descrizione`='"+a.getDescrizione()+"',`Prezzo`='"+a.getPrezzo()+"',`Quantita`='"+a.getQuantita()+"',`Quantita_max_ordinabile`='"+a.getQuantita_max_ordi()+"'"
				+ "WHERE `Prodotto`='"+a.getIdProdotto()+"';";

		String query2 = "UPDATE `ModelloER_mod`.`Prodotto` SET `Nome`='"+a.getNome()+"' WHERE `idProdotto`='"+a.getIdProdotto()+"';";
		
		boolean x= DbConnection.getInstance().eseguiAggiornamento(query);
		boolean x1= DbConnection.getInstance().eseguiAggiornamento(query2);
		
		
		return x && x1;
	}
	
	@SuppressWarnings("rawtypes")
	public ArrayList<Articolo> caricaDatiCatalogo(int idMagazzino){
	
		String query = "SELECT Nome, Descrizione, Prezzo, Quantita, Quantita_max_ordinabile, Prodotto , p.Categoria , a.idArticoli FROM ModelloER_mod.Articolo a , ModelloER_mod.Prodotto p WHERE a.Prodotto = p.idProdotto AND a.Magazzino = "+ idMagazzino +";";
	
		ArrayList<Articolo> articoli = new ArrayList<Articolo>();

		Articolo art;
		ArrayList<String[]> result = DbConnection.getInstance().eseguiQuery(query);
		
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			String[] riga = (String[]) iterator.next();
			art = new Articolo (riga[0],riga[1],Float.parseFloat(riga[2]),Integer.parseInt(riga[3]), Integer.parseInt(riga[4]), Integer.parseInt(riga[6]), Integer.parseInt(riga[7]),Integer.parseInt(riga[5]), 0);
			articoli.add(art);
		}
		return articoli;
	}

	
}
