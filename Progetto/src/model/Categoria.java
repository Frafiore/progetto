package model;

import java.util.ArrayList;

import dao.CategoriaDAO;

public class Categoria {
	private int idCat;
	private String nome;
	
	public String getNome() {
		return nome;
	}
	public Categoria(String nome, int idCat){
		this.nome = nome;
		this.idCat = idCat;
	}
	public Categoria(){}
	
	public ArrayList<Categoria> getCategorie(){
		return CategoriaDAO.getInstance().caricaCategorie();
	}
	public int getIdCategoria(String nomeCategoria){
		return CategoriaDAO.getInstance().caricaIdCategorie(nomeCategoria);
	}
	public int getIdCat() {
		return idCat;
	}

}
