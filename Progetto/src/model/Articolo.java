package model;

import java.util.ArrayList;

import dao.ArticoloProdottoDAO;
import dao.CatalogoDAO;
import dao.OrdineDAO;

public class Articolo {
	
	private int idArticolo;
	private String nome;
	private String descrizione;
	private float prezzo;
	private int quantita;
	private int quantita_max_ordi;
	private int categoria;
	private int idProdotto;
	private int idProduttore;
	

	public Articolo(String nome, String descrizione, float prezzo, int quantita,int quantita_max_ordi,int categoria,int idArt, int idPro, int idProd){
		this.nome=nome;
		this.descrizione=descrizione;
		this.prezzo=prezzo;
		this.quantita=quantita;
		this.quantita_max_ordi=quantita_max_ordi;
		this.categoria=categoria;
		this.idArticolo = idArt;
		this.idProdotto = idPro;
		this.idProduttore = idProd;
		
	}

	public Articolo() {
		// TODO Auto-generated constructor stub
	}

	public String getNome() {
		return nome;
	}
	
	public int getIdProdotto() {
		return idProdotto;
	}
	
	public int getIdProduttore() {
		return idProduttore;
	}
	
	public String getDescrizione() {
		return descrizione;
	}

	public float getPrezzo() {
		return prezzo;
	}
	public int getQuantita_max_ordi() {
		return quantita_max_ordi;
	}

	public int getCategoria() {
		return categoria;
	}

	public int getIdArticolo() {
		return idArticolo;
	}

	public int getQuantita() {
		return quantita;
	}

	public boolean aggiornaRifornimento(Articolo a) {
		return CatalogoDAO.getInstance().aggiornaRifornimento(a);
	}
	
	public boolean caricaNuovo(int idMagazzino,Articolo a,int idForn) {
		return ArticoloProdottoDAO.getInstance().caricaNuovo(idMagazzino, a,idForn);
	}
}
