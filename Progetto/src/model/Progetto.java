package model;

import java.util.ArrayList;

import dao.ProgettoDAO;

public class Progetto {
	
	private int idProgetto;
	private String nome;
	private float budget;
	private boolean attivo;
	
	public Progetto(int id, String nome, float budjet, int stato){
		this.idProgetto = id;
		this.nome = nome;
		this.budget = budjet;
		
		if(stato==1)
			this.attivo = true;
		else
			this.attivo = false;
		
	}

	public Progetto() {}

	public int getIdProgetto() {
		return idProgetto;
	}

	public String getNome() {
		return nome;
	}

	public float getBudget() {
		return budget;
	}

	public boolean isAttivo() {
		return attivo;
	}
	public ArrayList<Progetto> elencoProgetti(int idUtente){
		return ProgettoDAO.getInstance().elencoProgetti(idUtente);
	}
	public int caricaIdProgetto(String progetto){
		return ProgettoDAO.getInstance().caricaIdProgetto(progetto);
	}
	public String caricaNomeProgetto(int idprogetto){
		return ProgettoDAO.getInstance().caricaNomeProgetto(idprogetto);
	}
	public float caricaBudgetProgetto(int progetto){
		return ProgettoDAO.getInstance().caricaBudgetProgetto(progetto);
	}
	public float caricaBudgetDisponibile(int progetto){
		return ProgettoDAO.getInstance().caricaBudgetRimanente(progetto);
	}
	public float caricaBudgetSpesoDa(int progetto, int idUtente){
		return ProgettoDAO.getInstance().caricaBudgetSpesoDa(progetto, idUtente);
	}
	public boolean updateProgetto(int idProgetto, String nome,String stato, String budget){
		return ProgettoDAO.getInstance().updateProgetto(idProgetto, nome, stato, budget);
	}
	
}
