package model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.mysql.fabric.xmlrpc.base.Array;

import dao.ContenutoDAO;
import dao.OrdineDAO;

public class Ordine {

	private int idOrdine;
	private float prezzo;
	private float spedizione;
	private String data;
	private boolean evasione;
	private String dipendete;
	private int progetto;
	private float costo_totale;
	private String note;
	private int idDip;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	
	//	costruttore per carica da database per magazziniere
	public Ordine(int idOrdine, float prezzo, String spedizione, String data, int evasione, String dipendete, int progetto, float costo_totale, String note, int idDip) {

		this.idOrdine = idOrdine;
		this.prezzo = prezzo;

		if(spedizione==null)
			this.spedizione = 0;
		else 
			this.spedizione = Float.parseFloat(spedizione);	

		this.data = data;
		if(evasione==1)
			this.evasione= true;
		else
			this.evasione= false;
		this.dipendete = dipendete;
		this.progetto = progetto;
		this.costo_totale = costo_totale;
		this.note = note;
		this.idDip = idDip;
	}
	//	costruttore per inserimento ordine
	public Ordine(int idOrdine,float prezzo, float spedizione, int progetto, float costo_totale, String note, int idDip) {

		this.prezzo = prezzo;
		this.spedizione = spedizione;
		this.data =sdf.format(Calendar.getInstance().getTime());

		this.evasione = false;

		this.progetto = progetto;
		this.costo_totale = costo_totale;
		this.note = note;
		this.idDip = idDip;
		this.idOrdine = idOrdine;
	}
	//	costruttore per carica da database per dipendente perche non serve il nome
	public Ordine(int idOrdine,float prezzo, String spedizione,String data, int evasione ,int progetto, float costo_totale, String note) {

		this.prezzo = prezzo;
		if(spedizione==null)
			this.spedizione = 0;
		else 
			this.spedizione = Float.parseFloat(spedizione);
		this.data =data;

		if(evasione==0)
			this.evasione=false;
		else
			this.evasione=true;

		this.progetto = progetto;
		this.costo_totale = costo_totale;
		this.note = note;
		this.idOrdine = idOrdine;
	}

	public Ordine() {}

	public Ordine(int idOrdine2) {
		this.idOrdine = idOrdine2;
	}

	public int getIdOrdine() {
		return idOrdine;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public float getSpedizione() {
		return spedizione;
	}

	public String getData() {
		return data;
	}

	public boolean isEvasione() {
		return evasione;
	}

	public String getDipendete() {
		return dipendete;
	}

	public int getProgetto() {
		return progetto;
	}

	public float getCosto_totale() {
		return costo_totale;
	}

	public String getNote() {
		return note;
	}


	public int getIdDip() {
		return idDip;
	}


	public ArrayList<Ordine> caricaOrdini(){

		return OrdineDAO.getInstance().caricaElencoOrdini();

	}
	
	public ArrayList<Contenuto> caricaContenuto(){

		return ContenutoDAO.getInstance().visualizzaContenutoOrdini(idOrdine);

	}
	
	public boolean inserisciOrdine(ArrayList<Contenuto> cont){

		return OrdineDAO.getInstance().inserisciOrdine(this, cont);

	}

	public boolean evadiOrdine(int idOrdine, int idMagazzino){

		return OrdineDAO.getInstance().evadiOrdine(idOrdine, idMagazzino);

	}

	public ArrayList<Ordine> cercaData(String data){

		return OrdineDAO.getInstance().cercaData(data);

	}
	public boolean modificaNota(String nuovaNota, int idOrdine){
		return OrdineDAO.getInstance().modificaNota(nuovaNota, idOrdine);
	}
	public ArrayList<Ordine> caricaordiniPerCapoProgetto(int idCapo){
		return OrdineDAO.getInstance().caricaElencoOrdiniPerCapoProgetto(idCapo);
	}
	public int controlloSuQuantita(int idArticolo, String data){
		return OrdineDAO.getInstance().controlloSuQuantita(idArticolo, data);
	}
}
