package model;

import java.util.ArrayList;

import dao.FornitoreDAO;
import dao.OrdineDAO;;

public class Fornitore {
	
	int idFornitore;
	private String nome;
	private String cognome;
	private String email;
	
	public int getidFornitore() {
		return idFornitore;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getCognome(){
		return cognome;
	}
	
	public String getEmail() {
		return email;
	}
	
	public Fornitore(int idFornitore ,String nome, String cognome, String email){
		this.idFornitore = idFornitore;
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
	}
	
	public Fornitore(){}
	
	public Fornitore(String[] dati) {
		for(int i=0; i<dati.length; i++){
			if(dati[i]==null)
				dati[i] = "0";
		}
		this.nome = dati[0];
		this.cognome = dati[1];
		this.email = dati[2];

	}

	public ArrayList<Fornitore> getFornitore(){
		return FornitoreDAO.getInstance().caricaElencoFornitori();
	}
	
	public ArrayList<Fornitore> caricaFornitori(){
		return FornitoreDAO.getInstance().caricaElencoFornitori();
	}
	
	public Fornitore caricaFornitore(int idArticolo){
		return FornitoreDAO.getInstance().caricaFornitore(idArticolo);
	}

}
