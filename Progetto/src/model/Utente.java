package model;

import java.util.ArrayList;
import java.util.Iterator;

import dao.UtenteDAO;

public class  Utente {
	
	private int idUtente;
	private String nome;
	private String cognome;
	private String email;
	private String cod_fiscale;
	private String password;
	private int codice_utente;
	private String sede;	
	private String ruolo;		
	private int magazzino;
	
	public Utente(int codiceUtente, String password2){
		this.codice_utente = codiceUtente;
		this.password = password2;
	}
	public Utente(int codiceUtente){
		this.codice_utente = codiceUtente;
	}

	public Utente(int idUtente, int c, boolean f){
		this.idUtente = idUtente;
	}

	public Utente(String[] dati){
		
		for(int i=0; i<dati.length; i++){
			if(dati[i]==null)
				dati[i] = "0";
		}
		this.idUtente = Integer.parseInt(dati[0]);
		this.nome = dati[1];
		this.cognome = dati[2];
		this.email = dati[3];
		this.cod_fiscale = dati[4];
		this.password = dati[5];
		this.codice_utente = Integer.parseInt(dati[6]);
		this.magazzino = Integer.parseInt(dati[7]);
		this.ruolo = dati[8];
		this.sede=dati[9];
	}	
	public boolean login()
	{
		return UtenteDAO.getInstance().userExists(this);
	}
	public int getIdUtente() {
		return idUtente;
	}
	public String getNome() {
		return nome;
	}
	public String getCognome() {
		return cognome;
	}
	public String getEmail() {
		return email;
	}
	public String getCod_fiscale() {
		return cod_fiscale;
	}
	public String getPassword() {
		return password;
	}
	public int getCodice_utente() {
		return codice_utente;
	}
	public String getSede() {
		return UtenteDAO.getInstance().getSede(this.idUtente);
	}
	public String getRuolo() {
		return ruolo;
	}
	public int getMagazzino() {
		if(this.magazzino != 0)
			return magazzino;
		else
			return UtenteDAO.getInstance().getIdMagazzino(this);
	}
	public boolean setPassword(String newPwd, int idUtente){
		return UtenteDAO.getInstance().cambiaPassword(this, newPwd, idUtente);
	}
	public String getEmailFromDB(){
		return UtenteDAO.getInstance().getEmailFromDB(this);
	}
	public int getIdFromDB(){
		return UtenteDAO.getInstance().getIdFromDB(this);
	}
	public Utente caricaDatiUtente(){
		return UtenteDAO.getInstance().caricaUtente(this);
	}
	public ArrayList<Utente> caricaElencoDipendenti(int idCapo){
		return UtenteDAO.getInstance().caricaElencoDipendenti(idCapo);
	}
	public boolean lavoraAlProgetto(int idProgetto){
		return UtenteDAO.getInstance().lavoraAlProgetto(this, idProgetto);
	}
}
