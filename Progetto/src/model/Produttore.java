package model;

import java.util.ArrayList;

import dao.ProduttoreDAO;

public class Produttore {
	private int idProduttore;
	private String nome;
	private String email; 
	
	public String getNome() {
		return nome;
	}
	
	public int getidProduttore() {
		return idProduttore;
	}
	
	public String getemail() {
		return email;
	}
	
	public Produttore(int idProduttore, String nome, String email){
		this.idProduttore = idProduttore;
		this.nome = nome;
		this.email = email;
	}
	
	public Produttore(){}
	
	public ArrayList<Produttore> getProduttore(){
		return ProduttoreDAO.getInstance().caricaProduttore();
	}
	public Produttore prodottoDa(int idArt){
		return ProduttoreDAO.getInstance().prodottoDa(idArt);
	}

}
