package model;

import java.util.ArrayList;

import dao.CatalogoDAO;
import dao.MagazzinoDAO;

public class Magazzino {
	
	private int idmag;
	private String nome;
	private float lat;
	private float longit;
	
	public Magazzino(int id, String nome, float lat, float longit){
		
		this.idmag=id;
		this.nome=nome;
		this.lat =lat;
		this.longit=longit;
		
	}

	public Magazzino(){}
	
	public int getIdmag() {
		return idmag;
	}

	public String getNome() {
		return nome;
	}

	public float getLat() {
		return lat;
	}

	public float getLongit() {
		return longit;
	}
	public Magazzino caricaDatiMag(int idMagazzino){
		return MagazzinoDAO.getInstance().caricaDatiMagazzino(idMagazzino);
	}
	public int caricaIdMagazzino(String magazzino){
		return MagazzinoDAO.getInstance().caricaIdMagazzino(magazzino);	
	}
	public ArrayList<Magazzino> caricaElencoMagazzini(){
		return MagazzinoDAO.getInstance().caricaMagazzini();
	}

}
