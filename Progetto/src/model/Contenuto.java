package model;

public class Contenuto {
	
	private int idArticolo;
	private String nome;
	private int quantita;
	private int idMag;
	private boolean evaso;
	private int idOrdine;
	private float prezzoPagato;
	
	public Contenuto(int idArticolo, String nome, int quantita, int evaso,int idMag, float prezzoPag) {
		this.idArticolo = idArticolo;
		this.nome = nome;
		this.quantita = quantita;
		this.idMag=idMag;
		
		if(evaso==0)
			this.evaso = false;
		else
			this.evaso=true;
		this.prezzoPagato=prezzoPag;
	}

	public float getPrezzo() {
		return prezzoPagato;
	}

	public int getIdArticolo() {
		return idArticolo;
	}

	public String getNome() {
		return nome;
	}

	public int getQuantita() {
		return quantita;
	}

	public int getIdMag() {
		return idMag;
	}

	public boolean isEvaso() {
		return evaso;
	}

	public int getIdOrdine() {
		return idOrdine;
	}

}
