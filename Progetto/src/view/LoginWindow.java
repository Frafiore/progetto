package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import listeners.LoginListener;
import listeners.constants.LoginConstants;

public class LoginWindow extends JFrame {

	JTextField codiceUtente = new JTextField();
	JPasswordField password = new JPasswordField();
	
	public LoginWindow(){
		super ("Login window");
		
		Container c = this.getContentPane();
		this.setLayout(new GridLayout(3,2));
		
		LoginListener l = new LoginListener(this);
		
		JButton login = new JButton("Login");
			login.setActionCommand(LoginConstants.login);
			login.addActionListener(l);
			login.setFont(new Font("", Font.ITALIC, 20));
			
		JButton pwd_dimenticata = new JButton("Password dimenticata");
			pwd_dimenticata.setActionCommand(LoginConstants.pwd_dimenticata);
			pwd_dimenticata.addActionListener(l);
			pwd_dimenticata.setFont(new Font("", Font.ITALIC, 20));
			
		JLabel cod_utente =new JLabel("Codice Utente: ");
		cod_utente.setHorizontalAlignment(JLabel.CENTER);
		cod_utente.setFont(new Font("", Font.ITALIC, 20));
		JLabel pwd =new JLabel("Password: ");
		pwd.setHorizontalAlignment(JLabel.CENTER);
		pwd.setFont(new Font("", Font.ITALIC, 20));
		
		this.getRootPane().setDefaultButton(login);
		
		codiceUtente.setFont(new Font("", Font.ITALIC, 20));;
		pwd_dimenticata.setFont(new Font("", Font.ITALIC, 20));
		
		c.add(cod_utente);
		c.add(codiceUtente);
		c.add(pwd);
		c.add(password);
		c.add(pwd_dimenticata);
		c.add(login);
		
		
		this.setSize(520,250);
		this.setMinimumSize(new Dimension(520, 250));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public JTextField getCodiceUtente() {
		return codiceUtente;
	}

	public JPasswordField getPassword() {
		return password;
	}
}
