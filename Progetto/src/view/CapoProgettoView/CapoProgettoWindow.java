package view.CapoProgettoView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import business.Sessione;
import dao.ProgettoDAO;
import listeners.CapoProgettoListener;
import listeners.constants.CapoProgettoConstants;
import model.Progetto;
import view.tables.CapoProgetto_table;
import view.tables.StatoProgetto_table;

public class CapoProgettoWindow extends JFrame {

	private CapoProgetto_table tabella;
	private StatoProgetto_table progettoTabella;
	private JButton stampaRapp;
	private JComboBox<String> progetto;
	private JButton applica;
	private JButton dettProg;
	private JButton dettOrdine;
	private JButton dettUtente;
	private JButton logout;

	public CapoProgettoWindow(){

		super("Utente connesso come: "+Sessione.getInstance().getUtenteLoggato().getNome()+" "+Sessione.getInstance().getUtenteLoggato().getCognome()+".");

		CapoProgettoListener l = new CapoProgettoListener(this);

		Container c = this.getContentPane();

		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{50,850,50,50,110,30,30,30,50,50};
		layout.rowHeights = new int[]{30,50,60,50,75,50,62,50,63,50,100,50,50};

		c.setLayout(layout);

		GridBagConstraints g = new GridBagConstraints();

		progettoTabella = new StatoProgetto_table(Sessione.getInstance().getUtenteLoggato().getIdUtente());
		JScrollPane scroll2 = new JScrollPane(progettoTabella);

		g.gridx = 1;
		g.gridy = 8;
		g.gridheight = 4;
		g.gridwidth = 1;
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.fill = GridBagConstraints.BOTH;

		c.add(scroll2, g);

		tabella = new CapoProgetto_table(Sessione.getInstance().getUtenteLoggato().getIdUtente(),this);

		JScrollPane scroll = new JScrollPane(tabella);

		g.gridx = 1;
		g.gridy = 1;
		g.gridheight = 6;
		g.gridwidth = 1;

		c.add(scroll,g);

		stampaRapp = new JButton("Stampa rapposto spese");
		stampaRapp.setFont(new Font("", Font.PLAIN,18));
		stampaRapp.setActionCommand(CapoProgettoConstants.rappSpese);
		stampaRapp.addActionListener(l);
		g.gridx = 4;
		g.gridy = 1;
		g.gridheight = 1;
		g.gridwidth = 4;

		c.add(stampaRapp, g);

		progetto = new JComboBox<>();
		ArrayList<Progetto>  elencoProgetti = ProgettoDAO.getInstance().elencoProgetti(Sessione.getInstance().getUtenteLoggato().getIdUtente());

		progetto.addItem("Seleziona il progetto...");
		for(int i = 0; i<elencoProgetti.size();i++)
			progetto.addItem(elencoProgetti.get(i).getNome());

		progetto.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 3;
		g.gridheight = 1;
		g.gridwidth = 2;

		c.add(progetto, g);

		applica = new JButton("Applica");
		applica.setFont(new Font("", Font.PLAIN,18));
		applica.setActionCommand(CapoProgettoConstants.applica);
		applica.addActionListener(l);

		g.gridx = 6;
		g.gridy = 3;
		g.gridheight = 1;
		g.gridwidth = 3;

		c.add(applica, g);

		dettProg = new JButton("Modifica dettagli progetto");
		dettProg.setFont(new Font("", Font.PLAIN,18));
		dettProg.setActionCommand(CapoProgettoConstants.dettProgetto);
		dettProg.addActionListener(l);

		g.gridx = 4;
		g.gridy = 5;
		g.gridheight = 1;
		g.gridwidth = 4;

		c.add(dettProg, g);


		dettOrdine = new JButton("Visualizza dettagli ordine");
		dettOrdine.setFont(new Font("", Font.PLAIN,18));
		dettOrdine.setActionCommand(CapoProgettoConstants.dettOrdine);
		dettOrdine.addActionListener(l);

		g.gridx = 4;
		g.gridy = 7;
		g.gridheight = 1;
		g.gridwidth = 4;

		c.add(dettOrdine, g);

		dettUtente = new JButton("Visualizza dettagli utente");
		dettUtente.setFont(new Font("", Font.PLAIN,18));
		dettUtente.setActionCommand(CapoProgettoConstants.dettUtente);
		dettUtente.addActionListener(l);

		g.gridx = 4;
		g.gridy = 9;
		g.gridheight = 1;
		g.gridwidth = 4;

		c.add(dettUtente, g);

		logout = new JButton("Logout");
		logout.setFont(new Font("", Font.PLAIN,18));
		logout.setActionCommand(CapoProgettoConstants.logout);
		logout.addActionListener(l);

		g.gridx = 7;
		g.gridy = 11;
		g.gridheight = 1;
		g.gridwidth = 2;

		c.add(logout, g);


		this.setSize(1600,800);
		this.setMinimumSize(new Dimension(1600,800));
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public CapoProgetto_table getTabella() {
		return tabella;
	}

	public JButton getStampaRapp() {
		return stampaRapp;
	}

	public JComboBox<String> getProgetto() {
		return progetto;
	}

	public JButton getApplica() {
		return applica;
	}

	public JButton getDettOrdine() {
		return dettOrdine;
	}

	public JButton getDettUtente() {
		return dettUtente;
	}

	public JButton getLogout() {
		return logout;
	}

	public StatoProgetto_table getProgettoTabella() {
		return progettoTabella;
	}

	public JButton getDettProg() {
		return dettProg;
	}

}
