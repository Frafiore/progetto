package view.CapoProgettoView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import business.EmailManager;
import business.FornitoreBusiness;
import model.Fornitore;
import model.Produttore;
import model.Utente;
import view.MagazziniereView.*;

public class ReclamaWindow extends JFrame {
	MagazziniereWindow finestraM;
	public ReclamaWindow(Utente u, Fornitore f,Produttore p, JFrame finestra){
		super("Reclamo");

		Container c = this.getContentPane();

		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{20,150,50,50,250,100,100,20};
		layout.rowHeights = new int[]{20,40,20,40,20,300,20,40,20};	

		c.setLayout(layout);

		GridBagConstraints g = new GridBagConstraints();
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.fill = GridBagConstraints.BOTH;

		JLabel dest = new JLabel("Destinatario:");
		dest.setFont(new Font("", Font.PLAIN,18));
		dest.setHorizontalAlignment(SwingConstants.CENTER);
		g.gridx = 1;
		g.gridy = 1;
		g.gridwidth = 2;

		c.add(dest,g);

		JLabel dest1 = new JLabel();
		if(f==null && p==null)
			dest1.setText(u.getEmail()+"  "+u.getNome()+" "+u.getCognome());
		else if(p==null && u ==null)
			dest1.setText(f.getEmail()+"  "+f.getNome()+" "+f.getCognome());
		else
			dest1.setText(p.getemail()+"  "+p.getNome());
		
		dest1.setFont(new Font("", Font.PLAIN,18));
		g.gridx = 4;
		g.gridy = 1;
		g.gridwidth = 2;

		c.add(dest1,g);

		JLabel oggetto = new JLabel("Oggetto:");
		oggetto.setFont(new Font("", Font.PLAIN,18));
		oggetto.setHorizontalAlignment(SwingConstants.CENTER);
		g.gridx = 1;
		g.gridy = 3;
		g.gridwidth = 2;

		c.add(oggetto,g);


		JTextField oggetto1 = new JTextField();
		oggetto1.setFont(new Font("", Font.PLAIN,18));
		g.gridx = 4;
		g.gridy = 3;
		g.gridwidth = 1;

		c.add(oggetto1,g);

		JTextArea corpo = new JTextArea();
		corpo.setFont(new Font("", Font.PLAIN,18));
		g.gridx = 1;
		g.gridy = 5;
		g.gridwidth = 6;
		JScrollPane scroll = new JScrollPane(corpo);
		c.add(scroll,g);

		JButton annulla = new JButton("Annulla");
		annulla.setPreferredSize(new Dimension(150, annulla.getPreferredSize().height*3/2));
		annulla.setFont(new Font("", Font.PLAIN,18));
		annulla.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				setVisible(false);
				dispose();
			}
		});

		g.gridx = 1;
		g.gridy = 7;
		g.gridwidth = 1;
		g.fill = GridBagConstraints.NONE;

		c.add(annulla,g);

		JButton invia = new JButton("Invia");
		invia.setPreferredSize(new Dimension(150, invia.getPreferredSize().height*3/2));
		invia.setFont(new Font("", Font.PLAIN,18));
		invia.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				if(oggetto1.getText().equals("") || corpo.getText().equals("")){
					JOptionPane.showMessageDialog(corpo, "L'oggetto o il corpo dell'email è vuoto.\nPrego inserire il testo.", "Attenzione!", JOptionPane.WARNING_MESSAGE);
				}
				else{
					String oggetto = oggetto1.getText();
					String emailBody = corpo.getText();

					if(f==null){
						String destinatario = u.getEmail();
						try {
							EmailManager.getInstance().sendEmail(destinatario, oggetto,emailBody);
							JOptionPane.showMessageDialog(corpo, "E' stata inviata un'email di reclamo all'indirizzo "+destinatario+".");
							setVisible(false);
							dispose();

						} catch (AddressException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(dest1, "Errore nell'email inserita","Errore",JOptionPane.ERROR_MESSAGE);
						} catch (MessagingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(dest1, "Errore nel messaging","Errore",JOptionPane.ERROR_MESSAGE);
						}
					}else{
						String destinatario = f.getEmail();

						try {
							EmailManager.getInstance().sendEmail(destinatario, oggetto,emailBody);
							JOptionPane.showMessageDialog(corpo, "E' stata inviata un'email di reclamo all'indirizzo "+destinatario+".");
							setVisible(false);
							dispose();

						} catch (AddressException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(dest1, "Errore nell'email inserita","Errore",JOptionPane.ERROR_MESSAGE);
						} catch (MessagingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(dest1, "Errore nel messaging","Errore",JOptionPane.ERROR_MESSAGE);
						}
					}

				}

			}
		});

		g.gridx = 6;
		g.gridy = 7;
		g.gridwidth = 1;

		c.add(invia,g);

		this.setSize(750,550);
		this.setMinimumSize(new Dimension(750,550));
		this.setLocationRelativeTo(finestra);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);

	}

}
