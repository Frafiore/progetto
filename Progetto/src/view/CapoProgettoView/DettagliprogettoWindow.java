package view.CapoProgettoView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import business.ProgettoBusiness;
import business.Sessione;
import model.Progetto;

public class DettagliprogettoWindow extends JFrame {

	public DettagliprogettoWindow(Progetto p, CapoProgettoWindow f){
		super("Modifica dettagli ordine");

		Container c = this.getContentPane();

		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{20,150,50,150,50,150,20};
		layout.rowHeights = new int[]{20,40,20,40,40,20,40,20,40,20};	

		c.setLayout(layout);

		GridBagConstraints g = new GridBagConstraints();
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.fill = GridBagConstraints.BOTH;
		
		JLabel nome = new JLabel("Nome:");
		nome.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 1;
		g.gridwidth = 1;
		c.add(nome,g);
		
		JTextField nome1 = new JTextField(p.getNome());
		nome1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 4;
		g.gridy = 1;
		g.gridwidth = 2;
		c.add(nome1,g);
		
		JLabel stato = new JLabel("Stato:");
		stato.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 3;
		g.gridwidth = 1;
		c.add(stato,g);
		
		JRadioButton attivo = new JRadioButton("Attivo");
		attivo.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 4;
		g.gridy = 3;
		g.gridwidth = 2;
		c.add(attivo,g);	
		
		JRadioButton chiuso = new JRadioButton("Chiuso");
		chiuso.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 4;
		g.gridy = 4;
		g.gridwidth = 2;
		c.add(chiuso,g);
		
		ButtonGroup group = new ButtonGroup();
		group.add(attivo);
		group.add(chiuso);
		
		if(p.isAttivo())
			group.setSelected(attivo.getModel(), true);
		else
			group.setSelected(chiuso.getModel(), true);
		
		JLabel budget = new JLabel("Budget:");
		budget.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 6;
		g.gridwidth = 1;
		c.add(budget,g);
		

		final DecimalFormat df = new DecimalFormat("0");
		df.setRoundingMode(RoundingMode.CEILING);
		
		JTextField budg = new JTextField(""+df.format(p.getBudget()));
		budg.setFont(new Font("", Font.PLAIN,18));
		budg.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if(budg.getText().equals(""))
					budg.setText(""+df.format(p.getBudget()));
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				budg.setText("");
			}
		});

		g.gridx = 4;
		g.gridy = 6;
		g.gridwidth = 2;
		c.add(budg,g);
		
		JButton annulla = new JButton("Annulla");
		annulla.setFont(new Font("", Font.PLAIN,18));
		annulla.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				setVisible(false);
				dispose();
			}
		});
		
		g.gridx = 1;
		g.gridy = 8;
		g.gridwidth = 1;
		c.add(annulla,g);

		JButton reset = new JButton("Reset");
		reset.setFont(new Font("", Font.PLAIN,18));
		reset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				nome1.setText(p.getNome());
				budg.setText(""+df.format(p.getBudget()));
				if(p.isAttivo())
					group.setSelected(attivo.getModel(), true);
				else
					group.setSelected(chiuso.getModel(), true);
				
			}
		});
		
		g.gridx = 3;
		g.gridy = 8;
		g.gridwidth = 1;
		c.add(reset,g);
		
		JButton applica = new JButton("Applica");
		applica.setFont(new Font("", Font.PLAIN,18));
		applica.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub

				try {
					String n = nome1.getText();
					String b = budg.getText();
					

					final DecimalFormat df = new DecimalFormat("0.00");
					df.setRoundingMode(RoundingMode.CEILING);
					
					String x = df.format(Float.parseFloat(b));
					
					
					
					String s = new String();
					int idProg = ProgettoBusiness.getInstance().caricaIdProgetto(p.getNome());
					
					if(attivo.isSelected())
						s="1";
					else if(chiuso.isSelected())
						s="0";
					
					boolean y = ProgettoBusiness.getInstance().updateProgetto(idProg, n, s, b);
					
					if(y){
						f.getProgettoTabella().aggiornaTabella(f, Sessione.getInstance().getUtenteLoggato().getIdUtente());
						setVisible(false);
					}
					
				} catch (NumberFormatException e) {
					// TODO: handle exception
					JOptionPane.showMessageDialog(null, "Caratteri inseriti in budget non validi.\nInserire un numero grazie.", "Errore", JOptionPane.ERROR_MESSAGE);
					budg.setText(""+df.format(p.getBudget()));
				}
			}
		});
		
		g.gridx = 5;
		g.gridy = 8;
		g.gridwidth = 1;
		c.add(applica,g);

		this.setSize(450,450);
		this.setMinimumSize(new Dimension(450,450));
		this.setLocationRelativeTo(f);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		
	}

}
