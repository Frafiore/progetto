package view.CapoProgettoView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import business.OrdineBusiness;
import business.ProgettoBusiness;
import business.StampaDistinta;
import business.UtenteBusiness;
import model.Contenuto;
import model.Ordine;
import model.Utente;

public class DettagliOrdineWindow extends JFrame {

	private Ordine o;
	private	ArrayList<Contenuto> c;
	private	ArrayList<String> magazzini;

	public DettagliOrdineWindow(CapoProgettoWindow finestra, Ordine o, ArrayList<Contenuto> c, ArrayList<String> magazzini) {
		super("Dettagli ordine "+o.getIdOrdine());
		this.o = o;
		this.c =c;
		this.magazzini =magazzini;

		Container co = this.getContentPane();

		GridBagLayout lay = new GridBagLayout();
		lay.columnWidths = new int[]{20,100,70,200,100,20};
		lay.rowHeights = new int[]{20,30,5,30,5,30,5,50,5,30,5,60,5,30,5,200,5,30,5,30,20};

		co.setLayout(lay);

		GridBagConstraints g = new GridBagConstraints();
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.fill = GridBagConstraints.BOTH;

		JLabel dip = new JLabel("DIPENDENTE: "+ o.getDipendete()+" (id Dipendente: "+o.getIdDip()+")");
		dip.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		dip.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		dip.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 1;
		g.gridwidth = 4;
		co.add(dip,g);

		JLabel data = new JLabel("- Data:");
		data.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		data.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		data.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 3;
		g.gridwidth = 4;
		co.add(data,g);

		JLabel data1 = new JLabel(o.getData());
		data1.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		data1.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		data1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 3;
		g.gridwidth = 2;
		co.add(data1,g);

		JLabel numOrdine = new JLabel("- Numero ordine: ");
		numOrdine.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		numOrdine.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		numOrdine.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 5;
		g.gridwidth = 4;
		co.add(numOrdine,g);

		JLabel numOrdine1 = new JLabel(""+o.getIdOrdine());
		numOrdine1.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		numOrdine1.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		numOrdine1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 5;
		g.gridwidth = 2;
		co.add(numOrdine1,g);

		JLabel mag = new JLabel("- Magazzino:");
		mag.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		mag.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		mag.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 7;
		g.gridwidth = 1;
		co.add(mag,g);

		JLabel mags = new JLabel("<html>");
		for (Iterator iterator = magazzini.iterator(); iterator.hasNext();) {
			String stringa = (String) iterator.next();
			mags.setText(mags.getText()+"- "+stringa+"<br>");
		}
		mags.setText(mags.getText()+"</html>");
		mags.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		mags.setAlignmentY(JLabel.NORTH_WEST);
		mags.setFont(new Font("", Font.PLAIN,18));
		JScrollPane scroll1 = new JScrollPane(mags);

		g.gridx = 3;
		g.gridy = 7;
		g.gridwidth = 2;
		co.add(scroll1,g);

		JLabel progetto = new JLabel("- Progetto: ");
		progetto.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		progetto.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		progetto.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 9;
		g.gridwidth = 4;
		co.add(progetto,g);

		JLabel progetto1 = new JLabel(""+ProgettoBusiness.getInstance().caricaNomeProgetto(o.getProgetto()));
		progetto1.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		progetto1.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		progetto1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 9;
		g.gridwidth = 2;
		co.add(progetto1,g);

		JLabel note = new JLabel("- Note: ");
		note.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		note.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		note.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 11;
		g.gridwidth = 4;
		co.add(note,g);

		JLabel note2 = new JLabel(o.getNote());
		note2.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		note2.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		note2.setFont(new Font("", Font.PLAIN,18));
		JScrollPane scroll2 = new JScrollPane(note2);

		g.gridx = 3;
		g.gridy = 11;
		g.gridwidth = 2;
		co.add(scroll2,g);

		JLabel contenuto = new JLabel("CONTENUTO");
		contenuto.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		contenuto.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		contenuto.setFont(new Font("", Font.PLAIN,20));

		g.gridx = 2;
		g.gridy = 13;
		g.gridwidth = 3;
		co.add(contenuto,g);

		JLabel contenutoVero=new JLabel("<html>");
		for (Iterator iterator = c.iterator(); iterator.hasNext();) {
			Contenuto cont = (Contenuto) iterator.next();
			contenutoVero.setText(contenutoVero.getText()+"- "+cont.getQuantita()+" -- "+cont.getNome()+";  Costo unitario (EUR): "+cont.getPrezzo()+";<br>");
		}
		contenutoVero.setText(contenutoVero.getText()+"</html>");
		contenutoVero.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		contenutoVero.setFont(new Font("", Font.PLAIN,18));
		JScrollPane scroll3 = new JScrollPane(contenutoVero);

		g.gridx = 1;
		g.gridy = 15;
		g.gridwidth = 4;
		co.add(scroll3,g);

		JLabel spedizione = new JLabel("- Spedizione (EUR): ");
		spedizione.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		spedizione.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		spedizione.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 17;
		g.gridwidth = 3;
		co.add(spedizione,g);

		JLabel spedizione1 = new JLabel(""+o.getSpedizione());
		spedizione1.setHorizontalAlignment(SwingConstants.CENTER);
		spedizione1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 17;
		g.gridwidth = 2;
		co.add(spedizione1,g);

		JLabel totale = new JLabel("- Costo totale (EUR): ");
		totale.setAlignmentX(JLabel.LEFT_ALIGNMENT);
		totale.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		totale.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 19;
		g.gridwidth = 3;
		co.add(totale,g);

		JLabel totale1 = new JLabel(""+o.getCosto_totale());
		totale1.setHorizontalAlignment(SwingConstants.CENTER);
		totale1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 19;
		g.gridwidth = 2;
		co.add(totale1,g);

		JButton chiudi = new JButton("Chiudi");
		chiudi.setFont(new Font("", Font.PLAIN,18));
		chiudi.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
				dispose();
			}
		});

		g.gridx = 4;
		g.gridy = 21;
		g.gridwidth = 1;
		co.add(chiudi,g);

		JButton stampa = new JButton("Stampa");
		stampa.setFont(new Font("", Font.PLAIN,18));
		stampa.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				JFileChooser fc = new JFileChooser();
				int ans = fc.showSaveDialog(finestra);

				if(ans==JFileChooser.APPROVE_OPTION){

					String path = fc.getSelectedFile().getPath();
					String nomeFilePDF = path+".pdf";
					Utente u = UtenteBusiness.getInstance().caricaUtente(o.getIdDip());
					try {
						StampaDistinta.getInstance().stampaDistinta(u, getO(), getC(), getMagazzini(),nomeFilePDF,null);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});

		g.gridx = 1;
		g.gridy = 21;
		g.gridwidth = 1;
		co.add(stampa,g);

		this.setSize(650,850);
		this.setMinimumSize(new Dimension(650,850));
		this.setLocationRelativeTo(finestra);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);

	}

	public Ordine getO() {
		return o;
	}
	public ArrayList<Contenuto> getC() {
		return c;
	}
	public ArrayList<String> getMagazzini() {
		return magazzini;
	}
}
