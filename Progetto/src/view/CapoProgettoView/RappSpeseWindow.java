package view.CapoProgettoView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import business.OrdineBusiness;
import business.ProgettoBusiness;
import business.Sessione;
import business.StampaRapportoSpese;
import listeners.CapoProgettoListener;
import model.Ordine;
import model.Progetto;

public class RappSpeseWindow extends JFrame {

	JRadioButton dipendente;
	JRadioButton progetto;
	private JButton conferma;
	private JButton annulla;

	public RappSpeseWindow(CapoProgettoWindow finestra){
		super("Stampa rapporto spese");

		Container c = this.getContentPane();


		GridBagLayout lay = new GridBagLayout();
		lay.columnWidths = new int[]{20,80,20,80,20};
		lay.rowHeights = new int[]{20,50,10,50,50,10,50,20};
		c.setLayout(lay);

		GridBagConstraints g = new GridBagConstraints();
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.gridheight = 1;
		g.fill = GridBagConstraints.BOTH;

		JLabel raggruppa = new JLabel("Raggruppa spese per:");
		raggruppa.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		raggruppa.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		raggruppa.setFont(new Font("", Font.PLAIN,20));
		g.gridx = 1;
		g.gridy = 1;
		g.gridwidth = 3;
		c.add(raggruppa,g);


		dipendente = new JRadioButton("Dipendente");
		dipendente.setFont(new Font("", Font.PLAIN,18));
		dipendente.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				conferma.setEnabled(true);
			}
		});

		g.gridx = 1;
		g.gridy = 3;
		g.gridwidth = 3;
		c.add(dipendente,g);

		progetto = new JRadioButton("Progetto");
		progetto.setFont(new Font("", Font.PLAIN,18));
		progetto.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				conferma.setEnabled(true);
			}
		});
		g.gridx = 1;
		g.gridy = 4;
		g.gridwidth = 3;
		c.add(progetto,g);

		ButtonGroup group = new ButtonGroup();
		group.add(progetto);
		group.add(dipendente);


		annulla = new JButton("Annulla");
		annulla.setFont(new Font("", Font.PLAIN,18));
		annulla.setFont(new Font("", Font.PLAIN,18));
		annulla.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
				dispose();
			}
		});

		g.gridx = 1;
		g.gridy = 6;
		g.gridwidth = 1;
		c.add(annulla,g);

		conferma = new JButton("Conferma");
		conferma.setFont(new Font("", Font.PLAIN,18));
		conferma.setFont(new Font("", Font.PLAIN,18));
		conferma.setEnabled(false);
		RappSpeseWindow f = this;
		conferma.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean x;
				if(dipendente.isSelected())
					x=true;
				else 
					x=false;

				ArrayList<Ordine> elenco = OrdineBusiness.getInstance().caricaElencoOrdiniPerCapoProgetto(Sessione.getInstance().getUtenteLoggato().getIdUtente());
				
				JFileChooser fc = new JFileChooser();
				int ans = fc.showSaveDialog(f);
				if(ans==JFileChooser.APPROVE_OPTION){
					String path = fc.getSelectedFile().getPath();
					String nomeFilePDF = path+".pdf";

					try {
						StampaRapportoSpese.getInstance().stampaRapporto(elenco, x, nomeFilePDF, finestra);
						setVisible(false);
						dispose();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});

		g.gridx = 3;
		g.gridy = 6;
		g.gridwidth = 1;
		c.add(conferma,g);

		this.setSize(400,300);
		this.setMinimumSize(new Dimension(400,300));
		this.setLocationRelativeTo(finestra);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);

	}

}
