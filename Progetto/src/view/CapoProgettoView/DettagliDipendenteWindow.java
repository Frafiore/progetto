package view.CapoProgettoView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import model.Utente;

public class DettagliDipendenteWindow extends JFrame {
	
	public DettagliDipendenteWindow(Utente u,CapoProgettoWindow finestra){
		super("Dettagli dipendente");
		
		Container c = this.getContentPane();
		
		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{20,100,80,100,20};
		layout.rowHeights = new int[]{20,30,5,30,5,30,5,30,5,30,5,30,5,30,20,50,20};	
		
		c.setLayout(layout);

		GridBagConstraints g = new GridBagConstraints();
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.fill = GridBagConstraints.BOTH;
		
		JLabel nome = new JLabel("Nome:");
		nome.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 1;
		g.gridwidth = 2;
		c.add(nome,g);
		
		JLabel nome1 = new JLabel(u.getNome());
		nome1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 1;
		g.gridwidth = 1;
		c.add(nome1,g);
		
		JLabel cognome = new JLabel("Cognome:");
		cognome.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 3;
		g.gridwidth = 2;
		c.add(cognome,g);
		
		JLabel cognome1 = new JLabel(u.getCognome());
		cognome1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 3;
		g.gridwidth = 1;
		c.add(cognome1,g);
		
		JLabel id = new JLabel("ID:");
		id.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 5;
		g.gridwidth = 2;
		c.add(id,g);
		
		JLabel id1 = new JLabel(""+u.getIdUtente());
		id1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 5;
		g.gridwidth = 1;
		c.add(id1,g);
		
		JLabel email = new JLabel("Email:");
		email.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 7;
		g.gridwidth = 2;
		c.add(email,g);
		
		JLabel email1 = new JLabel(""+u.getEmail());
		email1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 7;
		g.gridwidth = 1;
		c.add(email1,g);
		
		JLabel codFisc = new JLabel("Codice fiscale:");
		codFisc.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 9;
		g.gridwidth = 2;
		c.add(codFisc,g);
		
		JLabel codFisc1 = new JLabel(""+u.getCod_fiscale());
		codFisc1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 9;
		g.gridwidth = 1;
		c.add(codFisc1,g);
		
		JLabel codUt = new JLabel("Codice utente:");
		codUt.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 11;
		g.gridwidth = 2;
		c.add(codUt,g);
		
		JLabel codUt1 = new JLabel(""+u.getCodice_utente());
		codUt1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 11;
		g.gridwidth = 1;
		c.add(codUt1,g);
		
		JLabel sede = new JLabel("Sede:");
		sede.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 1;
		g.gridy = 13;
		g.gridwidth = 2;
		c.add(sede,g);
		
		JLabel sede1 = new JLabel(""+u.getSede());
		sede1.setFont(new Font("", Font.PLAIN,18));

		g.gridx = 3;
		g.gridy = 13;
		g.gridwidth = 1;
		c.add(sede1,g);
		
		JButton chiudi = new JButton("Chiudi");
		chiudi.setFont(new Font("", Font.PLAIN,18));
		chiudi.setPreferredSize(new Dimension(130, chiudi.getPreferredSize().height*3/2));
		chiudi.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
				dispose();
			}
		});

		g.gridx = 1;
		g.gridy = 15;
		g.gridwidth = 1;
		g.fill = GridBagConstraints.NONE;
		c.add(chiudi,g);
		
		JButton richiama = new JButton("Richiama");
		richiama.setFont(new Font("", Font.PLAIN,18));
		richiama.setPreferredSize(new Dimension(130, richiama.getPreferredSize().height*3/2));
		DettagliDipendenteWindow f = this;
		richiama.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				new ReclamaWindow(u,null,null, f);
				
			}
		});

		g.gridx = 3;
		g.gridy = 15;
		g.gridwidth = 1;
		c.add(richiama,g);
		

		this.setSize(450,450);
		this.setMinimumSize(new Dimension(450,450));
		this.setLocationRelativeTo(finestra);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

}
