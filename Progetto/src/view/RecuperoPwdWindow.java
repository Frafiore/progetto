package view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import listeners.RecuperoPwdListener;
import listeners.constants.LoginConstants;
import listeners.constants.RecuperoPwdConstants;

public class RecuperoPwdWindow extends JFrame {
	
	
	//jp1
	private JPanel jpN1 = new JPanel(new FlowLayout());
	private JPanel jpC1 = new JPanel(new FlowLayout());
	private JPanel jpS1 = new JPanel(new FlowLayout());
	
	private JTextField codiceUtente = new JTextField(10);
	
	//jp2
	private JPanel jpN2 = new JPanel(new FlowLayout());
	private JPanel jpC2 = new JPanel(new FlowLayout());
	private JPanel jpS2 = new JPanel(new FlowLayout());
	
	private JButton conferma2 = new JButton("Conferma");
	private JTextField codiceDiVerifica = new JTextField(10);
	
	//jp3
	private JPanel jpN3 = new JPanel(new FlowLayout());
	private JPanel jpC3 = new JPanel(new FlowLayout());
	private JPanel jpS3 = new JPanel(new FlowLayout());
	
	private JPasswordField new_pwd1 = new JPasswordField(10);
	private JPasswordField new_pwd2 = new JPasswordField(10);
	private JButton conferma3 = new JButton("Conferma");
	
	public RecuperoPwdWindow(){
		
		super("Cambiamento password");
		Container c = this.getContentPane();
		c.setLayout(new BorderLayout());
		
		RecuperoPwdListener l = new RecuperoPwdListener(this);
		
		//CREO JP1
		JLabel label1 = new JLabel("Inserisci il tuo codice utente:");
		JButton conferma1 = new JButton("Conferma");
			conferma1.setActionCommand(RecuperoPwdConstants.conferma1);
			conferma1.addActionListener(l);
		JButton annulla1 = new JButton("Annulla");
			annulla1.setActionCommand(RecuperoPwdConstants.annulla1);
			annulla1.addActionListener(l);
		this.getRootPane().setDefaultButton(conferma1);
			jpN1.add(label1);
			jpC1.add(codiceUtente);
			jpS1.add(annulla1);
			jpS1.add(conferma1);
		
		//CREO JP2
		JLabel label2 = new JLabel("Inserisci il codice di verifica:");
		
			conferma2.setActionCommand(RecuperoPwdConstants.conferma2);
			conferma2.addActionListener(l);
		JButton annulla2 = new JButton("Annulla");
			annulla2.setActionCommand(RecuperoPwdConstants.annulla2);
			annulla2.addActionListener(l);
		JButton reSndPwd = new JButton("Reinvia email");
			reSndPwd.setActionCommand(RecuperoPwdConstants.reSendPwd);
			reSndPwd.addActionListener(l);
			
			jpN2.add(label2);
			jpC2.add(codiceDiVerifica);
			jpS2.add(annulla2);
			jpS2.add(reSndPwd);
			jpS2.add(conferma2);
		
		//CREO JP3
		JLabel nuovaPwd = new JLabel("Inserisci la nuova password:");
		JLabel confPwd = new JLabel("Conferma la password:");
		
			conferma3.setActionCommand(RecuperoPwdConstants.conferma3);
			conferma3.addActionListener(l);
		JButton annulla3 = new JButton("Annulla");
			annulla3.setActionCommand(RecuperoPwdConstants.annulla3);
			annulla3.addActionListener(l);
			
			jpN3.add(nuovaPwd);
			jpN3.add(new_pwd1);
			jpC3.add(confPwd);
			jpC3.add(new_pwd2);
			jpS3.add(annulla3);
			jpS3.add(conferma3);
			
		c.add(jpN1, BorderLayout.NORTH);
		c.add(jpC1, BorderLayout.CENTER);
		c.add(jpS1, BorderLayout.SOUTH);
		
			
		this.setSize(400,150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}


	public JTextField getCodiceUtente() {
		return codiceUtente;
	}


	public JTextField getCodiceDiVerifica() {
		return codiceDiVerifica;
	}


	public JPasswordField getNew_pwd1() {
		return new_pwd1;
	}


	public JPasswordField getNew_pwd2() {
		return new_pwd2;
	}


	public JPanel getJpC1() {
		return jpC1;
	}


	public JPanel getJpC2() {
		return jpC2;
	}


	public JPanel getJpN1() {
		return jpN1;
	}


	public JPanel getJpS1() {
		return jpS1;
	}


	public JPanel getJpN2() {
		return jpN2;
	}


	public JPanel getJpS2() {
		return jpS2;
	}


	public JPanel getJpN3() {
		return jpN3;
	}


	public JPanel getJpC3() {
		return jpC3;
	}


	public JPanel getJpS3() {
		return jpS3;
	}
	
	public JButton getConferma2() {
		return conferma2;
	}


	public JButton getConferma3() {
		return conferma3;
	}
}
