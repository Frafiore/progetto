package view.MagazziniereView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import business.FornitoreBusiness;
import business.OrdineBusiness;
import business.ProduttoreBusiness;
import business.ProgettoBusiness;
import business.Sessione;
import business.StampaRapportoSpese;
import listeners.CapoProgettoListener;
import model.Fornitore;
import model.Ordine;
import model.Produttore;
import model.Progetto;
import view.CapoProgettoView.ReclamaWindow;

public class SelezionaReclamoWindow extends JFrame {

	JRadioButton produttore;
	JRadioButton fornitore;
	private JButton conferma;
	private JButton annulla;

	public SelezionaReclamoWindow(MagazziniereWindow finestra){
		super("Seleziona Reclamo");

		Container c = this.getContentPane();


		GridBagLayout lay = new GridBagLayout();
		lay.columnWidths = new int[]{20,80,20,80,20};
		lay.rowHeights = new int[]{20,50,10,50,50,10,50,20};
		c.setLayout(lay);

		GridBagConstraints g = new GridBagConstraints();
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.gridheight = 1;
		g.fill = GridBagConstraints.BOTH;

		JLabel raggruppa = new JLabel("Reclama a:");
		raggruppa.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		raggruppa.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		raggruppa.setFont(new Font("", Font.PLAIN,20));
		g.gridx = 1;
		g.gridy = 1;
		g.gridwidth = 3;
		c.add(raggruppa,g);


		produttore = new JRadioButton("Produttore");
		produttore.setFont(new Font("", Font.PLAIN,18));
		produttore.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				conferma.setEnabled(true);
			}
		});

		g.gridx = 1;
		g.gridy = 3;
		g.gridwidth = 3;
		c.add(produttore,g);

		fornitore = new JRadioButton("Fornitore");
		fornitore.setFont(new Font("", Font.PLAIN,18));
		fornitore.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				conferma.setEnabled(true);
			}
		});
		g.gridx = 1;
		g.gridy = 4;
		g.gridwidth = 3;
		c.add(fornitore,g);

		ButtonGroup group = new ButtonGroup();
		group.add(produttore);
		group.add(fornitore);


		annulla = new JButton("Annulla");
		annulla.setFont(new Font("", Font.PLAIN,18));
		annulla.setFont(new Font("", Font.PLAIN,18));
		annulla.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
				dispose();
			}
		});

		g.gridx = 1;
		g.gridy = 6;
		g.gridwidth = 1;
		c.add(annulla,g);

		conferma = new JButton("Conferma");
		conferma.setFont(new Font("", Font.PLAIN,18));
		conferma.setFont(new Font("", Font.PLAIN,18));
		conferma.setEnabled(false);
		conferma.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean x;
				if(fornitore.isSelected())
					x=true;
				else 
					x=false;

				int idArticolo = (int) finestra.getTabella3().getValueAt(finestra.getTabella3().getSelectedRow(), 0);
				if(x){
					Fornitore fornitore = FornitoreBusiness.getInstance().caricaFornitore(idArticolo);
					new ReclamaWindow(null,fornitore,null,finestra);
				}
				else{
					Produttore p = ProduttoreBusiness.getInstance().prodottoDa(idArticolo);
					new ReclamaWindow(null,null,p,finestra);
				}
				
				setVisible(false);
			}
		});

		g.gridx = 3;
		g.gridy = 6;
		g.gridwidth = 1;
		c.add(conferma,g);

		this.setSize(400,300);
		this.setMinimumSize(new Dimension(400,300));
		this.setLocationRelativeTo(finestra);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);

	}

}
