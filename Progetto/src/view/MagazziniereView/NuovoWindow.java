package view.MagazziniereView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import business.CategoriaBusiness;
import business.ProduttoreBusiness;
import dao.FornitoreDAO;
import listeners.NuovoListener;
import listeners.constants.NuovoConstants;
import model.Categoria;
import model.Fornitore;
import model.Produttore;

public class NuovoWindow extends JFrame {

	private GridBagConstraints g = new GridBagConstraints();

	private MagazziniereWindow finestra;
	private GridBagConstraints gbc;
	private JLabel titolo;
	private JLabel nome;
	private JTextField nome_t;
	private JLabel categoria;
	private JComboBox<String> categoria_t;
	private JLabel produttore;
	private JComboBox<String> produttore_t;
	private JLabel descrizione;
	private JScrollPane descrizione_t;
	private JTextArea textArea;
	private JLabel prezzo;
	private JTextField prezzo_t;
	private JLabel quantita;
	private JTextField quantita_t;
	private JLabel ordinabili;
	private JTextField ordinabili_t;
	private JButton annulla;
	private JButton aggiungi;

	private JLabel fornitore;

	private JComboBox<String> fornitore_t;

	public NuovoWindow(MagazziniereWindow finestra) {		
		super("Nuovo prodotto");

		this.finestra = finestra;

		NuovoListener l = new NuovoListener(this);
		Container c = this.getContentPane();

		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{20,100,80,100,20};
		layout.rowHeights = new int[]{30,10,20,5,20,5,20,5,50,5,20,5,20,5,20,5,20,5,50,10};
		c.setLayout(layout);

		gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;


		titolo = new JLabel("AGGIUNGI NUOVO PRODOTTO");
		titolo.setFont(new Font("Poiret One", Font.BOLD, 25));
		titolo.setHorizontalAlignment(SwingConstants.CENTER);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		c.add(titolo, gbc);

		nome = new JLabel("Nome: ");
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		c.add(nome, gbc);
		nome_t = new JTextField();
		gbc.gridx = 3;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		c.add(nome_t, gbc);

		categoria = new JLabel("Categoria: ");
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridwidth = 1;
		c.add(categoria, gbc);
		categoria_t = new JComboBox<String>();

		ArrayList<Categoria> cat = CategoriaBusiness.getInstance().getCategorie();
		categoria_t.addItem("Seleziona...");
		for(int i = 0; i<cat.size();i++)
			categoria_t.addItem(cat.get(i).getNome());

		gbc.gridx = 3;
		gbc.gridy = 4;
		gbc.gridwidth = 1;
		c.add(categoria_t, gbc);

		produttore = new JLabel("Produttore: ");
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		c.add(produttore, gbc);
		produttore_t = new JComboBox<String>();

		ArrayList<Produttore> pro = ProduttoreBusiness.getInstance().getProduttore();
		produttore_t.addItem("Seleziona...");
		for(int i = 0; i<pro.size();i++)
			produttore_t.addItem(pro.get(i).getNome());

		gbc.gridx = 3;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		c.add(produttore_t, gbc);

		descrizione = new JLabel("Descrizione:");
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.gridwidth = 1;
		c.add(descrizione, gbc);
		descrizione_t = new JScrollPane();
		gbc.gridx = 3;
		gbc.gridy = 8;
		gbc.gridwidth = 1;
		c.add(descrizione_t, gbc);
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		descrizione_t.setViewportView(textArea);

		prezzo = new JLabel("Prezzo: ");
		gbc.gridx = 1;
		gbc.gridy = 10;
		gbc.gridwidth = 1;
		c.add(prezzo, gbc);
		prezzo_t = new JTextField();
		gbc.gridx = 3;
		gbc.gridy = 10;
		gbc.gridwidth = 1;
		c.add(prezzo_t, gbc);

		quantita = new JLabel("Quantita: ");
		gbc.gridx = 1;
		gbc.gridy = 12;
		gbc.gridwidth = 1;
		c.add(quantita, gbc);
		quantita_t = new JTextField();
		gbc.gridx = 3;
		gbc.gridy = 12;
		gbc.gridwidth = 1;
		c.add(quantita_t, gbc);

		ordinabili = new JLabel("Ordinabili: ");
		gbc.gridx = 1;
		gbc.gridy = 14;
		gbc.gridwidth = 1;
		c.add(ordinabili, gbc);
		ordinabili_t = new JTextField();
		gbc.gridx = 3;
		gbc.gridy = 14;
		gbc.gridwidth = 1;
		c.add(ordinabili_t, gbc);

		fornitore = new JLabel("Foritore: ");
		gbc.gridx = 1;
		gbc.gridy = 16;
		gbc.gridwidth = 1;
		c.add(fornitore, gbc);
		fornitore_t = new JComboBox<String>();

		ArrayList<Fornitore> forn = FornitoreDAO.getInstance().caricaElencoFornitori();
		fornitore_t.addItem("Seleziona...");
		for(int i = 0; i<forn.size();i++)
			fornitore_t.addItem(forn.get(i).getNome() + " " + forn.get(i).getCognome());

		gbc.gridx = 3;
		gbc.gridy = 16;
		gbc.gridwidth = 1;
		c.add(fornitore_t, gbc);

		annulla = new JButton("Reset");
		annulla.setActionCommand(NuovoConstants.annulla);
		annulla.addActionListener(l);
		gbc.gridx = 1;
		gbc.gridy = 18;
		gbc.fill = GridBagConstraints.NONE;
		annulla.setPreferredSize(new Dimension(annulla.getPreferredSize().width*3/2, annulla.getPreferredSize().height*3/2));
		gbc.gridwidth = 1;
		c.add(annulla, gbc);

		aggiungi = new JButton("Aggiungi");
		aggiungi.addActionListener(l);
		aggiungi.setActionCommand(NuovoConstants.aggiungi);
		gbc.gridx = 3;
		gbc.gridy = 18;
		gbc.fill = GridBagConstraints.NONE;
		aggiungi.setPreferredSize(new Dimension(aggiungi.getPreferredSize().width*3/2, aggiungi.getPreferredSize().height*3/2));
		gbc.gridwidth = 1;
		c.add(aggiungi, gbc);

		this.setSize(450, 450);
		this.setMinimumSize(new Dimension(450, 450));
		this.setMaximumSize(new Dimension(450, 450));
		this.setExtendedState(this.getExtendedState());
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	public MagazziniereWindow getFinestra() {
		return finestra;
	}

	public GridBagConstraints getG() {
		return g;
	}

	public GridBagConstraints getGbc() {
		return gbc;
	}

	public JLabel getTitolo() {
		return titolo;
	}

	public JLabel getNome() {
		return nome;
	}

	public JTextField getNome_t() {
		return nome_t;
	}

	public JLabel getCategoria() {
		return categoria;
	}

	public JComboBox<String> getCategoria_t() {
		return categoria_t;
	}

	public JLabel getProduttore() {
		return produttore;
	}

	public JComboBox<String> getProduttore_t() {
		return produttore_t;
	}

	public JLabel getDescrizione() {
		return descrizione;
	}

	public JScrollPane getDescrizione_t() {
		return descrizione_t;
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public JLabel getPrezzo() {
		return prezzo;
	}

	public JTextField getPrezzo_t() {
		return prezzo_t;
	}

	public JLabel getQuantita() {
		return quantita;
	}

	public JTextField getQuantita_t() {
		return quantita_t;
	}

	public JLabel getOrdinabili() {
		return ordinabili;
	}

	public JTextField getOrdinabili_t() {
		return ordinabili_t;
	}

	public JButton getAnnulla() {
		return annulla;
	}

	public JButton getAggiungi() {
		return aggiungi;
	}

	public JLabel getFornitore() {
		return fornitore;
	}

	public JComboBox<String> getFornitore_t() {
		return fornitore_t;
	}


}
