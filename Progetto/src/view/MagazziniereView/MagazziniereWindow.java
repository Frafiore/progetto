package view.MagazziniereView;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import com.toedter.calendar.JDateChooser;

import business.Sessione;
import listeners.MagazziniereListener;
import listeners.constants.MagazziniereConstants;
import view.tables.Contenuto_Ordine_table;
import view.tables.Contenuto_StoricoOrdineMag_table;
import view.tables.Ordine_table;
import view.tables.Rifornimento_table;
import view.tables.StoricoOrdineMag_table;


public class MagazziniereWindow extends JFrame{

	private int mag = Sessione.getInstance().getUtenteLoggato().getMagazzino();

	private Ordine_table tabella;
	private Contenuto_Ordine_table tabella2;
	private Rifornimento_table tabella3; 
	private StoricoOrdineMag_table tabella4;
	private Contenuto_StoricoOrdineMag_table tabella5;

	private JScrollPane p;

	private JFrame frame;

	private GridBagConstraints g;

	private GridBagConstraints gbc_pN1 = new GridBagConstraints();
	private GridBagConstraints gbc_pC1 = new GridBagConstraints();
	private GridBagConstraints gbc_pS1= new GridBagConstraints();

	private GridBagConstraints gbc_pC2 = new GridBagConstraints();
	private GridBagConstraints gbc_pS2 = new GridBagConstraints();
	private GridBagConstraints gbc_pP2 = new GridBagConstraints();

	private GridBagConstraints gbc_pC3 = new GridBagConstraints();
	private GridBagConstraints gbc_pS3 = new GridBagConstraints();

	private JButton ordini;
	private JButton rifornimento;
	private JButton storico;
	private JButton logout;
	private JButton cerca;
	private JButton evasione;
	private JButton note;
	private JButton applica;
	private JButton annulla;
	private JButton nuovo;
	private JButton reclamo;
	private JButton aggiorna;
	private JButton annulla_cer;
	
	private JDateChooser data;

	private JTextField textField_1 ;
	private JTextField textField_2 = new JTextField();
	private JTextField textField_3 = new JTextField();
	private JTextField textField_4 = new JTextField();
	private JScrollPane scrollPane = new JScrollPane();

	private JTextArea textArea = new JTextArea();

	//jp1
	private JPanel jpN1 = new JPanel(new GridBagLayout());
	private JPanel jpC1 = new JPanel(new GridBagLayout());
	private JPanel jpS1 = new JPanel(new GridBagLayout());

	//jp2
	private JPanel jpC2 =new JPanel();
	private JPanel jpS2 =new JPanel();


	//jp3
	private JPanel jpC3 = new JPanel(new GridBagLayout());
	private JPanel jpS3 = new JPanel(new GridBagLayout());

	public MagazziniereWindow(){

		super("Utente connesso come: "+Sessione.getInstance().getUtenteLoggato().getNome()+" "+Sessione.getInstance().getUtenteLoggato().getCognome());
		Container c = this.getContentPane();


		MagazziniereListener l = new MagazziniereListener(this);


		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{1200};
		layout.rowHeights = new int[]{50,630,50};
		c.setLayout(layout);

		//ORDINI ---------------------------------------------------------------------------------------------------------------------------
		//NORD ---------------------------------------------------------------------------------------------------------------------------
		GridBagLayout layout_pN1 = new GridBagLayout();
		layout_pN1.columnWidths = new int[]{100,200,100,200,100,200,100};
		jpN1.setLayout(layout_pN1);
		gbc_pN1 = new GridBagConstraints(); 

		gbc_pN1.fill = GridBagConstraints.BOTH;

		ordini = new JButton("Ordini");
		//		ordini.setFont(new Font("Poiret One", Font.BOLD, 20));
		ordini.setEnabled(false);
		ordini.setActionCommand(MagazziniereConstants.ordini);
		ordini.setToolTipText("Visualizza gli ordini");
		ordini.setPreferredSize(new Dimension(ordini.getPreferredSize().width, ordini.getPreferredSize().height*2));
		ordini.addActionListener(l);
		gbc_pN1.gridx = 1;
		gbc_pN1.gridy = 0;
		gbc_pN1.fill = GridBagConstraints.HORIZONTAL;
		jpN1.add(ordini,gbc_pN1);

		rifornimento = new JButton("Rifornimento");
		//		rifornimento.setFont(new Font("Poiret One", Font.BOLD, 20));
		rifornimento.setActionCommand(MagazziniereConstants.rifornimento);
		rifornimento.setToolTipText("Effettua un rifornimento");
		rifornimento.setPreferredSize(new Dimension(rifornimento.getPreferredSize().width, rifornimento.getPreferredSize().height*2));
		rifornimento.addActionListener(l);
		gbc_pN1.gridx = 3;
		gbc_pN1.gridy = 0;
		gbc_pN1.fill = GridBagConstraints.HORIZONTAL;
		jpN1.add(rifornimento,gbc_pN1);			

		storico = new JButton("Storico ordini");
		//		storico.setFont(new Font("Poiret One", Font.BOLD, 20));
		storico.setActionCommand(MagazziniereConstants.storico);
		storico.setToolTipText("Visualizza lo storico ordini");
		storico.setPreferredSize(new Dimension(storico.getPreferredSize().width, storico.getPreferredSize().height*2));
		storico.addActionListener(l);
		gbc_pN1.gridx = 5;
		gbc_pN1.gridy = 0;
		gbc_pN1.fill = GridBagConstraints.HORIZONTAL;
		jpN1.add(storico,gbc_pN1);

		g = new GridBagConstraints();
		g.gridx = 0;
		g.gridy = 0;
		g.fill = GridBagConstraints.BOTH;
		g.weightx = 1.0;
		g.weighty = 1.0;
		c.add(jpN1, g);	

		//CENTRO ---------------------------------------------------------------------------------------------------------------------------------	
		GridBagLayout layout_pC1 = new GridBagLayout();
		layout_pC1.columnWidths = new int[]{200,200,20,600};
		layout_pC1.rowHeights = new int[]{540,50};
		jpC1.setLayout(layout_pC1);
		gbc_pC1 = new GridBagConstraints(); 

		gbc_pC1.fill = GridBagConstraints.BOTH;
		gbc_pC1.weightx = 1.0;
		gbc_pC1.weighty = 1.0;

		tabella2 = new Contenuto_Ordine_table();	
		tabella2.removeColumn(tabella2.getColumn("Magazzino"));
		p = new JScrollPane(tabella2);

		gbc_pC1.gridx = 3;
		gbc_pC1.gridy = 0;
		gbc_pC1.gridheight=2;
		gbc_pC1.fill = GridBagConstraints.BOTH;
		jpC1.add(p,gbc_pC1);

		tabella = new Ordine_table(mag,this);	
		p = new JScrollPane(tabella);

		gbc_pC1.gridx = 0;
		gbc_pC1.gridy = 0;
		gbc_pC1.gridwidth=2;
		gbc_pC1.gridheight=1;
		gbc_pC1.fill = GridBagConstraints.BOTH;
		jpC1.add(p,gbc_pC1);

		evasione = new JButton("Evadi ordine");
		//		evasione.setFont(new Font("Poiret One", Font.BOLD, 20));
		evasione.setActionCommand(MagazziniereConstants.evasione);
		evasione.setToolTipText("Evadi ordine");
		evasione.setPreferredSize(new Dimension(evasione.getPreferredSize().width, evasione.getPreferredSize().height*2));
		evasione.addActionListener(l);
		gbc_pC1.gridx = 0;
		gbc_pC1.gridy = 1;
		gbc_pC1.gridwidth=1;
		gbc_pC1.fill = GridBagConstraints.HORIZONTAL;
		jpC1.add(evasione,gbc_pC1);

		note = new JButton("Note");
		//		note.setFont(new Font("Poiret One", Font.BOLD, 20));
		note.setActionCommand(MagazziniereConstants.note);
		note.setToolTipText("Visualizza Note");
		note.setPreferredSize(new Dimension(note.getPreferredSize().width, note.getPreferredSize().height*2));
		note.addActionListener(l);
		gbc_pC1.gridx = 1;
		gbc_pC1.gridy = 1;
		gbc_pC1.fill = GridBagConstraints.HORIZONTAL;
		jpC1.add(note,gbc_pC1);			

		g.gridx = 0;
		g.gridy=1;
		g.fill = GridBagConstraints.BOTH;
		c.add(jpC1, g);

		//SUD ---------------------------------------------------------------------------------------------------------------------------------	
		GridBagLayout layout_pS1 = new GridBagLayout();
		layout_pS1.columnWidths = new int[]{150,200,400,100,100,150};
		layout_pS1.rowHeights = new int[]{20};
		jpS1.setLayout(layout_pS1);
		gbc_pS1 = new GridBagConstraints(); 

		gbc_pS1.fill = GridBagConstraints.BOTH;

		logout = new JButton("Logout");
		//		logout.setFont(new Font("Poiret One", Font.BOLD, 20));
		logout.setPreferredSize(new Dimension(logout.getPreferredSize().width, logout.getPreferredSize().height*3/2));
		logout.setActionCommand(MagazziniereConstants.logout);
		logout.addActionListener(l);
		gbc_pS1.gridx = 0;
		gbc_pS1.gridy = 0;
		jpS1.add(logout, gbc_pS1);

		data = new JDateChooser();
		data.setDateFormatString("yyyy-MM-dd");
		gbc_pS1.gridx = 2;
		gbc_pS1.gridy = 0;
		gbc_pS1.fill = GridBagConstraints.BOTH;
		jpS1.add(data, gbc_pS1);

		cerca = new JButton("Cerca");
		//		cerca.setFont(new Font("Poiret One", Font.BOLD, 20));
		cerca.setActionCommand(MagazziniereConstants.cerca);
		cerca.addActionListener(l);
		gbc_pS1.gridx = 3;
		gbc_pS1.gridy = 0;
		jpS1.add(cerca, gbc_pS1);		
		
		annulla_cer = new JButton("Mostra Tutti Ordini");
		//		cerca.setFont(new Font("Poiret One", Font.BOLD, 20));
		annulla_cer.setActionCommand(MagazziniereConstants.annulla_cer);
		annulla_cer.addActionListener(l);
		gbc_pS1.gridx = 4;
		gbc_pS1.gridy = 0;
		jpS1.add(annulla_cer, gbc_pS1);	

		g.gridx = 0;
		g.gridy = 2;
		g.gridwidth = 2;
		g.fill = GridBagConstraints.BOTH;
		c.add(jpS1, g);

		//----------------------------------------------------------------------------------------------------------------------------------
		// RIFORNIMENTO --------------------------------------------------------------------------------------------------------------------------		
		//CENTRO ---------------------------------------------------------------------------------------------------------------------------
		GridBagLayout layout_pC2 = new GridBagLayout();
		layout_pC2.columnWidths = new int[]{700,20,1,1,180,20,180,20,180,10};
		layout_pC2.rowHeights = new int[]{540,50};
		jpC2.setLayout(layout_pC2);
		gbc_pC2 = new GridBagConstraints(); 

		gbc_pC2.fill = GridBagConstraints.BOTH;
		gbc_pC2.weightx = 1.0;
		gbc_pC2.weighty = 1.0;

		frame = new JFrame();

		tabella3 = new Rifornimento_table(mag,this);	
		p = new JScrollPane(tabella3);
		gbc_pC2.gridx = 0;
		gbc_pC2.gridy = 0;
		gbc_pC2.gridheight = 2;
		jpC2.add(p, gbc_pC2);

		// pannello		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createLineBorder(Color.gray));
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		GridBagLayout layout_pP2 = new GridBagLayout();
		layout_pP2.columnWidths = new int[]{150,350};
		layout_pP2.rowHeights = new int[]{50,80,80,80,80,100,80};
		panel.setLayout(layout_pP2);
		gbc_pP2 = new GridBagConstraints(); 

		gbc_pP2.fill = GridBagConstraints.HORIZONTAL;
		gbc_pP2.weightx = 1.0;
		gbc_pP2.weighty = 1.0;

		JLabel lblTitolo = new JLabel("Aggiorna Prodotto");
		lblTitolo.setFont(new Font("Poiret One", Font.BOLD, 25));
		lblTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		gbc_pP2.gridx = 0;
		gbc_pP2.gridy = 0;
		gbc_pP2.gridwidth = 2;
		panel.add(lblTitolo, gbc_pP2);

		JLabel lblNome = new JLabel("Nome:");		
		//		lblNome.setFont(new Font("Poiret One", Font.BOLD, 20));
		lblNome.setHorizontalAlignment(SwingConstants.CENTER);
		gbc_pP2.gridx = 0;
		gbc_pP2.gridy = 1;
		gbc_pP2.gridwidth = 1;
		panel.add(lblNome, gbc_pP2);
		
		textField_1 = new JTextField();
		gbc_pP2.gridx = 1;
		gbc_pP2.gridy = 1;
		panel.add(textField_1, gbc_pP2);

		JLabel lblPrezzo = new JLabel("Prezzo:");
		//		lblPrezzo.setFont(new Font("Poiret One", Font.BOLD, 20));
		lblPrezzo.setHorizontalAlignment(SwingConstants.CENTER);
		gbc_pP2.gridx = 0;
		gbc_pP2.gridy = 2;
		panel.add(lblPrezzo, gbc_pP2);
		textField_2 = new JTextField();
		gbc_pP2.gridx = 1;
		gbc_pP2.gridy = 2;
		panel.add(textField_2, gbc_pP2);

		JLabel lblQuantit = new JLabel("Quantita:");
		//		lblQuantit.setFont(new Font("Poiret One", Font.BOLD, 20));
		lblQuantit.setHorizontalAlignment(SwingConstants.CENTER);
		gbc_pP2.gridx = 0;
		gbc_pP2.gridy = 3;
		panel.add(lblQuantit, gbc_pP2);
		textField_3 = new JTextField();
		gbc_pP2.gridx = 1;
		gbc_pP2.gridy = 3;
		panel.add(textField_3, gbc_pP2);

		JLabel lblQuantitMassima = new JLabel("Ordinabili:");
		//		lblQuantitMassima.setFont(new Font("Poiret One", Font.BOLD, 20));
		lblQuantitMassima.setHorizontalAlignment(SwingConstants.CENTER);
		gbc_pP2.gridx = 0;
		gbc_pP2.gridy = 4;
		panel.add(lblQuantitMassima, gbc_pP2);
		textField_4 = new JTextField();
		gbc_pP2.gridx = 1;
		gbc_pP2.gridy = 4;
		panel.add(textField_4, gbc_pP2);

		gbc_pP2.fill = GridBagConstraints.BOTH;

		JLabel lblDescrizione = new JLabel("Descrizione:");
		//		lblDescrizione.setFont(new Font("Poiret One", Font.BOLD, 20));
		lblDescrizione.setHorizontalAlignment(SwingConstants.CENTER);
		gbc_pP2.gridx = 0;
		gbc_pP2.gridy = 5;
		panel.add(lblDescrizione, gbc_pP2);
		scrollPane = new JScrollPane();
		gbc_pP2.gridx = 1;
		gbc_pP2.gridy = 5;
		panel.add(scrollPane, gbc_pP2);
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);

		gbc_pC2.gridx = 4;
		gbc_pC2.gridy = 0;
		gbc_pC2.gridheight = 1;
		gbc_pC2.gridwidth = 5;
		jpC2.add(panel, gbc_pC2);

		applica = new JButton("Applica");
		//		applica.setFont(new Font("Poiret One", Font.BOLD, 20));
		applica.setActionCommand(MagazziniereConstants.applica);
		applica.setToolTipText("Applica modifica prodotto");
		applica.setPreferredSize(new Dimension(applica.getPreferredSize().width, applica.getPreferredSize().height*2));
		applica.addActionListener(l);
		gbc_pC2.gridx = 4;
		gbc_pC2.gridy = 1;
		gbc_pC2.gridwidth = 1;
		//		gbc_pC2.insets = new Insets(0, 20, 0, 0);
		gbc_pC2.fill = GridBagConstraints.HORIZONTAL;		
		jpC2.add(applica,gbc_pC2);

		annulla = new JButton("Reset");
		//		annulla.setFont(new Font("Poiret One", Font.BOLD, 20));
		annulla.setActionCommand(MagazziniereConstants.annulla);
		annulla.setToolTipText("Annulla modifica prodotto");
		annulla.setPreferredSize(new Dimension(annulla.getPreferredSize().width, annulla.getPreferredSize().height*2));
		annulla.addActionListener(l);
		gbc_pC2.gridx = 8;
		gbc_pC2.gridy = 1;
		//		gbc_pC2.insets = new Insets(0, 0, 0, 20);
		gbc_pC2.fill = GridBagConstraints.HORIZONTAL;		
		jpC2.add(annulla,gbc_pC2);		

		nuovo = new JButton("Nuovo");
		//		nuovo.setFont(new Font("Poiret One", Font.BOLD, 20));
		nuovo.setActionCommand(MagazziniereConstants.nuovo);
		nuovo.setToolTipText("Aggiungi nuovo prodotto");
		nuovo.setPreferredSize(new Dimension(nuovo.getPreferredSize().width, nuovo.getPreferredSize().height*2));
		nuovo.addActionListener(l);
		gbc_pC2.gridx = 6;
		gbc_pC2.gridy = 1;
		//		gbc_pC2.insets = new Insets(0, 0, 0, 20);
		gbc_pC2.fill = GridBagConstraints.HORIZONTAL;		
		jpC2.add(nuovo,gbc_pC2);	

		//SUD ---------------------------------------------------------------------------------------------------------------------
		GridBagLayout layout_pS2 = new GridBagLayout();
		layout_pS2.columnWidths = new int[]{150,200,400,100,150,150};
		layout_pS2.rowHeights = new int[]{20};
		jpS2.setLayout(layout_pS2);
		gbc_pS2 = new GridBagConstraints(); 

		gbc_pS2.fill = GridBagConstraints.BOTH;

		logout = new JButton("Logout");
		//		logout.setFont(new Font("Poiret One", Font.BOLD, 20));
		logout.setPreferredSize(new Dimension(logout.getPreferredSize().width, logout.getPreferredSize().height*3/2));
		logout.setActionCommand(MagazziniereConstants.logout);
		logout.addActionListener(l);
		gbc_pS2.gridx = 0;
		gbc_pS2.gridy = 0;
		jpS2.add(logout, gbc_pS2);

		reclamo = new JButton("Reclamo");
		//		reclamo.setFont(new Font("Poiret One", Font.BOLD, 20));
		reclamo.setPreferredSize(new Dimension(reclamo.getPreferredSize().width, reclamo.getPreferredSize().height*3/2));
		reclamo.setActionCommand(MagazziniereConstants.reclamo);
		reclamo.addActionListener(l);
		gbc_pS2.gridx = 5;
		gbc_pS2.gridy = 0;
		jpS2.add(reclamo, gbc_pS2);	


		//----------------------------------------------------------------------------------------------------------------------------------
		// STORICO ORDINI --------------------------------------------------------------------------------------------------------------------------		
		//CENTRO ---------------------------------------------------------------------------------------------------------------------------

		GridBagLayout layout_pC3 = new GridBagLayout();
		layout_pC3.columnWidths = new int[]{30,400,100,50,200,400,30};
		layout_pC3.rowHeights = new int[]{560,20};
		jpC3.setLayout(layout_pC3);
		gbc_pC3 = new GridBagConstraints(); 

		gbc_pC3.fill = GridBagConstraints.BOTH;
		gbc_pC3.weightx = 1.0;
		gbc_pC3.weighty = 1.0;

		frame = new JFrame();
		
		tabella5 = new Contenuto_StoricoOrdineMag_table();
		p = new JScrollPane(tabella5);
		gbc_pC3.gridx = 4;
		gbc_pC3.gridy = 0;
		gbc_pC3.gridwidth = 2;
		jpC3.add(p, gbc_pC3);

		tabella4 = new StoricoOrdineMag_table(mag,this);	
		p = new JScrollPane(tabella4);
		gbc_pC3.gridx = 1;
		gbc_pC3.gridy = 0;
		gbc_pC3.gridwidth = 2;
		jpC3.add(p, gbc_pC3);



		g.gridx = 0;
		g.gridy=1;
		g.fill = GridBagConstraints.BOTH;
		c.add(jpC3, g);

		//SUD ---------------------------------------------------------------------------------------------------------------------------
		GridBagLayout layout_pS3 = new GridBagLayout();
		layout_pS3.columnWidths = new int[]{150,200,400,100,150,150};
		//		layout_pS3.rowHeights = new int[]{20};
		jpS3.setLayout(layout_pS3);
		gbc_pS3 = new GridBagConstraints(); 

		gbc_pS3.fill = GridBagConstraints.BOTH;

		logout = new JButton("Logout");
		//		logout.setFont(new Font("Poiret One", Font.BOLD, 20));
		logout.setPreferredSize(new Dimension(logout.getPreferredSize().width, logout.getPreferredSize().height*3/2));
		logout.setActionCommand(MagazziniereConstants.logout);
		logout.addActionListener(l);
		gbc_pS3.gridx = 0;
		gbc_pS3.gridy = 0;
		jpS3.add(logout, gbc_pS3);

		aggiorna = new JButton("Aggiorna");
		//		aggiorna.setFont(new Font("Poiret One", Font.BOLD, 20));
		aggiorna.setPreferredSize(new Dimension(aggiorna.getPreferredSize().width, aggiorna.getPreferredSize().height*3/2));
		aggiorna.setActionCommand(MagazziniereConstants.aggiorna);
		aggiorna.addActionListener(l);
		gbc_pS3.gridx = 5;
		gbc_pS3.gridy = 0;
		jpS3.add(aggiorna, gbc_pS3);

		this.setSize(1450,800);
		this.setMinimumSize(new Dimension(1450,800));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public GridBagConstraints getG() {
		return g;
	}

	public GridBagConstraints getGbc_jpC2() {
		return gbc_pC2;
	}

	public JPanel getJpS1() {
		return jpS1;
	}

	public JPanel getJpC2() {
		return jpC2;
	}


	public JPanel getJpC1() {
		return jpC1;
	}

	public JButton getOrdini() {
		return ordini;
	}

	public JButton getRifornimento() {
		return rifornimento;
	}

	public JButton getStorico() {
		return storico;
	}

	public JPanel getJpN1(){
		return jpN1;
	}

	public Ordine_table getTabella() {
		return tabella;
	}

	public int getMag() {
		return mag;
	}

	public Contenuto_Ordine_table getTabella2() {
		return tabella2;
	}

	public Rifornimento_table getTabella3() {
		return tabella3;
	}
	
	public JScrollPane getP() {
		return p;
	}

	public JFrame getFrame() {
		return frame;
	}

	public GridBagConstraints getGbc_pN1() {
		return gbc_pN1;
	}

	public GridBagConstraints getGbc_pC1() {
		return gbc_pC1;
	}

	public GridBagConstraints getGbc_pS1() {
		return gbc_pS1;
	}

	public GridBagConstraints getGbc_pC2() {
		return gbc_pC2;
	}

	public JButton getLogout() {
		return logout;
	}

	public JButton getCerca() {
		return cerca;
	}

	public JButton getEvasione() {
		return evasione;
	}

	public JButton getNote() {
		return note;
	}
	
	public JDateChooser getData() {
		return data;
	}

	public JPanel getJpS2() {
		return jpS2;
	}

	public JTextField getTextField_1() {
		return textField_1;
	}

	public JTextField getTextField_2() {
		return textField_2;
	}

	public JTextField getTextField_3() {
		return textField_3;
	}

	public JTextField getTextField_4() {
		return textField_4;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public JPanel getJpC3() {
		return jpC3;
	}

	public JPanel getJpS3() {
		return jpS3;
	}

	public StoricoOrdineMag_table getTabella4() {
		return tabella4;
	}
	
	public Contenuto_StoricoOrdineMag_table getTabella5() {
		return tabella5;
	}
}