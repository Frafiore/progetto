package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import business.OrdineBusiness;
import business.ProgettoBusiness;
import dao.ProgettoDAO;
import model.Ordine;
import model.Progetto;
import view.CapoProgettoView.CapoProgettoWindow;
import view.CapoProgettoView.DettagliprogettoWindow;

public class StatoProgetto_table extends JTable {
	private String[] colonne = new String[]{"Nome Progetto", "Stato","Budget","Budget Speso", "Budget disponibile"};

	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			if(column == 1 || column==2){

				JComponent jc = (JComponent) c;
				jc.setToolTipText(getValueAt(row, column).toString());
			}
		}
		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))
			c.setBackground(new Color(102, 255, 102));
		

		return c;
	}
	
	public StatoProgetto_table(int idCapoProgetto){
		
		final DecimalFormat df = new DecimalFormat("0");
		df.setRoundingMode(RoundingMode.CEILING);

		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		model.setColumnIdentifiers(colonne);
	
		ArrayList<Progetto> elencoProgetti = ProgettoDAO.getInstance().elencoProgetti(idCapoProgetto);
		
		Object[] riga = new Object[5];
		
		for (Iterator iterator = elencoProgetti.iterator(); iterator.hasNext();) {
			Progetto progetto = (Progetto) iterator.next();
			
			riga[0] = progetto.getNome();
			if(progetto.isAttivo())
				riga[1] = "Attivo";
			else
				riga[1] = "Chiuso";
			riga[2] = df.format(progetto.getBudget());
			riga[3] = df.format((float)progetto.getBudget() - (float)ProgettoBusiness.getInstance().caricaBudgetRimanente(progetto.getIdProgetto()));
			riga[4] = df.format(ProgettoBusiness.getInstance().caricaBudgetRimanente(progetto.getIdProgetto()));
			
			model.addRow(riga);
			
		}
		
		this.setModel(model);


		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
		this.setFont(new Font("Arial", Font.ITALIC, 18));

		setRowHeight(21);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setBorder(new EtchedBorder(EtchedBorder.RAISED));
		this.getTableHeader().setReorderingAllowed(false);
		this.getColumnModel().getColumn(0).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(3).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(4).setCellRenderer(riRenderer);
	}
	public void aggiornaTabella(CapoProgettoWindow finestra, int id){
		
		DefaultTableModel model = (DefaultTableModel) finestra.getProgettoTabella().getModel();

		final DecimalFormat df = new DecimalFormat("0");
		df.setRoundingMode(RoundingMode.CEILING);
		
		int righe=model.getRowCount();

		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);
	
		ArrayList<Progetto> elencoProgetti = ProgettoDAO.getInstance().elencoProgetti(id);
		
		Object[] riga = new Object[5];
		
		for (Iterator iterator = elencoProgetti.iterator(); iterator.hasNext();) {
			Progetto progetto = (Progetto) iterator.next();
			
			riga[0] = progetto.getNome();
			if(progetto.isAttivo())
				riga[1] = "Attivo";
			else
				riga[1] = "Chiuso";
			riga[2] = df.format(progetto.getBudget());
			riga[3] = df.format((float)progetto.getBudget() - (float)ProgettoBusiness.getInstance().caricaBudgetRimanente(progetto.getIdProgetto()));
			riga[4] = df.format(ProgettoBusiness.getInstance().caricaBudgetRimanente(progetto.getIdProgetto()));
			
			model.addRow(riga);
			
		}
		finestra.repaint();
		finestra.revalidate();
		
	}
}