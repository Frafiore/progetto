package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import business.OrdineBusiness;
import business.ProgettoBusiness;
import model.Ordine;
import model.Progetto;
import view.CapoProgettoView.CapoProgettoWindow;

public class CapoProgetto_table extends JTable {

	private String[] colonne = new String[]{"IdOrdine","Nome Dipendente", "Spesa totale","Progetto","Data"};

	
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			if(column == 1 || column==2){

				JComponent jc = (JComponent) c;
				jc.setToolTipText(getValueAt(row, column).toString());
			}
		}
		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))
			c.setBackground(new Color(102, 255, 102));


		return c;
	}

	public CapoProgetto_table(int idCapoProgetto, CapoProgettoWindow finestra){

		final DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.CEILING);
		
		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		model.setColumnIdentifiers(colonne);
		
		ArrayList<Ordine> elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

		ArrayList<Progetto> elencoProgetti = ProgettoBusiness.getInstance().elencoProgetti(idCapoProgetto);

		Object[] rigaDati = new Object[5];

		for( int k = 0 ; k<elencoOrdini.size(); k++){
			rigaDati[0] = elencoOrdini.get(k).getIdOrdine();
			rigaDati[1] = elencoOrdini.get(k).getDipendete();
			rigaDati[2] = df.format(elencoOrdini.get(k).getCosto_totale());
			rigaDati[3] = ProgettoBusiness.getInstance().caricaNomeProgetto( elencoOrdini.get(k).getProgetto());
			rigaDati[4] = elencoOrdini.get(k).getData();

			for (Iterator iterator = elencoProgetti.iterator(); iterator.hasNext();) {
				Progetto progetto = (Progetto) iterator.next();

				if(elencoOrdini.get(k).getProgetto() == progetto.getIdProgetto())
					model.addRow(rigaDati);
			}
		}
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

			DefaultTableModel model2 = (DefaultTableModel) finestra.getProgettoTabella().getModel();


			@Override
			public void valueChanged(ListSelectionEvent arg0) {

				if(getSelectedRowCount()!=0){

					Object ordineSelezionato = finestra.getTabella().getValueAt( finestra.getTabella().getSelectedRow(), 3);

					for(int i=0; i < finestra.getProgettoTabella().getRowCount(); i++){
						if(finestra.getProgettoTabella().getValueAt(i, 0).equals(ordineSelezionato))
							finestra.getProgettoTabella().setRowSelectionInterval(i, i);
					}
				}

			}
		});
		this.setModel(model);


		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
		this.setFont(new Font("Arial", Font.ITALIC, 18));

		setRowHeight(21);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.setBorder(new EtchedBorder(EtchedBorder.RAISED));
		this.getTableHeader().setReorderingAllowed(false);
		this.getColumnModel().getColumn(0).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(3).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(4).setCellRenderer(riRenderer);
	}
	public void aggiornaTabellaProgetto(CapoProgettoWindow finestra, int idCapo){
		DefaultTableModel model = (DefaultTableModel)finestra.getTabella().getModel();
		
		final DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.CEILING);
		
		int righe=finestra.getTabella().getModel().getRowCount();
		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);

		ArrayList<Ordine> elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();


		Object[] rigaDati = new Object[5];

		int i =0;
		for( int k = 0 ; k<elencoOrdini.size(); k++){
			rigaDati[0] = elencoOrdini.get(k).getIdOrdine();
			rigaDati[1] = elencoOrdini.get(k).getDipendete();
			rigaDati[2] = df.format(elencoOrdini.get(k).getCosto_totale());
			rigaDati[3] = ProgettoBusiness.getInstance().caricaNomeProgetto( elencoOrdini.get(k).getProgetto());
			rigaDati[4] = elencoOrdini.get(k).getData();

			if(finestra.getProgetto().getSelectedItem().equals(rigaDati[3])){
				model.addRow(rigaDati);
				i++;
			}
				
		}
		if(i==0){
			
			JOptionPane.showMessageDialog(finestra, "Nessun ordine associato al progetto '"+ finestra.getProgetto().getSelectedItem() +"'.", "Attenzione!", JOptionPane.WARNING_MESSAGE);
			aggiornaTabella(finestra, idCapo);
			finestra.getProgetto().setSelectedIndex(0);
		}
			
		finestra.repaint();
		finestra.revalidate();

	}
	public void aggiornaTabella(CapoProgettoWindow finestra,int idCapoProgetto){
		DefaultTableModel model = (DefaultTableModel)finestra.getTabella().getModel();

		final DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.CEILING);
		
		int righe=finestra.getTabella().getModel().getRowCount();
		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);

		ArrayList<Ordine> elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

		ArrayList<Progetto> elencoProgetti = ProgettoBusiness.getInstance().elencoProgetti(idCapoProgetto);

		Object[] rigaDati = new Object[5];

		for( int k = 0 ; k<elencoOrdini.size(); k++){
			rigaDati[0] = elencoOrdini.get(k).getIdOrdine();
			rigaDati[1] = elencoOrdini.get(k).getDipendete();
			rigaDati[2] = df.format(elencoOrdini.get(k).getCosto_totale());
			rigaDati[3] = ProgettoBusiness.getInstance().caricaNomeProgetto( elencoOrdini.get(k).getProgetto());
			rigaDati[4] = elencoOrdini.get(k).getData();

			for (Iterator iterator = elencoProgetti.iterator(); iterator.hasNext();) {
				Progetto progetto = (Progetto) iterator.next();

				if(elencoOrdini.get(k).getProgetto() == progetto.getIdProgetto())
					model.addRow(rigaDati);
			}
		}
		finestra.repaint();
		finestra.revalidate();

	}
}
