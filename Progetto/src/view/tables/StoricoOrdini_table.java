package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import business.CatalogoBusiness;
import business.MagazzinoBusiness;
import business.OrdineBusiness;
import business.ProgettoBusiness;
import business.Sessione;
import model.Contenuto;
import model.Ordine;
import view.MagazziniereView.*;
import view.DipView.DipendenteWindow;

public class StoricoOrdini_table extends JTable {

	private int idDipendente;
	private String[] colonne = new String[]{"idOrdine", "Data","Evasione","Totale", "Note"};
	private ArrayList<Ordine> elencoOrdini;

	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			if(column == 0 || column==1){

				JComponent jc = (JComponent) c;
				jc.setToolTipText(getValueAt(row, column).toString());
			}
		}
		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))
			c.setBackground(new Color(102, 255, 102));

		return c;
	}

	public StoricoOrdini_table(DipendenteWindow finestra){


		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			public boolean isCellEditable(int row, int column) {
				return false;
			}


		};		

		model.setColumnIdentifiers(colonne);

		elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

		Object[] rigaDati = new Object[5];

		for (Iterator iterator = elencoOrdini.iterator(); iterator.hasNext();) {
			Ordine o = (Ordine) iterator.next();

			rigaDati[0]=o.getIdOrdine();
			rigaDati[1]=o.getData();
			if(o.isEvasione())
				rigaDati[2]="Si";
			else
				rigaDati[2] = "No";
			rigaDati[3]=o.getCosto_totale();
			rigaDati[4]=o.getNote();

			if(o.getIdDip()==Sessione.getInstance().getUtenteLoggato().getIdUtente())
				model.addRow(rigaDati);

		}

		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

			DefaultTableModel model2 = (DefaultTableModel) finestra.getContenuto().getModel();


			@Override
			public void valueChanged(ListSelectionEvent arg0) {

				if(model2.getRowCount()!=0){
					int righe=model2.getRowCount();
					for (int i = righe-1; i >= 0; i-- )
						model2.removeRow(i);
				}

				if(getSelectedRowCount()!=0){

					ArrayList<Contenuto> cont = OrdineBusiness.getInstance().caricaContenuto((int)getValueAt(getSelectedRow(), 0));

					Object[] rigaDati2 = new Object[6];
					
					for (Iterator iterator = cont.iterator(); iterator.hasNext();) {
						Contenuto c = (Contenuto) iterator.next();
						rigaDati2[0]=c.getIdArticolo();
						rigaDati2[1]=c.getNome();
						rigaDati2[2]=c.getQuantita();

						if(c.isEvaso())
							rigaDati2[4]="Si";
						else
							rigaDati2[4]="No";
						rigaDati2[3]=c.getPrezzo();

						if(Sessione.getInstance().getUtenteLoggato().getRuolo().equals("D"))
							rigaDati2[5] = MagazzinoBusiness.getInstance().caricaDatiMagazzino(c.getIdMag()).getNome();

						model2.addRow(rigaDati2);
					}
					Ordine o = OrdineBusiness.getInstance().caricaOrdine((int)finestra.getStoricoTable().getValueAt(finestra.getStoricoTable().getSelectedRow(), 0));
					String prog = ProgettoBusiness.getInstance().caricaNomeProgetto(o.getProgetto());
					
					int idProgetto=o.getProgetto();
					float budget = ProgettoBusiness.getInstance().caricaBudgetProgetto(idProgetto);
					float budgetSpesoDa = ProgettoBusiness.getInstance().caricaBudgetSpesoDa(idProgetto, Sessione.getInstance().getUtenteLoggato().getIdUtente());
					float budgetRimanente = ProgettoBusiness.getInstance().caricaBudgetRimanente(idProgetto);
					String text = "<html>STATO PROGETTO ("+prog+"):<br>-Budget progetto (EUR): "+budget+"<br>"
							+ "-Budget speso da te (EUR) : "+budgetSpesoDa+"<br>-Budget disponibile (EUR) : "+budgetRimanente+"</html>";	

					finestra.getStatoProgetto().setText(text);

				}

			}
		});

		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
		this.setFont(new Font("Arial", Font.ITALIC, 18));
		this.setModel(model);

	}
	public void aggiornaStoricoordini_tabella(DipendenteWindow finestra, String progetto){

		DefaultTableModel model = (DefaultTableModel)finestra.getStoricoTable().getModel();
		DefaultTableModel model2 = (DefaultTableModel)finestra.getContenuto().getModel();

		finestra.getStoricoTable().clearSelection();

		int righe2=model2.getRowCount();

		for (int i = righe2-1; i >= 0; i-- )
			model2.removeRow(i);

		int righe=model.getRowCount();

		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);

		model.setColumnIdentifiers(colonne);

		elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

		Object[] rigaDati = new Object[5];

		for (Iterator iterator = elencoOrdini.iterator(); iterator.hasNext();) {
			Ordine o = (Ordine) iterator.next();

			rigaDati[0]=o.getIdOrdine();
			rigaDati[1]=o.getData();
			if(o.isEvasione())
				rigaDati[2]="Si";
			else
				rigaDati[2] = "No";
			rigaDati[3]=o.getCosto_totale();
			rigaDati[4]=o.getNote();

			if(o.getIdDip()==Sessione.getInstance().getUtenteLoggato().getIdUtente()){

				if(progetto!=null && o.getProgetto() == ProgettoBusiness.getInstance().caricaIdProgetto(progetto))
					model.addRow(rigaDati);

				if(progetto==null)
					model.addRow(rigaDati);
			}
		}

		finestra.repaint();
		finestra.revalidate();

	}

}