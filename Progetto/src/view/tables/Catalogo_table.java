package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import business.CatalogoBusiness;
import business.CategoriaBusiness;
import business.MagazzinoBusiness;
import business.Sessione;
import dao.CatalogoDAO;
import model.Articolo;
import model.Categoria;
import view.DipView.DipendenteWindow;

@SuppressWarnings("serial")
public class Catalogo_table extends JTable {


	private int idMagazzino;
	private String[] colonne = new String[]{"IdArticolo","Nome", "Descrizione","Prezzo (EUR)","Ordinabili","Disponibili","Magazzino"}; 
	private ArrayList<Articolo> datiCatalogo;

	private static JLabel mex = new JLabel("Non a� stato trovato nessun articolo della categoria selezionata.");
	final DecimalFormat df = new DecimalFormat("0.00");

	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			if(column == 1 || column==2){

				JComponent jc = (JComponent) c;
				jc.setToolTipText(getValueAt(row, column).toString());
			}
		}
		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))
			c.setBackground(new Color(102, 255, 102));
		return c;
	}


	public Catalogo_table(int idMagazzino){

		this.idMagazzino=idMagazzino;

		df.setRoundingMode(RoundingMode.CEILING);

		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			public boolean isCellEditable(int row, int column) {
				return false;
			}


		};



		model.setColumnIdentifiers(colonne);



		datiCatalogo = CatalogoBusiness.getInstance().caricaArticoli(idMagazzino);

		Object[] rigaDati = new Object[7];
		
		for (Iterator iterator = datiCatalogo.iterator(); iterator.hasNext();) {
			Articolo a = (Articolo) iterator.next();
			rigaDati[0]=a.getIdArticolo();
			rigaDati[1]=a.getNome();
			rigaDati[2]=a.getDescrizione();
			rigaDati[3]=a.getPrezzo();
			rigaDati[4]=a.getQuantita_max_ordi();
			rigaDati[5]=a.getQuantita();
			rigaDati[6]=MagazzinoBusiness.getInstance().caricaDatiMagazzino(idMagazzino).getNome();
			
			model.addRow(rigaDati);
		}

		for( int k = 0 ; k<datiCatalogo.size(); k++){

			

		}

		this.setModel(model);

		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
		this.setFont(new Font("Arial", Font.ITALIC, 18));

		//			setAutoCreateRowSorter(true);

		setRowHeight(21);
		this.setBorder(new EtchedBorder(EtchedBorder.RAISED));

		this.getTableHeader().setReorderingAllowed(false);
		this.getColumnModel().getColumn(0).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(3).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(4).setCellRenderer(riRenderer);

		this.getColumnModel().getColumn(0).setPreferredWidth(5);


	}
	public ArrayList<Articolo> getDatiCatalogo() {
		return datiCatalogo;
	}

	public void aggiornaTabella(DipendenteWindow finestra, String mag,int cat){
		DefaultTableModel model = (DefaultTableModel)finestra.getTabella().getModel();
		
		int righe=finestra.getTabella().getModel().getRowCount();
		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);
		
		ArrayList<Articolo> dati = CatalogoDAO.getInstance().caricaDatiCatalogo(MagazzinoBusiness.getInstance().caricaIdMag(mag));

		for(int i=0;i<dati.size();i++){					
			Object[] rigaDati = new Object[7];


			rigaDati[0]=dati.get(i).getIdArticolo();
			rigaDati[1]=dati.get(i).getNome();
			rigaDati[2]=dati.get(i).getDescrizione();
			rigaDati[3]=dati.get(i).getPrezzo();
			rigaDati[4]=dati.get(i).getQuantita_max_ordi();
			rigaDati[5]=dati.get(i).getQuantita();
			if(mag.equals(null))
				rigaDati[6]=MagazzinoBusiness.getInstance().caricaDatiMagazzino(Sessione.getInstance().getUtenteLoggato().getMagazzino()).getNome();
			else 
				rigaDati[6]=mag;
			if(cat == 0 || cat == dati.get(i).getCategoria())
				model.addRow(rigaDati);
		}
		finestra.repaint();
		finestra.revalidate();

	}
	public void aggiornaCatalogo(DipendenteWindow finestra){
		DefaultTableModel model = (DefaultTableModel)finestra.getTabella().getModel();
		if(model.getRowCount()!=0){
			int righe=model.getRowCount();
			for (int i = righe-1; i >= 0; i-- )
				model.removeRow(i);
		}
		datiCatalogo = CatalogoBusiness.getInstance().caricaArticoli(idMagazzino);

		Object[] rigaDati = new Object[7];

		for( int k = 0 ; k<datiCatalogo.size(); k++){

			rigaDati[0]=datiCatalogo.get(k).getIdArticolo();
			rigaDati[1]=datiCatalogo.get(k).getNome();
			rigaDati[2]=datiCatalogo.get(k).getDescrizione();
			rigaDati[3]=datiCatalogo.get(k).getPrezzo();
			rigaDati[4]=datiCatalogo.get(k).getQuantita_max_ordi();
			rigaDati[5]=datiCatalogo.get(k).getQuantita();
			rigaDati[6]=MagazzinoBusiness.getInstance().caricaDatiMagazzino(idMagazzino).getNome();

			model.addRow(rigaDati);

		}

		finestra.repaint();
		finestra.revalidate();
	}
	public void aggiornaTabellaCategorie(DipendenteWindow finestra, int cat ){
		DefaultTableModel model = (DefaultTableModel)finestra.getTabella().getModel();
		
		int righe=finestra.getTabella().getModel().getRowCount();
		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);
		
		int k=0;		
		for(int i=0;i<datiCatalogo.size();i++){
			if(cat==datiCatalogo.get(i).getCategoria()){

				Object[] rigaDati = new Object[7];

				rigaDati[0]=datiCatalogo.get(i).getIdArticolo();
				rigaDati[1]=datiCatalogo.get(i).getNome();
				rigaDati[2]=datiCatalogo.get(i).getDescrizione();
				rigaDati[3]=datiCatalogo.get(i).getPrezzo();
				rigaDati[4]=datiCatalogo.get(i).getQuantita_max_ordi();
				rigaDati[5]=datiCatalogo.get(i).getQuantita();
				rigaDati[6]=MagazzinoBusiness.getInstance().caricaDatiMagazzino(Sessione.getInstance().getUtenteLoggato().getMagazzino()).getNome();
				k++;
				model.addRow(rigaDati);
			}
		}
		if(k==0){
			finestra.getJpC1().add(mex);
		}

		finestra.repaint();
		finestra.revalidate();

	}

}