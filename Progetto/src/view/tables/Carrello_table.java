package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import business.CalcoloDistanza;
import business.MagazzinoBusiness;
import business.Sessione;
import view.DipView.DipendenteWindow;

@SuppressWarnings("serial")
public class Carrello_table extends JTable{


	private String[] colonne = new String[]{"IdArticolo","Nome", "Descrizione","Prezzo","Ordinabili","Quantita","Magazzino"};

	private float totale;
	private float sommaPrezzi;
	private float spedizione;
	
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			if(column == 1 || column==2){

				JComponent jc = (JComponent) c;
				jc.setToolTipText(getValueAt(row, column).toString());
			}
		}
		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))
			c.setBackground(new Color(102, 255, 102));
		

		return c;
	}



	public  Carrello_table() {

		final DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.CEILING);

		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			public boolean isCellEditable(int row, int column) {
				return false;
			}

		};

		model.setColumnIdentifiers(colonne);

		this.setModel(model);


		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
		this.setFont(new Font("Arial", Font.ITALIC, 18));

		setRowHeight(21);
		this.setBorder(new EtchedBorder(EtchedBorder.RAISED));
		this.getTableHeader().setReorderingAllowed(false);
		this.getColumnModel().getColumn(0).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(3).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(4).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(5).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(6).setCellRenderer(riRenderer);

	}
	public void aggiornaSommario(DipendenteWindow finestra){
		
		DefaultTableModel model = (DefaultTableModel) finestra.getTabella2().getModel();
		

		sommaPrezzi=0;
		spedizione = 0;

		float latDip = MagazzinoBusiness.getInstance().caricaDatiMagazzino(Sessione.getInstance().getUtenteLoggato().getMagazzino()).getLat();
		float longDip = MagazzinoBusiness.getInstance().caricaDatiMagazzino(Sessione.getInstance().getUtenteLoggato().getMagazzino()).getLongit();

		int l=0,m=0,n=0;
		
		for(int i=0; i<finestra.getTabella2().getRowCount();i++){

			float x = (float) model.getValueAt(i, 3);
			int y = (int)model.getValueAt(i, 5);

			sommaPrezzi+=x*y;

			int idMag = MagazzinoBusiness.getInstance().caricaIdMag((String)model.getValueAt(i, 6));
			float latMagArt = MagazzinoBusiness.getInstance().caricaDatiMagazzino(idMag).getLat();
			float longMagArt = MagazzinoBusiness.getInstance().caricaDatiMagazzino(idMag).getLongit();
			
			double distanza =CalcoloDistanza.calculateDist(latDip, longDip, latMagArt, longMagArt);
			
			if(distanza/1000 >10 && distanza/1000 <=200)
				l++;
			
			else if(distanza/1000 >200 && distanza/1000 <=1000)
				m++;
			
			else if(distanza/1000 >1000)
				n++;
		}
		//se esiste almeno un articolo entro 20 km aggiungi 5 euro alla spedizione
		if(l>0)
			spedizione+=5;
		//entro 100 km aggiungi 15 euro
		if(m>0)
			spedizione+=15;
		//oltre i 1000 km aggiungi 25 euro
		if(n>0)
			spedizione+=25;
		
		
		totale = sommaPrezzi + spedizione;

		finestra.getSommario().setText("Costo (EUR): "+sommaPrezzi+"    +     	Spedizione (EUR): "+spedizione+"     = 	 	Totale (EUR): "+totale);

	}
	public void svuotaCarrello(DipendenteWindow finestra){
		DefaultTableModel model = (DefaultTableModel) finestra.getTabella2().getModel();
		
		int righe=model.getRowCount();
		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);
		
		finestra.getTabella2().aggiornaSommario(finestra);
		finestra.getConfermaAcquisto().setEnabled(false);
		finestra.getSvuota().setEnabled(false);
		finestra.getDelSelected().setEnabled(false);
		
		finestra.repaint();
		finestra.revalidate();
	}

	public float getTotale() {
		return totale;
	}



	public float getSommaPrezzi() {
		return sommaPrezzi;
	}



	public float getSpedizione() {
		return spedizione;
	}

}