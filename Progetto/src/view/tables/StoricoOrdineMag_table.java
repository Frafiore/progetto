package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import business.CatalogoBusiness;
import business.OrdineBusiness;
import business.Sessione;
import model.Contenuto;
import model.Ordine;
import view.MagazziniereView.*;
import view.DipView.DipendenteWindow;

public class StoricoOrdineMag_table extends JTable {

	private int idMagazzino;
	private String[] colonne = new String[]{"idOrdine", "Data","Dipendente","Note"};
	private ArrayList<Ordine> elencoOrdini;

	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			if(column == 0 || column==1){

				JComponent jc = (JComponent) c;
				jc.setToolTipText(getValueAt(row, column).toString());
			}
		}
		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))
			c.setBackground(new Color(102, 255, 102));

		return c;
	}

	public StoricoOrdineMag_table(int idMagazzino, MagazziniereWindow finestra){

		this.idMagazzino=idMagazzino;
		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			boolean[] columnEditables = new boolean[] {
					false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return false;
			}


		};		

		model.setColumnIdentifiers(colonne);

		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

		Object[] rigaDati = new Object[5];

		for( int k = 0 ; k<elencoOrdini.size(); k++){

			rigaDati[0]=elencoOrdini.get(k).getIdOrdine();
			rigaDati[1]=elencoOrdini.get(k).getData();
			rigaDati[2]=elencoOrdini.get(k).getDipendete();
			rigaDati[3]=elencoOrdini.get(k).getNote();

			ArrayList<Contenuto> con = OrdineBusiness.getInstance().caricaContenuto((int)rigaDati[0]);
			
			boolean x=false;
			
			for (Iterator iterator = con.iterator(); iterator.hasNext();) {
				Contenuto c = (Contenuto) iterator.next();
				if(c.isEvaso() && c.getIdMag() == Sessione.getInstance().getUtenteLoggato().getMagazzino())
					x=true;
			}
			
			if(x)
				model.addRow(rigaDati);
		}

		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
		this.setFont(new Font("Arial", Font.ITALIC, 18));

		setRowHeight(21);
		this.setBorder(new EtchedBorder(EtchedBorder.RAISED));

		this.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
		
			DefaultTableModel model3 = (DefaultTableModel) finestra.getTabella5().getModel();

			@Override
			public void valueChanged(ListSelectionEvent a) {

				if(finestra.getTabella5().getRowCount()!=0){
					int righe=model3.getRowCount();
					for (int i = righe-1; i >= 0; i-- )
						model3.removeRow(i);
				}
				
				if(getSelectedRow()!=-1){
					@SuppressWarnings("unused")
					int x= (int)getValueAt(getSelectedRow(), 0);

					ArrayList<Contenuto> cont = OrdineBusiness.getInstance().caricaContenuto((int)getValueAt(getSelectedRow(), 0));

					Object[] rigaDati2 = new Object[5];

					for( int k = 0 ; k<cont.size(); k++){

						rigaDati2[0]=cont.get(k).getIdArticolo();
						rigaDati2[1]=cont.get(k).getNome();
						rigaDati2[2]=cont.get(k).getQuantita();
						rigaDati2[3]=cont.get(k).getPrezzo();

						if(cont.get(k).isEvaso()==false){
							rigaDati2[4]="NO";
						} else {
							rigaDati2[4]="SI";
						}

						if(cont.get(k).getIdMag()==Sessione.getInstance().getUtenteLoggato().getMagazzino())
							model3.addRow(rigaDati2);

					}

				}
			}
		});

		this.setModel(model);			
	}

	public void AggiornaTabellaMag(MagazziniereWindow finestra){

		DefaultTableModel model = (DefaultTableModel)finestra.getTabella4().getModel();
		DefaultTableModel model2 = (DefaultTableModel)finestra.getTabella5().getModel();

		int righe2=model2.getRowCount();

		for (int i = righe2-1; i >= 0; i-- )
			model2.removeRow(i);


		int righe=model.getRowCount();

		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);

		model.setColumnIdentifiers(colonne);

		elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

		Object[] rigaDati = new Object[4];

		for( int k = 0 ; k<elencoOrdini.size(); k++){

			rigaDati[0]=elencoOrdini.get(k).getIdOrdine();
			rigaDati[1]=elencoOrdini.get(k).getData();
			rigaDati[2]=elencoOrdini.get(k).getDipendete();
			rigaDati[3]=elencoOrdini.get(k).getNote();

			ArrayList<Contenuto> con = OrdineBusiness.getInstance().caricaContenuto((int)rigaDati[0]);
			
			boolean x=false;
			
			for (Iterator iterator = con.iterator(); iterator.hasNext();) {
				Contenuto c = (Contenuto) iterator.next();
				if(c.isEvaso() && c.getIdMag() == Sessione.getInstance().getUtenteLoggato().getMagazzino())
					x=true;
			}
			
			if(x)
				model.addRow(rigaDati);
		}
		
		finestra.repaint();
		finestra.revalidate();

		JOptionPane.showMessageDialog(null, "Aggiornato");
	}
}








