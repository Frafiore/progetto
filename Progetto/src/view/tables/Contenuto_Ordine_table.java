package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import model.Contenuto;

public class Contenuto_Ordine_table extends JTable {
	
	private int idMagazzino;
	private String[] colonne = new String[]{"idArticolo","Nome","Quantita","Prezzo","Evaso", "Magazzino"};
	private ArrayList<Contenuto> elencoContenutoOrdini;
	
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);
        if (c instanceof JComponent) {
           if(column == 0 || column==1){
            
            JComponent jc = (JComponent) c;
            jc.setToolTipText(getValueAt(row, column).toString());
           }
        }
        if(row % 2 == 0)
        	c.setBackground(new Color(215, 215, 215));
        else
        	c.setBackground(Color.white);
        return c;
    }

	public Contenuto_Ordine_table(){
		
		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
    	riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
    	
			DefaultTableModel model = new DefaultTableModel(){
				
	        	public boolean isCellEditable(int row, int column) {
					return false;
				}
	        	
				
			};		
			
			model.setColumnIdentifiers(colonne);
			this.getTableHeader().setBackground(new Color(153, 255, 229));
			this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
			this.setFont(new Font("Arial", Font.ITALIC, 18));
			this.setModel(model);
			this.getColumnModel().getColumn(0).setCellRenderer(riRenderer);
			this.getColumnModel().getColumn(1).setCellRenderer(riRenderer);
			this.getColumnModel().getColumn(2).setCellRenderer(riRenderer);
			this.getColumnModel().getColumn(3).setCellRenderer(riRenderer);
			this.getColumnModel().getColumn(4).setCellRenderer(riRenderer);
			this.getColumnModel().getColumn(5).setCellRenderer(riRenderer);
			
	}

}
