package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.sun.glass.events.MouseEvent;

import business.CatalogoBusiness;
import business.MagazzinoBusiness;
import business.OrdineBusiness;
import business.Sessione;
import model.Articolo;
import model.Contenuto;
import view.MagazziniereView.*;
import view.DipView.DipendenteWindow;

@SuppressWarnings("serial")
public class Rifornimento_table extends JTable {

	private int idMagazzino;
	private int idArticolo;
	private String[] colonne = new String[]{"IdArticolo","Nome", "Descrizione","Prezzo (EUR)","Quantita","Ordinabili"}; 
	private ArrayList<Articolo> datiCatalogo;

	final DecimalFormat df = new DecimalFormat("0.00");

	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		if (c instanceof JComponent) {
			if(column == 1 || column==2){

				JComponent jc = (JComponent) c;
				jc.setToolTipText(getValueAt(row, column).toString());
			}
		}

		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))	
			c.setBackground(new Color(102, 255, 102));

		int q=6;

		if((int)getValueAt(row, 4) < q)
			c.setBackground(new Color(244,164,96));	
		return c;
	}


	public Rifornimento_table(int idMagazzino , MagazziniereWindow finestra){

		this.idMagazzino=idMagazzino;

		df.setRoundingMode(RoundingMode.CEILING);

		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			public boolean isCellEditable(int row, int column) {
				return false;
			}


		};

		model.setColumnIdentifiers(colonne);

		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		datiCatalogo = CatalogoBusiness.getInstance().caricaRifornimento(idMagazzino);

		Object[] rigaDati = new Object[6];

		for( int k = 0 ; k<datiCatalogo.size(); k++){

			rigaDati[0]=datiCatalogo.get(k).getIdArticolo();
			rigaDati[1]=datiCatalogo.get(k).getNome();
			rigaDati[2]=datiCatalogo.get(k).getDescrizione();
			rigaDati[3]=datiCatalogo.get(k).getPrezzo();
			rigaDati[4]=datiCatalogo.get(k).getQuantita();
			rigaDati[5]=datiCatalogo.get(k).getQuantita_max_ordi();

			model.addRow(rigaDati);

		}		

		this.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

			@Override
			public void valueChanged(ListSelectionEvent arg0) {

				
				if(getSelectedRowCount()!=0){
					int i = finestra.getTabella3().getSelectedRow();
					
					finestra.getTextField_1().setText(finestra.getTabella3().getValueAt(i, 1).toString());
					finestra.getTextArea().setText(finestra.getTabella3().getValueAt(i, 2).toString());
					finestra.getTextField_2().setText(finestra.getTabella3().getValueAt(i, 3).toString());
					finestra.getTextField_3().setText(finestra.getTabella3().getValueAt(i, 4).toString());
					finestra.getTextField_4().setText(finestra.getTabella3().getValueAt(i, 5).toString());			
	
				}
			}
		});


		this.setModel(model);

		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
		this.setFont(new Font("Arial", Font.ITALIC, 18));

		setRowHeight(21);
		this.setBorder(new EtchedBorder(EtchedBorder.RAISED));

		this.getTableHeader().setReorderingAllowed(false);
		this.getColumnModel().getColumn(0).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(3).setCellRenderer(riRenderer);


		this.getColumnModel().getColumn(0).setPreferredWidth(5);

	}

	public void aggiornaRifornimento_table(MagazziniereWindow finestra){
		
		DefaultTableModel model = (DefaultTableModel)finestra.getTabella3().getModel();
		
		finestra.getTabella3().clearSelection();
		
		int righe=finestra.getTabella3().getRowCount();
		for (int i = righe-1; i >= 0; i-- )
			model.removeRow(i);
		
		datiCatalogo = CatalogoBusiness.getInstance().caricaRifornimento(idMagazzino);

		Object[] rigaDati = new Object[6];

		for( int k = 0 ; k<datiCatalogo.size(); k++){

			rigaDati[0]=datiCatalogo.get(k).getIdArticolo();
			rigaDati[1]=datiCatalogo.get(k).getNome();
			rigaDati[2]=datiCatalogo.get(k).getDescrizione();
			rigaDati[3]=datiCatalogo.get(k).getPrezzo();
			rigaDati[4]=datiCatalogo.get(k).getQuantita();
			rigaDati[5]=datiCatalogo.get(k).getQuantita_max_ordi();

			model.addRow(rigaDati);

			finestra.repaint();
			finestra.revalidate();
		}
	}
}