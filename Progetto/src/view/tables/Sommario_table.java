package view.tables;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

@SuppressWarnings("serial")
public class Sommario_table extends JTable{

	String[] colonne = new String[]{"IdArticolo","Nome","Magazzino","Prezzo * Quantita" , "Totale (EUR)"};
	
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		Component c = super.prepareRenderer(renderer, row, column);
		
		if(row % 2 == 0)
			c.setBackground(new Color(215, 215, 215));
		else
			c.setBackground(Color.white);
		if(isCellSelected(row, column))
			c.setBackground(new Color(102, 255, 102));

		return c;
	}



	public Sommario_table() {


		DefaultTableCellRenderer riRenderer= new DefaultTableCellRenderer();
		riRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);


		DefaultTableModel model = new DefaultTableModel(){

			public boolean isCellEditable(int row, int column) {
				return false;
			}

		};

		model.setColumnIdentifiers(colonne);


		this.setModel(model);

		this.getTableHeader().setReorderingAllowed(false);
		this.getTableHeader().setBackground(new Color(153, 255, 229));
		this.getTableHeader().setFont(new Font("Arial", Font.BOLD, 15));
		this.setFont(new Font("Arial", Font.ITALIC, 13));

		setRowHeight(21);
		this.setBorder(new EtchedBorder(EtchedBorder.RAISED));
		
		this.getColumnModel().getColumn(0).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(3).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(4).setCellRenderer(riRenderer);
		this.getColumnModel().getColumn(2).setCellRenderer(riRenderer);

	}
	
}

