package view.DipView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import business.Sessione;
import dao.ProgettoDAO;
import listeners.ConfermaOrdineListener;
import listeners.constants.ConfermaOrdineConstants;
import model.Progetto;
import view.tables.Sommario_table;

@SuppressWarnings("serial")
public class ConfermaOrdineWindow extends JFrame {

	private Sommario_table sommario;
	private DipendenteWindow finestra;
	
	private JComboBox<String> progetto;
	private JLabel sommarioLabel;
	private JTextArea note;
	
	private JButton annulla;
	private JButton conferma;
	private JCheckBox stampaDistinta;
	
	private float sommaPrezzi;
	private float spedizione;
	private float totale;

	public ConfermaOrdineWindow(DipendenteWindow finestra){
		super("Conferma ordine");

		this.finestra = finestra;

		DefaultTableModel model = (DefaultTableModel) finestra.getTabella2().getModel();

		ConfermaOrdineListener l = new ConfermaOrdineListener(this);
		
		Container c = this.getContentPane();
		GridBagConstraints g = new GridBagConstraints();

		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{200,300,100,200};
		layout.rowHeights = new int[]{20,150,150,50,100,30};
		c.setLayout(layout);

		JLabel mex = new JLabel("SOMMARIO DI SPESA");
		mex.setHorizontalAlignment(SwingConstants.CENTER);
		mex.setFont(new Font("", Font.BOLD, 18));
		g.gridx = 0;
		g.gridy =0;
		g.gridwidth =4;
		g.fill = GridBagConstraints.BOTH;
		g.insets = new Insets(10, 5, 5, 5);

		c.add(mex , g);

		//Tabella sommario----------------------------------------------------------------------------
		sommario = new Sommario_table();
		DefaultTableModel model2 = (DefaultTableModel) sommario.getModel();

		if(finestra.getTabella2().getRowCount() != 0){

			for (int i =0; i < finestra.getTabella2().getRowCount(); i++){

				Object[] rigaDati = new Object[6];

				rigaDati[0] = finestra.getTabella2().getValueAt(i, 0);
				rigaDati[1] = finestra.getTabella2().getValueAt(i, 1);
				rigaDati[2] = finestra.getTabella2().getValueAt(i, 6);
				rigaDati[3] = finestra.getTabella2().getValueAt(i, 3)+" x "+finestra.getTabella2().getValueAt(i, 5);
				rigaDati[4] = (float)model.getValueAt(i, 3)*(int) model.getValueAt(i, 5);

				model2.addRow(rigaDati);
			}
		}

		JScrollPane p = new JScrollPane(sommario);

		g.gridx = 0;
		g.gridy = 1;
		g.gridwidth = 4;
		g.fill = GridBagConstraints.BOTH;

		c.add(p, g);

		//jlabel sommario----------------------------------------------------------

		sommaPrezzi =finestra.getTabella2().getSommaPrezzi();
		spedizione =finestra.getTabella2().getSpedizione();
		totale =finestra.getTabella2().getTotale();

		sommarioLabel = new JLabel();
		sommarioLabel.setFont(new Font("", Font.ITALIC, 16));
		sommarioLabel.setText("<html>Costo (EUR):	 <b>"+sommaPrezzi+"</b><br>+ Spedizione (EUR):	 <b>"+spedizione+"</b><br>= Totale (EUR): 	<b>"+totale+"</b></html>");

		g.gridx = 0;
		g.gridy =2;
		g.gridwidth = 0;
		g.fill = GridBagConstraints.BOTH;
		c.add(sommarioLabel, g);

		//text area per le note--------------------------------------------------------------

		note=new JTextArea("Note...");
		note.setMaximumSize(new Dimension(600, 150));
		note.setLineWrap(true);
		note.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if(note.getText().equals(""))
					note.setText("Note...");
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(note.getText().equals("Note..."))
					note.setText("");
				else
					note.selectAll();

			}
		});

		JScrollPane scroll = new JScrollPane (note, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		g.gridx = 1;
		g.gridy = 2;
		g.gridwidth = 3;
		g.fill = GridBagConstraints.BOTH;
		c.add(scroll, g);



		//jlabel inserire il progetto-----------------------------------------------------------

		JLabel mex2 = new JLabel("Scegliere il progetto a cui associare la spesa: ");
		mex2.setHorizontalAlignment(SwingConstants.CENTER);
		mex2.setFont(new Font("", Font.ITALIC, 16));
		g.gridx = 0;
		g.gridy =3;
		g.gridwidth = 2;
		c.add(mex2, g);

		//combobox dei progetti------------------------------------------------------------------

		progetto = new JComboBox<>();

		ArrayList<Progetto> elenco = ProgettoDAO.getInstance().elencoProgetti(Sessione.getInstance().getUtenteLoggato().getIdUtente());

		for (int i=0; i<elenco.size(); i++){
			if(elenco.get(i).isAttivo())
				progetto.addItem(elenco.get(i).getNome());
		}

		progetto.setAlignmentX(SwingConstants.CENTER);
		g.gridx = 2;
		g.gridy =3;
		g.gridwidth = 2;
		g.fill = GridBagConstraints.HORIZONTAL;
		c.add(progetto, g);

		
		//bottone annulla----------------------------------------------------------------------
		
		annulla = new JButton("Annulla");
		annulla.setActionCommand(ConfermaOrdineConstants.annulla);
		annulla.addActionListener(l);
		g.gridx =0;
		g.gridy = 5;
		g.gridwidth =1;
		c.add(annulla, g);
		
		//bottone conferma---------------------------------------------------------------------
		
		conferma=new JButton("Conferma");
		conferma.setActionCommand(ConfermaOrdineConstants.conferma);
		conferma.addActionListener(l);
		g.gridx =3;
		g.gridy = 5;
		g.gridwidth =1;
		c.add(conferma, g);
		
		//checkbox stampa distinta------------------------------------------------------------------
		
		stampaDistinta = new JCheckBox("Stampa distinta");
		g.gridx =2;
		g.gridy = 5;
		g.gridwidth =1;
		c.add(stampaDistinta, g);
		
		
		
		pack();

		this.setSize(850, 600);
		this.setMinimumSize(new Dimension(850, 600));
		this.setMaximumSize(new Dimension(850, 600));
		
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	public Sommario_table getSommario() {
		return sommario;
	}

	public DipendenteWindow getFinestra() {
		return finestra;
	}

	public JComboBox<String> getProgetto() {
		return progetto;
	}

	public JLabel getSommarioLabel() {
		return sommarioLabel;
	}

	public JTextArea getNote() {
		return note;
	}

	public JButton getAnnulla() {
		return annulla;
	}

	public JButton getConferma() {
		return conferma;
	}

	public float getSommaPrezzi() {
		return sommaPrezzi;
	}

	public float getSpedizione() {
		return spedizione;
	}

	public float getTotale() {
		return totale;
	}

	public JCheckBox getStampaDistinta() {
		return stampaDistinta;
	}
}
