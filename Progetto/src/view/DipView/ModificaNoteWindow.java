package view.DipView;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import business.OrdineBusiness;

public class ModificaNoteWindow extends JFrame {

	private JTextArea area;
	private JButton conferma;
	private JButton annulla;
	
	public ModificaNoteWindow(DipendenteWindow finestra){
		super("Modifica le note...");
		
		Container c = this.getContentPane();

		
		GridBagLayout lay = new GridBagLayout();
		lay.columnWidths = new int[]{200,200};
		lay.rowHeights = new int[]{200, 80};
		
		GridBagConstraints g = new GridBagConstraints();
		c.setLayout(lay);
		area = new JTextArea((String) finestra.getStoricoTable().getValueAt(finestra.getStoricoTable().getSelectedRow(), 4));
		area.setFont(new Font("", Font.PLAIN, 18));
		
		JScrollPane scroll = new JScrollPane(area);
		g.gridx=0;
		g.gridy = 0;
		g.gridwidth =2;
		g.fill = GridBagConstraints.BOTH;
		c.add(scroll,g);
		
		conferma = new JButton("Conferma");
		conferma.setFont(new Font("", Font.PLAIN, 18));
		conferma.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean x = OrdineBusiness.getInstance().modificaData(area.getText(), (int) finestra.getStoricoTable().getValueAt(finestra.getStoricoTable().getSelectedRow(), 0));
				
				finestra.getStoricoTable().aggiornaStoricoordini_tabella(finestra, null);
				finestra.repaint();
				finestra.revalidate();
				setVisible(false);
			}
		});
		g.gridx=1;
		g.gridy=1;
		g.gridwidth=1;
		c.add(conferma,g);
		
		annulla = new JButton("Annulla");
		annulla.setFont(new Font("", Font.PLAIN, 18));
		annulla.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				setVisible(false);
			}
		});
		g.gridx=0;
		g.gridy=1;
		g.gridwidth=1;
		c.add(annulla,g);
		
		this.setVisible(true);
		this.getRootPane().setDefaultButton(conferma);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(new Dimension(420,350));
		this.setMinimumSize(new Dimension(420,350));
		this.setLocationRelativeTo(finestra);
	}
}
