package view.DipView;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import business.CategoriaBusiness;
import business.MagazzinoBusiness;
import business.Sessione;
import dao.ProgettoDAO;
import listeners.DipendenteListener;
import listeners.constants.DipendenteConstants;
import model.Categoria;
import model.Magazzino;
import model.Progetto;
import view.tables.Carrello_table;
import view.tables.Catalogo_table;
import view.tables.Contenuto_Ordine_table;
import view.tables.StoricoOrdini_table;

@SuppressWarnings("serial")
public class DipendenteWindow extends JFrame{


	private String sede = Sessione.getInstance().getUtenteLoggato().getSede();
	private int mag = Sessione.getInstance().getUtenteLoggato().getMagazzino();

	//jp1
	private JPanel jpW1 = new JPanel();
	private JPanel jpN1 = new JPanel();
	private JPanel jpC1 = new JPanel(new BorderLayout());
	private JPanel jpS1 = new JPanel();

	private Catalogo_table tabella;
	private JScrollPane p;

	private GridBagConstraints gbc_pw1 = new GridBagConstraints();

	private GridBagConstraints gbc_ps1;

	private GridBagConstraints g;
	private JButton carrello;
	private JButton storico;
	private JButton catalogo;
	private JComboBox<String> magazzini;
	private JComboBox<String> categorie;
	private JButton applicaFiltri;
	private JButton resetFiltri;
	private JButton logout;
	private JButton cerca;
	private JButton addToCart;
	private JButton delSelected;
	private JButton svuota;
	private JButton confermaAcquisto;

	JSpinner spinner;

	private JButton piu;
	private JButton meno;

	private GridBagConstraints gbc_jpc2;
	private JLabel sommario;
	private JCheckBox check = new JCheckBox("Cerca in tutti i magazzini");

	private Carrello_table tabella2;

	private JTextField ricerca = new JTextField(30);

	//jp2
	private JPanel jpC2 =new JPanel();
	private JPanel jpS2 =new JPanel();


	//	//jp3
	private JPanel jpC3 =new JPanel();
	private JPanel jpS3 =new JPanel();

	private JComboBox<String> progetto;
	private Contenuto_Ordine_table contenuto;
	private StoricoOrdini_table storicoTable;

	private JButton aggiorna;
	private JButton visualizzaNote;
	private JButton modificaNote;
	private JButton stampaDistinta;
	private JLabel statoProgetto;


	public DipendenteWindow() {

		super("Utente connesso come: "+Sessione.getInstance().getUtenteLoggato().getNome()+" "+Sessione.getInstance().getUtenteLoggato().getCognome()+".");
		Container c = this.getContentPane();


		DipendenteListener l = new DipendenteListener(this);


		GridBagLayout layout = new GridBagLayout();
		layout.columnWidths = new int[]{150,1150};
		layout.rowHeights = new int[]{50,650, 80};
		layout.columnWeights = new double[]{0.0, 1.0, 1.0, Double.MIN_VALUE};
		layout.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};

		c.setLayout(layout);

		jpW1.setLayout(new GridBagLayout());

		carrello = new JButton("Carrello");
		carrello.setFont(new Font("", Font.PLAIN,18));
		carrello.setActionCommand(DipendenteConstants.carrello);
		carrello.setToolTipText("Visualizza il carrello");
		carrello.setPreferredSize(new Dimension(carrello.getPreferredSize().width, carrello.getPreferredSize().height*2));
		carrello.addActionListener(l);
		gbc_pw1.gridx = 0;
		gbc_pw1.gridy = 1;
		gbc_pw1.weighty=1;
		gbc_pw1.fill = GridBagConstraints.HORIZONTAL;
		jpW1.add(carrello,gbc_pw1);

		storico = new JButton("Storico ordini");
		storico.setFont(new Font("", Font.PLAIN,18));
		storico.setActionCommand(DipendenteConstants.storico);
		storico.setToolTipText("Visualizza lo storico ordini");
		storico.setPreferredSize(new Dimension(storico.getPreferredSize().width, storico.getPreferredSize().height*2));
		storico.addActionListener(l);
		gbc_pw1.gridx = 0;
		gbc_pw1.gridy = 2;
		gbc_pw1.fill = GridBagConstraints.HORIZONTAL;
		jpW1.add(storico,gbc_pw1);

		catalogo = new JButton("Catalogo");
		catalogo.setFont(new Font("", Font.PLAIN,18));
		catalogo.setActionCommand(DipendenteConstants.catalogo);
		catalogo.setToolTipText("Visualizza il catalogo del magazzino pia� vicino a te.");
		catalogo.setPreferredSize(new Dimension(catalogo.getPreferredSize().width, catalogo.getPreferredSize().height*2));
		catalogo.addActionListener(l);
		gbc_pw1.gridx = 0;
		gbc_pw1.gridy = 0;
		gbc_pw1.fill = GridBagConstraints.HORIZONTAL;
		jpW1.add(catalogo,gbc_pw1);
		catalogo.setEnabled(false);

		logout = new JButton("Logout");
		logout.setFont(new Font("", Font.PLAIN,18));
		logout.setPreferredSize(new Dimension(logout.getPreferredSize().width, logout.getPreferredSize().height*3/2));
		logout.setActionCommand(DipendenteConstants.logout);
		logout.addActionListener(l);
		gbc_pw1.gridx = 0;
		gbc_pw1.gridy = 3;
		gbc_pw1.fill = GridBagConstraints.HORIZONTAL;
		jpW1.add(logout,gbc_pw1);

		tabella = new Catalogo_table(mag);
		tabella.getColumnModel().getColumn(0).setPreferredWidth(30);

		p = new JScrollPane(tabella);

		//aggiungo elementi creati al pannello jpc1
		jpC1.add(p, BorderLayout.CENTER);



		//creo gli elementi del pannello jpS1

		GridBagLayout layout_ps1 = new GridBagLayout();
		layout_ps1.columnWidths = new int[]{150,400,20,100,20,100,130,50,30,150};
		jpS1.setLayout(layout_ps1);
		gbc_ps1 = new GridBagConstraints(); 

		ricerca.setText("Cerca...");
		ricerca.setFont(new Font("", Font.PLAIN,18));
		ricerca.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				getRootPane().setDefaultButton(null);
				if(ricerca.getText().equals(""))
					ricerca.setText("Cerca...");
			}

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				ricerca.setText("");
				getRootPane().setDefaultButton(cerca);
			}
		});

		gbc_ps1.gridx = 1;
		gbc_ps1.gridy = 0;
		gbc_ps1.fill = GridBagConstraints.BOTH;
		jpS1.add(ricerca, gbc_ps1);

		cerca = new JButton("Cerca");
		cerca.setFont(new Font("", Font.PLAIN,18));
		cerca.setPreferredSize(new Dimension(cerca.getPreferredSize().width, cerca.getPreferredSize().height*5/3));
		cerca.setActionCommand(DipendenteConstants.cerca);
		cerca.addActionListener(l);
		gbc_ps1.gridx = 3;
		gbc_ps1.gridy = 0;
		jpS1.add(cerca, gbc_ps1);

		gbc_ps1.gridx = 5;
		jpS1.add(check, gbc_ps1);

		spinner  = new JSpinner();
		spinner.setFont(new Font("", Font.PLAIN,18));
		spinner.setValue(1);
		gbc_ps1.gridx = 7;
		jpS1.add(spinner, gbc_ps1);

		addToCart = new JButton("Aggiungi al carrello");
		addToCart.setFont(new Font("", Font.PLAIN,18));
		addToCart.setActionCommand(DipendenteConstants.addToCart);
		addToCart.addActionListener(l);
		gbc_ps1.gridx = 9;
		gbc_ps1.gridy = 0;
		jpS1.add(addToCart, gbc_ps1);
		
		GridBagLayout lay = new GridBagLayout();
		lay.columnWidths = new int[]{212,150,138,150,162,100,50,100,88};
		jpN1.setLayout(lay);
		GridBagConstraints g2 = new GridBagConstraints();
		
		magazzini = new JComboBox<>();
		
		ArrayList<Magazzino> mags = MagazzinoBusiness.getInstance().caricaElencoMagazzini();
		magazzini.addItem("Seleziona il magazzino..");
		
		
		for (Iterator iterator = mags.iterator(); iterator.hasNext();) {
			Magazzino m = (Magazzino) iterator.next();

			magazzini.addItem(m.getNome());
			
			if(m.getIdmag()==mag)
				magazzini.setSelectedItem(m.getNome());
		}

		magazzini.setFont(new Font("", Font.PLAIN,18));
		
		g2.gridx=1;
		g2.fill = GridBagConstraints.BOTH;
		
		jpN1.add(magazzini,g2);
		

		categorie = new JComboBox<>();
		
		ArrayList<Categoria> cat = CategoriaBusiness.getInstance().getCategorie();
		categorie.addItem("Tutte le categorie..");
		for(int i = 0; i<cat.size();i++)
			categorie.addItem(cat.get(i).getNome());

		categorie.setFont(new Font("", Font.PLAIN,18));
		
		g2.gridx=3;
		g2.fill = GridBagConstraints.BOTH;
		
		jpN1.add(categorie,g2);
		
		applicaFiltri = new JButton("Applica filtri");
		applicaFiltri.setFont(new Font("", Font.PLAIN,18));
		applicaFiltri.setActionCommand(DipendenteConstants.applica);
		applicaFiltri.addActionListener(l);
		
		g2.gridx=5;
		g2.fill = GridBagConstraints.BOTH;
		
		jpN1.add(applicaFiltri, g2);
		
		resetFiltri = new JButton("Reset");
		resetFiltri.setFont(new Font("", Font.PLAIN,18));
		resetFiltri.setActionCommand(DipendenteConstants.reset);
		resetFiltri.addActionListener(l);

		g2.gridx=7;
		g2.fill = GridBagConstraints.BOTH;
		
		jpN1.add(resetFiltri, g2);
		
		
		//aggiungo jpW1
		g = new GridBagConstraints();
		g.gridx = 0;
		g.gridy = 0;
		g.gridheight =3;
		g.fill = GridBagConstraints.BOTH;
		c.add(jpW1, g);

		//aggiungo jpC1
		g.gridx = 1;
		g.gridy=1;
		g.gridheight =1;
		g.weightx = 1.0;
		g.weighty = 1.0;
		g.fill = GridBagConstraints.BOTH;
		c.add(jpC1, g);

		//aggiungo jpS1
		g.gridx = 1;
		g.gridy = 2;
		g.gridwidth = 1;
		g.weightx = 0.0;
		g.weighty = 0.0;
		g.fill = GridBagConstraints.BOTH;
		c.add(jpS1, g);

		//aggiungo jpN1
		g.gridx = 1;
		g.gridy = 0;
		g.gridwidth = 1;
		g.weightx = 0.0;
		g.weighty = 0.0;
		g.fill = GridBagConstraints.BOTH;
		c.add(jpN1, g);
		

		//creo il pannello jpc2

		BorderLayout layout_jpC2 = new BorderLayout();
		jpC2.setLayout(layout_jpC2);

		tabella2 = new Carrello_table();


		JScrollPane p2 = new JScrollPane(tabella2);

		jpC2.add(p2, BorderLayout.CENTER);

		sommario = new JLabel();
		sommario.setFont(new Font("", Font.BOLD, 18));
		sommario.setBackground(Color.white);
		sommario.setHorizontalAlignment(SwingConstants.CENTER);

		jpC2.add(sommario, BorderLayout.SOUTH);
		//creo il pannello jpS2


		GridBagLayout layout_jpS2 = new GridBagLayout();
		layout_jpS2.columnWidths = new int[]{100,180,30,10,10,180,200,50,200,50,200,60};
		layout_jpS2.rowHeights = new int[]{20,30};
		jpS2.setLayout(layout_jpS2);

		GridBagConstraints gbc_jps2 = new GridBagConstraints(); 

		//bottone elimina selezione______________________________________________________________________________

		delSelected = new JButton("Elimina selezionato");
		delSelected.setFont(new Font("", Font.PLAIN,18));
		delSelected.setPreferredSize(new Dimension(delSelected.getPreferredSize().width, delSelected.getPreferredSize().height*3/2));	
		delSelected.setActionCommand(DipendenteConstants.delSel);
		delSelected.addActionListener(l);

		gbc_jps2.gridx = 6;
		gbc_jps2.gridwidth = 1;
		gbc_jps2.gridheight = 2;
		gbc_jps2.fill = GridBagConstraints.BOTH;
		jpS2.add(delSelected, gbc_jps2);


		//bottone svuota______________________________________________________________________________

		svuota = new JButton("Svuota carrello");
		svuota.setFont(new Font("", Font.PLAIN,18));
		svuota.setPreferredSize(new Dimension(svuota.getPreferredSize().width, svuota.getPreferredSize().height*3/2));	
		svuota.setActionCommand(DipendenteConstants.svuota);
		svuota.addActionListener(l);

		gbc_jps2.gridx = 8;
		jpS2.add(svuota, gbc_jps2);

		//bottone conferma acquisto______________________________________________________________________________

		confermaAcquisto = new JButton("Conferma ordine");
		confermaAcquisto.setFont(new Font("", Font.PLAIN,18));
		confermaAcquisto.setPreferredSize(new Dimension(confermaAcquisto.getPreferredSize().width, confermaAcquisto.getPreferredSize().height*3/2));	
		confermaAcquisto.setActionCommand(DipendenteConstants.confermaOrdine);
		confermaAcquisto.addActionListener(l);

		gbc_jps2.gridx = 10;
		jpS2.add(confermaAcquisto, gbc_jps2);

		//label modifica quantita______________________________________________________________________________
		JLabel modQua= new JLabel("Modifica quantita");
		modQua.setFont(new Font("", Font.PLAIN,18));
		modQua.setHorizontalAlignment(SwingConstants.CENTER);
		gbc_jps2.gridx = 2;
		gbc_jps2.gridy = 0;
		gbc_jps2.gridwidth=3;
		gbc_jps2.gridheight = 1;
		jpS2.add(modQua, gbc_jps2);


		//bottone + ______________________________________________________________________________
		piu = new JButton("+");
		piu.setFont(new Font("", Font.PLAIN,18));
		piu.setPreferredSize(new Dimension(piu.getPreferredSize().width*4/3, piu.getPreferredSize().height));
		piu.setActionCommand(DipendenteConstants.piu);
		piu.addActionListener(l);

		gbc_jps2.gridx=2;
		gbc_jps2.gridy=1;
		gbc_jps2.gridwidth=1;
		jpS2.add(piu, gbc_jps2);

		//bottone - ______________________________________________________________________________
		meno = new JButton("-");
		meno.setFont(new Font("", Font.PLAIN,18));
		meno.setPreferredSize(new Dimension(meno.getPreferredSize().width*4/3, meno.getPreferredSize().height));
		meno.setActionCommand(DipendenteConstants.meno);
		meno.addActionListener(l);

		gbc_jps2.gridx=4;
		gbc_jps2.gridwidth=1;
		jpS2.add(meno, gbc_jps2);


		//creo il pannello di centro 3_________________________________________________________________________________________
		//____________________________________________________________________________________________________________________


		GridBagLayout layout3 = new GridBagLayout();
		layout3.columnWidths = new int[]{150,50,20,80,30,80,50,600,50};
		layout3.rowHeights = new int[]{30,400,5,40,5,150,20,50,30};
		jpC3.setLayout(layout3);

		GridBagConstraints gbc_jpc3 = new GridBagConstraints();

		contenuto = new Contenuto_Ordine_table();

		gbc_jpc3.gridx =7;
		gbc_jpc3.gridy =1;
		gbc_jpc3.gridwidth = 1;
		gbc_jpc3.gridheight = 7;
		gbc_jpc3.weightx = 1.0;
		gbc_jpc3.weighty = 1.0;
		gbc_jpc3.fill = GridBagConstraints.BOTH;
		JScrollPane scroll2 = new JScrollPane(contenuto);
		jpC3.add(scroll2,gbc_jpc3);

		storicoTable = new StoricoOrdini_table(this);

		gbc_jpc3.gridx =0;
		gbc_jpc3.gridy =1;
		gbc_jpc3.gridwidth = 6;
		gbc_jpc3.gridheight = 1;
		gbc_jpc3.weightx = 0.0;
		gbc_jpc3.weighty = 0.0;
		JScrollPane scroll3 = new JScrollPane(storicoTable);

		jpC3.add(scroll3,gbc_jpc3);

		stampaDistinta = new JButton("Stampa distinta ordine");
		stampaDistinta.setFont(new Font("", Font.PLAIN,18));
		stampaDistinta.setPreferredSize(new Dimension(stampaDistinta.getPreferredSize().width, stampaDistinta.getPreferredSize().height*3/2));
		stampaDistinta.setActionCommand(DipendenteConstants.stampaDist);
		stampaDistinta.addActionListener(l);
		gbc_jpc3.fill = GridBagConstraints.HORIZONTAL;
		gbc_jpc3.gridx =0;
		gbc_jpc3.gridy =7;
		gbc_jpc3.gridwidth = 1;
		gbc_jpc3.gridheight = 1;

		jpC3.add(stampaDistinta, gbc_jpc3);

		visualizzaNote = new JButton("Visualizza note");
		visualizzaNote.setFont(new Font("", Font.PLAIN,18));
		visualizzaNote.setPreferredSize(new Dimension(visualizzaNote.getPreferredSize().width, visualizzaNote.getPreferredSize().height*3/2));
		visualizzaNote.setActionCommand(DipendenteConstants.visNote);
		visualizzaNote.addActionListener(l);
		//		gbc_jpc3.fill = GridBagConstraints.BOTH;
		gbc_jpc3.gridx =2;
		gbc_jpc3.gridy =7;
		gbc_jpc3.gridwidth = 2;
		gbc_jpc3.gridheight = 1;
		jpC3.add(visualizzaNote, gbc_jpc3);
		
		modificaNote = new JButton("Modifica note");
		modificaNote.setFont(new Font("", Font.PLAIN,18));
		modificaNote.setPreferredSize(new Dimension(modificaNote.getPreferredSize().width, modificaNote.getPreferredSize().height*3/2));
		modificaNote.setActionCommand(DipendenteConstants.modNote);
		modificaNote.addActionListener(l);
		//		gbc_jpc3.fill = GridBagConstraints.BOTH;
		gbc_jpc3.gridx =5;
		gbc_jpc3.gridy =7;
		gbc_jpc3.gridwidth = 1;
		gbc_jpc3.gridheight = 1;

		jpC3.add(modificaNote, gbc_jpc3);

		
		
		//		creo la combobox____________________________________________

		progetto = new JComboBox<>();
		ArrayList<Progetto>  elencoProgetti = ProgettoDAO.getInstance().elencoProgetti(Sessione.getInstance().getUtenteLoggato().getIdUtente());

		progetto.addItem("Seleziona il progetto...");
		for(int i = 0; i<elencoProgetti.size();i++)
			progetto.addItem(elencoProgetti.get(i).getNome());

		progetto.setFont(new Font("", Font.PLAIN,18));
		gbc_jpc3.fill = GridBagConstraints.BOTH;
		gbc_jpc3.gridx =0;
		gbc_jpc3.gridy =3;
		gbc_jpc3.gridwidth = 2;
		gbc_jpc3.gridheight = 1;

		jpC3.add(progetto, gbc_jpc3);

		//		creo pulsante di aggiornamento_________________________________________________________

		aggiorna = new JButton("Aggiorna");
		aggiorna.setFont(new Font("", Font.PLAIN,18));
		aggiorna.setActionCommand(DipendenteConstants.aggiorna);
		aggiorna.addActionListener(l);
		gbc_jpc3.fill = GridBagConstraints.BOTH;
		gbc_jpc3.gridx =3;
		gbc_jpc3.gridy =3;
		gbc_jpc3.gridwidth = 2;
		gbc_jpc3.gridheight = 1;

		jpC3.add(aggiorna, gbc_jpc3);
		
		statoProgetto = new JLabel("");
		statoProgetto.setFont(new Font("", Font.ITALIC, 30));
		statoProgetto.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		statoProgetto.setAlignmentY(JLabel.CENTER_ALIGNMENT);
		gbc_jpc3.fill = GridBagConstraints.BOTH;
		gbc_jpc3.gridx =0;
		gbc_jpc3.gridy =5;
		gbc_jpc3.gridwidth = 6;
		gbc_jpc3.gridheight = 2;
		jpC3.add(statoProgetto, gbc_jpc3);

		pack();
		this.setExtendedState(this.getExtendedState());
		this.setSize(1600,830);
		this.setMinimumSize(new Dimension(1600,830));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}



	public String getSede() {
		return sede;
	}



	public int getMagazzino() {
		return mag;
	}



	public JPanel getJpW1() {
		return jpW1;
	}



	public JPanel getJpC1() {
		return jpC1;
	}



	public JPanel getJpS1() {
		return jpS1;
	}



	public JScrollPane getP() {
		return p;
	}



	public GridBagConstraints getGbc_pw1() {
		return gbc_pw1;
	}



	public JButton getCarrello() {
		return carrello;
	}



	public JButton getStorico() {
		return storico;
	}



	public JButton getCatalogo() {
		return catalogo;
	}



	public JButton getLogout() {
		return logout;
	}



	public JButton getCerca() {
		return cerca;
	}



	public JButton getAddToCart() {
		return addToCart;
	}



	public JTextField getRicerca() {
		return ricerca;
	}

	public Catalogo_table getTabella() {
		return tabella;
	}

	public GridBagConstraints getGbc_ps1() {
		return gbc_ps1;
	}



	public JPanel getJpC3() {
		return jpC3;
	}



	public JPanel getJpS3() {
		return jpS3;
	}



	public JComboBox<String> getProgetto() {
		return progetto;
	}



	public JLabel getStatoProgetto() {
		return statoProgetto;
	}



	public Contenuto_Ordine_table getContenuto() {
		return contenuto;
	}



	public JButton getStampaDistinta() {
		return stampaDistinta;
	}



	public JComboBox<String> getMagazzini() {
		return magazzini;
	}



	public JComboBox<String> getCategorie() {
		return categorie;
	}



	public JButton getApplicaFiltri() {
		return applicaFiltri;
	}



	public GridBagConstraints getG() {
		return g;
	}



	public JCheckBox getCheck() {
		return check;
	}



	public int getMag() {
		return mag;
	}



	public JButton getDelSelected() {
		return delSelected;
	}



	public JButton getSvuota() {
		return svuota;
	}



	public JButton getConfermaAcquisto() {
		return confermaAcquisto;
	}



	public JPanel getJpC2() {
		return jpC2;
	}



	public JPanel getJpS2() {
		return jpS2;
	}



	public Carrello_table getTabella2() {
		return tabella2;
	}



	public GridBagConstraints getGbc_jpc2() {
		return gbc_jpc2;
	}



	public JLabel getSommario() {
		return sommario;
	}
	public StoricoOrdini_table getStoricoTable() {
		return storicoTable;
	}



	public JSpinner getSpinner() {
		return spinner;
	}



	public JButton getPiu() {
		return piu;
	}



	public JButton getMeno() {
		return meno;
	}

	public JButton getAggiorna() {
		return aggiorna;
	}

	public JButton getVisualizzaNote() {
		return visualizzaNote;
	}

	public JButton getModificaNote() {
		return modificaNote;
	}



	public JPanel getJpN1() {
		return jpN1;
	}
}