package listeners;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;
import business.ArticoloProdottoBusiness;
import business.CategoriaBusiness;
import business.FornitoreBusiness;
import business.ProduttoreBusiness;
import business.Sessione;
import model.Articolo;
import model.Fornitore;
import model.Produttore;
import view.MagazziniereView.*;

import java.awt.event.ActionEvent;

public class NuovoListener implements ActionListener{

	private NuovoWindow finestra;
	private MagazziniereWindow magWindow;

	public NuovoListener(NuovoWindow finestra) {
		this.finestra = finestra;
		this.magWindow = finestra.getFinestra();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("1")){

			if(finestra.getNome_t().getText().equals("") && finestra.getFornitore_t().equals("Seleziona...") && finestra.getCategoria_t().equals("Seleziona...") && finestra.getProduttore_t().equals("Seleziona...") && finestra.getOrdinabili_t().getText().equals("") && finestra.getPrezzo_t().getText().equals("") && finestra.getQuantita_t().getText().equals("") && finestra.getTextArea().getText().equals("")){
				JOptionPane.showMessageDialog(null, "I campi per inserire i Prodotti sono gia vuoti!",null, JOptionPane.INFORMATION_MESSAGE);
			}else{
				finestra.getNome_t().setText("");
				finestra.getOrdinabili_t().setText("");
				finestra.getPrezzo_t().setText("");
				finestra.getQuantita_t().setText("");
				finestra.getTextArea().setText("");
				finestra.getFornitore_t().setSelectedIndex(0);
				finestra.getProduttore_t().setSelectedIndex(0);
				finestra.getCategoria_t().setSelectedIndex(0);
			}

		}

		else if(e.getActionCommand().equals("2")){

			if(finestra.getNome_t().getText().equals("") || finestra.getFornitore_t().getSelectedIndex()==0 || finestra.getProduttore_t().getSelectedIndex()==0 || finestra.getCategoria_t().getSelectedIndex()==0 || finestra.getOrdinabili_t().getText().equals("") && finestra.getPrezzo_t().getText().equals("") || finestra.getQuantita_t().getText().equals("") || finestra.getTextArea().getText().equals("")){
				JOptionPane.showMessageDialog(null, "Alcuni campi sono vuoti.\nInserisca i valori!",null, JOptionPane.INFORMATION_MESSAGE);
			} else {

				try{

					String nome = finestra.getNome_t().getText();
					int ordinabili =  Integer.parseInt(finestra.getOrdinabili_t().getText());
					float prezzo = Float.parseFloat(finestra.getPrezzo_t().getText());
					int quantita = Integer.parseInt(finestra.getQuantita_t().getText());
					String descrizione = finestra.getTextArea().getText();
					int fornitore = 0;
					int produttore = 0;
					int categoria=CategoriaBusiness.getInstance().getIdCategoria((String) finestra.getCategoria_t().getSelectedItem());

					ArrayList<Produttore>  elencoProd = ProduttoreBusiness.getInstance().getProduttore();

					for (Iterator iterator = elencoProd.iterator(); iterator.hasNext();) {
						Produttore p = (Produttore) iterator.next();
						if(p.getNome().equals((String)finestra.getProduttore_t().getSelectedItem()))
							produttore=p.getidProduttore();
					}

					ArrayList<Fornitore> elencoForn = FornitoreBusiness.getInstance().caricaFornitori();

					for (Iterator iterator = elencoForn.iterator(); iterator.hasNext();) {
						Fornitore f = (Fornitore) iterator.next();
						String nome_forn = f.getNome() + " " + f.getCognome();
						if(nome_forn.equals((String)finestra.getFornitore_t().getSelectedItem()))
							fornitore=f.getidFornitore();
					}


					Articolo a = new Articolo(nome, descrizione, prezzo, quantita, ordinabili, categoria, 0, 0, produttore); 			

					int idMagazzino = Sessione.getInstance().getUtenteLoggato().getMagazzino();
					ArticoloProdottoBusiness.getInstance().caricaNuovo(idMagazzino,a,fornitore);		
					
					finestra.setVisible(false);
					finestra.dispose();
					
					magWindow.getTabella3().aggiornaRifornimento_table(magWindow);
					
					JOptionPane.showMessageDialog(finestra, "Articolo aggiunto", "Esito positivo", JOptionPane.INFORMATION_MESSAGE);
					

				} catch (NumberFormatException e2) {

					JOptionPane.showMessageDialog(finestra, "Valori inserito non permesso.\nPrego ricontrollare i parametri inseriti.", "Attenzione", JOptionPane.ERROR_MESSAGE);

				}
			}
		}
	}
}
