package listeners;

import business.Sessione;
import business.UtenteBusiness;
import view.LoginWindow;
import view.MagazziniereView.*;
import view.RecuperoPwdWindow;
import view.CapoProgettoView.CapoProgettoWindow;
import view.DipView.DipendenteWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class LoginListener implements ActionListener {

	private LoginWindow loginWindow;

	public LoginListener(LoginWindow loginWindow) {
		super();
		this.loginWindow = loginWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		String codiceUtente = loginWindow.getCodiceUtente().getText();
		char[] pwd = loginWindow.getPassword().getPassword();
		String password = new String(pwd);



		if(e.getActionCommand().equals("1")){
			if((codiceUtente.length() != 0) && (password.length()!=0)){

				//controllo che sia il codice e la password non contengano il carattere ' per prevenire la sql injection
				if(codiceUtente.indexOf("'")==-1 && password.indexOf("'")==-1){

					try {
						boolean utenteEsiste = UtenteBusiness.getInstance().verificaLogin(Integer.parseInt(codiceUtente), password);
						if(utenteEsiste){
							Sessione.getInstance().creaSessione(Integer.parseInt(codiceUtente), password);
							String ruolo = Sessione.getInstance().getUtenteLoggato().getRuolo();

							switch(ruolo){

							case"M": new MagazziniereWindow();
							break;
							case "D" : new DipendenteWindow();
							break;
							case "C": new CapoProgettoWindow();
							break;			
							}
							loginWindow.setVisible(false);
						}
						else
							JOptionPane.showMessageDialog(loginWindow, "Utente non riconosciuto");
					} catch (NumberFormatException e2) {
						// TODO: handle exception
						JOptionPane.showMessageDialog(loginWindow, "Codice utente inserito non valido.","Errore", JOptionPane.ERROR_MESSAGE);
					}


				}
				else
					JOptionPane.showMessageDialog(loginWindow, "Non � ammesso il carattere '.\nQuesto evento verra segnalato all'amministratore del sistema!");


			}
			else
				JOptionPane.showMessageDialog(loginWindow, "Codice utente o password non inseriti.\n\nInseriscili, grazie.");
		}
		else if (e.getActionCommand().equals("2")){

			new RecuperoPwdWindow();

			loginWindow.setVisible(false);
			loginWindow.dispose();
		}


	}

}