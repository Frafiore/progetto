package listeners;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.swing.JOptionPane;

import business.EmailManager;
import business.RandomString;
import business.UtenteBusiness;
import view.LoginWindow;
import view.RecuperoPwdWindow;

public class RecuperoPwdListener implements ActionListener {

	private RecuperoPwdWindow finestra;

	public RecuperoPwdListener(RecuperoPwdWindow finestra) {
		super();
		this.finestra = finestra;
	}
	String codiceDiVerifica = RandomString.getInstance().randomString(8);

	@Override
	public void actionPerformed(ActionEvent e) {



		if("1".equals(e.getActionCommand())){
			codiceDiVerifica = RandomString.getInstance().randomString(8);
			String oggetto = "Codice per modifica password";
			String codiceUtente = finestra.getCodiceUtente().getText();
			try {

				String email = UtenteBusiness.getInstance().getEmaiFromDataBase(Integer.parseInt(codiceUtente));String emailBody = "Questo e' il codice da inserire per poter cambiare la password: <b> " + codiceDiVerifica +"<b>."+ "<br><br> Se hai ricevuto questa email per sbaglio, ti preghiamo di ignorarla. <br><br>Cordialmente, <br> FrancescoAlfonso Project.";

				if(email == null)
					JOptionPane.showMessageDialog(finestra, "Codice utente non corretto");
				else{

					try {
						EmailManager.getInstance().sendEmail(email, oggetto,emailBody);
						JOptionPane.showMessageDialog(finestra, "E' stata inviata un'email con il codice di verifica all'indirizzo "+email+".\nInseriscilo nella prossima finestra.");

					} catch (AddressException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (MessagingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}


					//togli jpN1 dal Nord
					BorderLayout n=(BorderLayout)finestra.getContentPane().getLayout();
					finestra.getContentPane().remove(n.getLayoutComponent(BorderLayout.NORTH));

					//togli jpC1 dal centro
					BorderLayout c=(BorderLayout)finestra.getContentPane().getLayout();
					finestra.getContentPane().remove(c.getLayoutComponent(BorderLayout.CENTER));

					//togli jpS1 dal sud
					BorderLayout s=(BorderLayout)finestra.getContentPane().getLayout();
					finestra.getContentPane().remove(s.getLayoutComponent(BorderLayout.SOUTH));

					//aggiungi jpN2
					finestra.getContentPane().add(finestra.getJpN2(), BorderLayout.NORTH);
					finestra.repaint();
					finestra.revalidate();

					//aggiungi jpC2
					finestra.getContentPane().add(finestra.getJpC2(), BorderLayout.CENTER);
					finestra.repaint();
					finestra.revalidate();

					//aggiungi jpS2
					finestra.getContentPane().add(finestra.getJpS2(), BorderLayout.SOUTH);
					finestra.repaint();
					finestra.revalidate();

					finestra.getRootPane().setDefaultButton(finestra.getConferma2());
				}
			} catch (NumberFormatException e2) {
				// TODO: handle exception
				JOptionPane.showMessageDialog(finestra, "Codice non valido");

			}



		}
		else if("3".equals(e.getActionCommand())){

			if(finestra.getCodiceDiVerifica().getText() ==null){
				JOptionPane.showMessageDialog(finestra,"Inserire il codice di verifica.");
			}

			else if(codiceDiVerifica.equals(finestra.getCodiceDiVerifica().getText())){

				//togli jpN2 dal Nord
				BorderLayout n=(BorderLayout)finestra.getContentPane().getLayout();
				finestra.getContentPane().remove(n.getLayoutComponent(BorderLayout.NORTH));

				//togli jpC2 dal centro
				BorderLayout c=(BorderLayout)finestra.getContentPane().getLayout();
				finestra.getContentPane().remove(c.getLayoutComponent(BorderLayout.CENTER));

				//togli jpS2 dal sud
				BorderLayout s=(BorderLayout)finestra.getContentPane().getLayout();
				finestra.getContentPane().remove(s.getLayoutComponent(BorderLayout.SOUTH));

				//aggiungi jpN3
				finestra.getContentPane().add(finestra.getJpN3(), BorderLayout.NORTH);
				finestra.repaint();
				finestra.revalidate();

				//aggiungi jpC3
				finestra.getContentPane().add(finestra.getJpC3(), BorderLayout.CENTER);
				finestra.repaint();
				finestra.revalidate();

				//aggiungi jpS3
				finestra.getContentPane().add(finestra.getJpS3(), BorderLayout.SOUTH);
				finestra.repaint();
				finestra.revalidate();
				finestra.getRootPane().setDefaultButton(finestra.getConferma3());
			}
			else
				JOptionPane.showMessageDialog(finestra, "Codice di verifica errato.");

		}
		else if("4".equals(e.getActionCommand())){
			codiceDiVerifica = RandomString.getInstance().randomString(8);
			String oggetto = "Codice per modifica password";
			String codiceUtente = finestra.getCodiceUtente().getText();
			String email = UtenteBusiness.getInstance().getEmaiFromDataBase(Integer.parseInt(codiceUtente));
			String emailBody = "Questo e' il codice da inserire per poter cambiare la password: <b> " + codiceDiVerifica +"<b>."+ "<br><br> Se hai ricevuto questa email per sbaglio, ti preghiamo di ignorarla. <br><br>Cordialmente, <br> FrancescoAlfonso Project.";
			try {
				EmailManager.getInstance().sendEmail(email, oggetto,emailBody);
			} catch (AddressException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (MessagingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			JOptionPane.showMessageDialog(finestra, "E' stata inviata un'altra email con il  nuovo codice di verifica all'indirizzo "+email+".");

		}
		else if("7".equals(e.getActionCommand())){

			char[] nPwd = finestra.getNew_pwd1().getPassword();
			char[] nPwd2 = finestra.getNew_pwd2().getPassword();

			String newPassword = new String(nPwd);
			String newPassword2 = new String(nPwd2);
			String codiceUtente = finestra.getCodiceUtente().getText();

			if(newPassword==null || newPassword2 == null){
				JOptionPane.showMessageDialog(finestra, "Completare entrambi i campi.");
			}
			else if(newPassword.equals(newPassword2)){

				int idUtente= UtenteBusiness.getInstance().getIdFromDataBase(Integer.parseInt(codiceUtente));
				boolean cambiamento = UtenteBusiness.getInstance().changePassword(Integer.parseInt(codiceUtente), idUtente, newPassword);

				if(cambiamento==true){
					JOptionPane.showMessageDialog(finestra, "Password modificata con successo.");
					new LoginWindow();
					finestra.setVisible(false);
				}
				else
					JOptionPane.showMessageDialog(finestra, "Problema cambiamneto password.");
			}
			else if(!newPassword.equals(newPassword2)){
				JOptionPane.showMessageDialog(finestra, "Le password inserite non corrispondono.");
			}

		}
		else if("2".equals(e.getActionCommand()) || "6".equals(e.getActionCommand()) || "5".equals(e.getActionCommand())){

			new LoginWindow();
			finestra.setVisible(false);

		}

	}




}
