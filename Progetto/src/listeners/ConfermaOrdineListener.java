package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import business.OrdineBusiness;
import business.ProgettoBusiness;
import business.Sessione;
import business.StampaDistinta;
import model.Contenuto;
import model.Ordine;
import model.Utente;
import view.DipView.ConfermaOrdineWindow;
import view.DipView.DipendenteWindow;
import view.tables.Carrello_table;

public class ConfermaOrdineListener implements ActionListener {

	private ConfermaOrdineWindow finestra;
	private DipendenteWindow dipWindow;


	public ConfermaOrdineListener(ConfermaOrdineWindow finestra) {
		// TODO Auto-generated constructor stub
		this.finestra = finestra;
		this.dipWindow = finestra.getFinestra();
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		Carrello_table carrello = dipWindow.getTabella2();
		//listener annulla_____________________________________________________________________
		if(e.getActionCommand().equals("1")){
			finestra.setVisible(false);
			finestra.dispose();
		}
		//listener conferma_____________________________________________________________________
		else if(e.getActionCommand().equals("2")){
			float totale = carrello.getTotale();
			int idProgetto = ProgettoBusiness.getInstance().caricaIdProgetto((String)finestra.getProgetto().getSelectedItem());
			float restante = ProgettoBusiness.getInstance().caricaBudgetRimanente(idProgetto);
			if(totale<restante){
//				inserisco l'ordine nel database
				int idOrdine = OrdineBusiness.getInstance().caricaUltimoid();
				if(finestra.getStampaDistinta().isSelected()){
					try {
						JFileChooser fc = new JFileChooser();
						int ans = fc.showSaveDialog(finestra);
						if(ans==JFileChooser.APPROVE_OPTION){

							boolean x = OrdineBusiness.getInstance().inserisciOrdine(finestra, dipWindow.getTabella2());

							String path = fc.getSelectedFile().getPath();
							String nomeFilePDF = path+".pdf";

							Utente u = Sessione.getInstance().getUtenteLoggato();
							Ordine o = OrdineBusiness.getInstance().caricaOrdine(idOrdine);
							ArrayList<Contenuto> c = OrdineBusiness.getInstance().caricaContenuto(idOrdine);
							ArrayList<String> mag = OrdineBusiness.getInstance().getElencoMagazziniInteressati(c);

							StampaDistinta.getInstance().stampaDistinta(u, o,c, mag, nomeFilePDF, finestra);
							
							JOptionPane.showMessageDialog(dipWindow, "Ordine inviato con successo.", "Avviso",JOptionPane.INFORMATION_MESSAGE);

							finestra.setVisible(false);
							finestra.dispose();

							carrello.svuotaCarrello(dipWindow);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						JOptionPane.showMessageDialog(null, "Errore nell'inserimento dell'ordine", "Errore", JOptionPane.ERROR_MESSAGE);
					}
				}
				else{
					boolean x = OrdineBusiness.getInstance().inserisciOrdine(finestra, dipWindow.getTabella2());
					if(x)
						JOptionPane.showMessageDialog(dipWindow, "Ordine inviato con successo.", "Avviso",JOptionPane.INFORMATION_MESSAGE);
					finestra.setVisible(false);
					finestra.dispose();

					carrello.svuotaCarrello(dipWindow);
				}
			}
			else{
				JOptionPane.showMessageDialog(finestra, "Attenzione!!\n\nL'importo dell'ordine desiderato supera il budget disponibile del progetto associato.\nPer favore elimina qualche articolo oppure contatta il capo progetto, grazie.", "Attenzione", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}








