package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import DbInterface.DbConnection;
import business.MagazzinoBusiness;
import business.OrdineBusiness;
import business.ProgettoBusiness;
import business.Sessione;
import business.UtenteBusiness;
import model.Contenuto;
import model.Ordine;
import model.Progetto;
import model.Utente;
import sun.misc.Contended;
import view.LoginWindow;
import view.CapoProgettoView.CapoProgettoWindow;
import view.CapoProgettoView.DettagliDipendenteWindow;
import view.CapoProgettoView.DettagliOrdineWindow;
import view.CapoProgettoView.DettagliprogettoWindow;
import view.CapoProgettoView.RappSpeseWindow;

public class CapoProgettoListener implements ActionListener {

	private CapoProgettoWindow finestra;

	public CapoProgettoListener(CapoProgettoWindow finestra) {
		this.finestra = finestra;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		//		listener stampa rapporto spese
		if(e.getActionCommand().equals("1")){
			RappSpeseWindow rapp = new RappSpeseWindow(finestra);
		}
		//		listener applica
		if(e.getActionCommand().equals("2")){

			if(finestra.getProgetto().getSelectedIndex()!=0){
				finestra.getTabella().aggiornaTabellaProgetto(finestra, Sessione.getInstance().getUtenteLoggato().getIdUtente());
			}
			else{
				finestra.getTabella().aggiornaTabella(finestra, Sessione.getInstance().getUtenteLoggato().getIdUtente());
			}
			finestra.getTabella().clearSelection();
			finestra.getProgettoTabella().clearSelection();

		}
		//		listener dettagli ordine
		if(e.getActionCommand().equals("3")){
			if(!(finestra.getTabella().getSelectedRowCount()==0)){
				int idOrdine = (int)finestra.getTabella().getValueAt(finestra.getTabella().getSelectedRow(), 0);
				Ordine o = OrdineBusiness.getInstance().caricaOrdine(idOrdine);
				ArrayList<Contenuto> c = OrdineBusiness.getInstance().caricaContenuto(idOrdine);
				ArrayList<String> magazzini = OrdineBusiness.getInstance().getElencoMagazziniInteressati(c);

				DettagliOrdineWindow ord = new DettagliOrdineWindow(finestra, o, c, magazzini);
			}
			else{
				JOptionPane.showMessageDialog(finestra, "Nessun ordine selezionato", "Errore", JOptionPane.ERROR_MESSAGE);
			}

		}
		//		listener dettagli dipendente
		if(e.getActionCommand().equals("4")){
			if(!(finestra.getTabella().getSelectedRowCount()==0)){
				int idOrdine = (int)finestra.getTabella().getValueAt(finestra.getTabella().getSelectedRow(), 0);
				Ordine o = OrdineBusiness.getInstance().caricaOrdine(idOrdine);
				Utente u = UtenteBusiness.getInstance().caricaUtente(o.getIdDip());
				DettagliDipendenteWindow dip = new DettagliDipendenteWindow(u,finestra);
			}
			else{
				JOptionPane.showMessageDialog(finestra, "Nessun dipendente selezionato", "Errore", JOptionPane.ERROR_MESSAGE);
			}
		}
		//		listener logout
		if(e.getActionCommand().equals("5")){
			
			int result = JOptionPane.showConfirmDialog(finestra,"Sei sicuro di voler uscire?" ,null, JOptionPane.YES_NO_OPTION );

			if(result == JOptionPane.YES_OPTION){
				new LoginWindow();
				DbConnection.getInstance().disconnetti();
				finestra.setVisible(false);
				finestra.dispose();
			}
		}
		//		listener modifica dettagli progetto
		if(e.getActionCommand().equals("6")){
			if(!(finestra.getProgettoTabella().getSelectedRowCount()==0)){
				Progetto p = ProgettoBusiness.getInstance().caricaProgetto((String) finestra.getProgettoTabella().getValueAt(finestra.getProgettoTabella().getSelectedRow(), 0));
				new DettagliprogettoWindow(p, finestra);
			}
		}
	}

}
