package listeners;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import DbInterface.DbConnection;
import business.CategoriaBusiness;
import business.MagazzinoBusiness;
import business.OrdineBusiness;
import business.ProgettoBusiness;
import business.Sessione;
import business.StampaDistinta;
import dao.CatalogoDAO;
import model.Articolo;
import model.Contenuto;
import model.Magazzino;
import model.Ordine;
import model.Utente;
import view.LoginWindow;
import view.DipView.ConfermaOrdineWindow;
import view.DipView.DipendenteWindow;
import view.DipView.ModificaNoteWindow;

public class DipendenteListener implements ActionListener {

	private DipendenteWindow finestra;

	public DipendenteListener(DipendenteWindow finestra) {
		super();
		this.finestra = finestra;
	}


	private static JLabel mex2 = new JLabel("Nessun articolo trovato.");
	@Override
	public void actionPerformed(ActionEvent e) {

		DefaultTableModel model = (DefaultTableModel)finestra.getTabella().getModel();
		DefaultTableModel model2 = (DefaultTableModel)finestra.getTabella2().getModel();

		GridBagConstraints g = finestra.getG();
		g.fill = GridBagConstraints.BOTH;
		g.weightx = 1.0;
		g.weighty = 1.0;

		//listener carrello_______________________________________________________________________________________
		if(e.getActionCommand().equals("1")){

			finestra.remove(finestra.getJpC1());
			finestra.remove(finestra.getJpC3());
			finestra.remove(finestra.getJpS1());
			finestra.remove(finestra.getJpN1());

			finestra.getCatalogo().setEnabled(true);
			finestra.getStorico().setEnabled(true);
			finestra.getCarrello().setEnabled(false);

			if(finestra.getTabella2().getRowCount() == 0){

				finestra.getConfermaAcquisto().setEnabled(false);
				finestra.getDelSelected().setEnabled(false);
				finestra.getSvuota().setEnabled(false);
				finestra.getPiu().setEnabled(false);
				finestra.getMeno().setEnabled(false);
			}
			else{

				finestra.getConfermaAcquisto().setEnabled(true);
				finestra.getDelSelected().setEnabled(true);
				finestra.getSvuota().setEnabled(true);
				finestra.getPiu().setEnabled(true);
				finestra.getMeno().setEnabled(true);
			}

			g.gridx = 1;
			g.gridy=0;
			g.gridwidth =1;
			g.gridheight = 2;

			finestra.add(finestra.getJpC2(), g) ;

			g.gridx=1;
			g.gridy=2;
			g.gridwidth=1;
			finestra.add(finestra.getJpS2(),g);

			finestra.getTabella2().aggiornaSommario(finestra);

			finestra.repaint();
			finestra.revalidate();

		}
		//listener catalogo________________________________________________________________________________________
		if(e.getActionCommand().equals("2")){

			finestra.getCarrello().setEnabled(true);
			finestra.getStorico().setEnabled(true);
			finestra.getSpinner().setValue(1);
			finestra.remove(finestra.getJpC3());
			finestra.remove(finestra.getJpC2());

			g.gridx = 1;
			g.gridy=1;
			g.gridwidth =1;
			g.gridheight = 1;

			finestra.add(finestra.getJpC1(),g);


			finestra.remove(finestra.getJpS2());
			g.gridx=1;
			g.gridy=2;
			g.gridwidth=1;
			finestra.add(finestra.getJpS1(),g);

			g.gridx=1;
			g.gridy=0;
			g.gridwidth=1;
			finestra.add(finestra.getJpN1(),g);


			int righe=finestra.getTabella().getModel().getRowCount();
			for (int i = righe-1; i >= 0; i-- )
				model.removeRow(i);

			finestra.getTabella().aggiornaCatalogo(finestra);

			finestra.getCatalogo().setEnabled(false);
			
			finestra.getMagazzini().setSelectedIndex(0);
			finestra.getCategorie().setSelectedIndex(0);

			finestra.repaint();
			finestra.revalidate();
		}
		//listener storico_______________________________________________________________________________________
		else if(e.getActionCommand().equals("3")){

			finestra.getCarrello().setEnabled(true);
			finestra.getCatalogo().setEnabled(true);
			finestra.getStorico().setEnabled(false);

			//			rimuovo i pannelli centrali eventualmente presenti
			finestra.remove(finestra.getJpC1());
			finestra.remove(finestra.getJpC2());

			//			rimuovo pannello laterale
			finestra.remove(finestra.getJpS1());
			finestra.remove(finestra.getJpS2());
			finestra.remove(finestra.getJpN1());

			g.gridx = 1;
			g.gridy=0;
			g.gridwidth =1;
			g.gridheight =3;

			//			aggiungo il pannello centrale interessato
			finestra.add(finestra.getJpC3(),g);

			//			aggiorno la tabella degli ordini
			finestra.getStoricoTable().aggiornaStoricoordini_tabella(finestra,null);

			//			inserisco il testo dello stato
			String progettoSelezionato = (String)finestra.getProgetto().getSelectedItem();
			if(!progettoSelezionato.equals("Seleziona il progetto...")){
				int idProgetto=ProgettoBusiness.getInstance().caricaIdProgetto(progettoSelezionato);
				float budget = ProgettoBusiness.getInstance().caricaBudgetProgetto(idProgetto);
				float budgetSpesoDa = ProgettoBusiness.getInstance().caricaBudgetSpesoDa(idProgetto, Sessione.getInstance().getUtenteLoggato().getIdUtente());
				float budgetRimanente = ProgettoBusiness.getInstance().caricaBudgetRimanente(idProgetto);
				String text = "<html>STATO PROGETTO:<br>-Budget progetto (EUR): "+budget+"<br>"
						+ "-Budget speso da te (EUR) : "+budgetSpesoDa+"<br>-Budget disponibile (EUR) : "+budgetRimanente+"</html>";	

				finestra.getStatoProgetto().setText(text);
			}


			finestra.repaint();
			finestra.revalidate();
		}
		//		//listener applica filtri_______________________________________________________________________________________
		else if(e.getActionCommand().equals("4")){

			String magazzino = (String) finestra.getMagazzini().getSelectedItem();
			String categoria = (String) finestra.getCategorie().getSelectedItem();

			int idCategoria;

			if(finestra.getMagazzini().getSelectedIndex()==0 && finestra.getCategorie().getSelectedIndex()!=0){
				//				aggiorna solo categoria
				idCategoria=CategoriaBusiness.getInstance().getIdCategoria(categoria);
				finestra.getTabella().aggiornaTabellaCategorie(finestra, idCategoria);
			}
			else if(finestra.getMagazzini().getSelectedIndex()!=0 && finestra.getCategorie().getSelectedIndex()==0){
				//				aggiorna solo magazzino
				finestra.getTabella().aggiornaTabella(finestra, magazzino, 0);
			}
			else if(finestra.getMagazzini().getSelectedIndex()!=0 && finestra.getCategorie().getSelectedIndex()!=0){
				//				aggiorna entrambi

				idCategoria=CategoriaBusiness.getInstance().getIdCategoria(categoria);
				finestra.getTabella().aggiornaTabella(finestra, magazzino, idCategoria);
			}
			else if(finestra.getMagazzini().getSelectedIndex()==0 && finestra.getCategorie().getSelectedIndex()==0){
				//				ripristina la tabella
				finestra.getTabella().aggiornaCatalogo(finestra);
			}


		}
		//listener reset_______________________________________________________________________________________
		else if(e.getActionCommand().equals("5")){

			finestra.getMagazzini().setSelectedIndex(0);
			finestra.getCategorie().setSelectedIndex(0);

			finestra.getTabella().aggiornaCatalogo(finestra);
		}
		//listener logout_______________________________________________________________________________________
		else if(e.getActionCommand().equals("8")){

			if(finestra.getTabella2().getRowCount() == 0){
				int result = JOptionPane.showConfirmDialog(finestra,"Sei sicuro di voler uscire?" ,null, JOptionPane.YES_NO_OPTION );

				if(result == JOptionPane.YES_OPTION){
					new LoginWindow();
					DbConnection.getInstance().disconnetti();
					finestra.setVisible(false);
					finestra.dispose();
				}
			}
			else{
				int result = JOptionPane.showConfirmDialog(finestra,"Il carrello contiene articoli non acquistati.\nSe esci il carrello verra svuotato.\nContinuare?" ,null, JOptionPane.YES_NO_OPTION );

				if(result == JOptionPane.YES_OPTION){
					new LoginWindow();
					DbConnection.getInstance().disconnetti();
					finestra.setVisible(false);
					finestra.dispose();
				}
			}
		}
		//listener ricerca_______________________________________________________________________________________
		else if(e.getActionCommand().equals("9")){

			String testoRicercato = finestra.getRicerca().getText();
			
			if(!testoRicercato.equals("Cerca...")){
				int righe=finestra.getTabella().getModel().getRowCount();
				for (int i = righe-1; i >= 0; i-- )
					model.removeRow(i);
				

				if(!finestra.getCheck().isSelected()){
					ArrayList<Articolo> dati = finestra.getTabella().getDatiCatalogo();

					int k=0;

					for (Iterator iterator = dati.iterator(); iterator.hasNext();) {
						Articolo a = (Articolo) iterator.next();

						if(a.getNome().toLowerCase().indexOf(testoRicercato.toLowerCase()) != -1 ||
								a.getDescrizione().toLowerCase().indexOf(testoRicercato.toLowerCase()) != -1){

							Object[] rigaDati = new Object[7];
							rigaDati[0]=a.getIdArticolo();
							rigaDati[1]=a.getNome();
							rigaDati[2]=a.getDescrizione();
							rigaDati[3]=a.getPrezzo();
							rigaDati[4]=a.getQuantita_max_ordi();
							rigaDati[5]=a.getQuantita();
							rigaDati[6]=MagazzinoBusiness.getInstance().caricaDatiMagazzino(Sessione.getInstance().getUtenteLoggato().getMagazzino()).getNome();

							k++;
							model.addRow(rigaDati);
						}
					}

					if(k==0){
						JOptionPane.showMessageDialog(finestra, mex2);
						finestra.getTabella().aggiornaCatalogo(finestra);
					}
					finestra.revalidate();
					finestra.repaint();
				}
				else{

					ArrayList<Magazzino> magazzini= MagazzinoBusiness.getInstance().caricaElencoMagazzini();
					int k=0;
					for (Iterator iterator = magazzini.iterator(); iterator.hasNext();) {
						Magazzino m = (Magazzino) iterator.next();
						ArrayList<Articolo> dati = CatalogoDAO.getInstance().caricaDatiCatalogo(m.getIdmag());

						for (Iterator iterator2 = dati.iterator(); iterator2.hasNext();) {
							Articolo a = (Articolo) iterator2.next();
							if(a.getNome().toLowerCase().indexOf(testoRicercato.toLowerCase()) != -1 ||
									a.getDescrizione().toLowerCase().indexOf(testoRicercato.toLowerCase()) != -1){

								Object[] rigaDati = new Object[7];
								rigaDati[0]=a.getIdArticolo();
								rigaDati[1]=a.getNome();
								rigaDati[2]=a.getDescrizione();
								rigaDati[3]=a.getPrezzo();
								rigaDati[4]=a.getQuantita_max_ordi();
								rigaDati[5]=a.getQuantita();
								rigaDati[6]=m.getNome();

								k++;
								model.addRow(rigaDati);
							}
						}
						for(int h=0;h<dati.size();h++){
							
						}
					}
					for(int i=0; i<magazzini.size();i++){


					}
					if(k==0){
						JOptionPane.showMessageDialog(finestra, mex2);
						finestra.getTabella().aggiornaCatalogo(finestra);
					}
					finestra.revalidate();
					finestra.repaint();

				}

			}

			
		}
		//listener aggiungi al carrello_______________________________________________________________________________________
		else if(e.getActionCommand().equals("10")){

			int[] righe = finestra.getTabella().getSelectedRows();

			if(righe.length != 0){
				if((int)finestra.getSpinner().getValue()<=0)
					finestra.getSpinner().setValue(1);

				for (int i =0; i < righe.length; i++){

					Object[] rigaDati = new Object[7];

					rigaDati[0] = finestra.getTabella().getValueAt(righe[i], 0);
					rigaDati[1] = finestra.getTabella().getValueAt(righe[i], 1);
					rigaDati[2] = finestra.getTabella().getValueAt(righe[i], 2);
					rigaDati[3] = finestra.getTabella().getValueAt(righe[i], 3);
					rigaDati[4] = finestra.getTabella().getValueAt(righe[i], 4);

					if((int)finestra.getSpinner().getValue() < (int)finestra.getTabella().getValueAt(righe[i], 5)){

						if((int)finestra.getSpinner().getValue() < (int)finestra.getTabella().getValueAt(righe[i], 4))
							rigaDati[5] = finestra.getSpinner().getValue();
						else{
							rigaDati[5] = finestra.getTabella().getValueAt(righe[i], 4);
						}
					}

					rigaDati[6] = finestra.getTabella().getValueAt(righe[i], 6);

					if((int)finestra.getTabella().getValueAt(finestra.getTabella().getSelectedRow(), 5) > 2){
						int k=0;

						int rows=finestra.getTabella2().getRowCount();

						//controllo se l'articolo is gia presente nel carrello

						boolean presente=false;
						int j=0;
						for(j = 0; j<rows;j++){
							if(finestra.getTabella2().getValueAt(j, 0).equals(rigaDati[0]) && finestra.getTabella2().getValueAt(j, 6).equals(rigaDati[6])){
								presente=true;
								break;
							}
						}
						//se a� presnete controlla se la quantita non supera quella limite
						if(presente){

							int quantita= (int)finestra.getSpinner().getValue() + (int)finestra.getTabella2().getValueAt(j, 5);

							if(quantita<=(int)rigaDati[4]){							
								finestra.getTabella2().setValueAt(quantita, j, 5);
								k++;
							}
							else{
								if(righe.length>1){
									JOptionPane.showMessageDialog(finestra, "Quantita limite superata.","Avviso", JOptionPane.INFORMATION_MESSAGE);
									break;
								}
								else
									JOptionPane.showMessageDialog(finestra, "Quantita limite superata.","Avviso", JOptionPane.INFORMATION_MESSAGE);
							}


						}
						else{
							model2.addRow(rigaDati);
						}


					}
					else
						JOptionPane.showMessageDialog(finestra, "Articolo selezionato non disponibile", "Attenzione", JOptionPane.WARNING_MESSAGE);

				}



				finestra.revalidate();
				finestra.repaint();

			}
			else{
				JOptionPane.showMessageDialog(finestra, "Nessun articolo selezionato.", "Attenzione", JOptionPane.WARNING_MESSAGE);
			}


		}
		//listener svuota carrello_______________________________________________________________________________________
		else if(e.getActionCommand().equals("23")){

			finestra.getTabella2().svuotaCarrello(finestra);
			finestra.getMeno().setEnabled(false);
			finestra.getPiu().setEnabled(false);
		}
		//listener elimina selezionato_______________________________________________________________________________________
		else if(e.getActionCommand().equals("22")){

			int[] righe = finestra.getTabella2().getSelectedRows();

			if(finestra.getTabella2().getRowCount() == 0){

				finestra.getConfermaAcquisto().setEnabled(false);
				finestra.getDelSelected().setEnabled(false);
				finestra.getSvuota().setEnabled(false);
			}

			if(righe.length>0){
				for(int i=righe.length-1;i>=0;i--){
					model2.removeRow(righe[i]);
				}
			}
			else
				JOptionPane.showMessageDialog(finestra, "Nessun elemento selezionato.", "Avviso", JOptionPane.INFORMATION_MESSAGE);

			finestra.getTabella2().aggiornaSommario(finestra);

			if(finestra.getTabella2().getRowCount()==0)
				finestra.getConfermaAcquisto().setEnabled(false);

			finestra.revalidate();
			finestra.repaint();
		}
		//listner conferma ordine_______________________________________________________________________________________
		else if(e.getActionCommand().equals("24")){

			ConfermaOrdineWindow fin = new ConfermaOrdineWindow(finestra);

		}
		//		listener aumenta quantita________________________________________
		else if(e.getActionCommand().equals("25")){

			if(finestra.getTabella2().getSelectedRowCount()!=0){
				int row = finestra.getTabella2().getSelectedRow();

				int quantita= 1 + (int)finestra.getTabella2().getValueAt(row, 5);

				if(quantita<=(int)finestra.getTabella2().getValueAt(row, 4)){
					finestra.getTabella2().setValueAt(quantita, row, 5);
					finestra.getTabella2().setRowSelectionInterval(row, row);
				}
				else
					JOptionPane.showMessageDialog(finestra, "Quantita limite superata","Avviso", JOptionPane.INFORMATION_MESSAGE);

				finestra.getTabella2().aggiornaSommario(finestra);

				finestra.revalidate();
				finestra.repaint();
			}
		}
		//		listener diminuisci quantita________________________________________
		else if(e.getActionCommand().equals("26")){

			if(finestra.getTabella2().getSelectedRowCount()!=0){
				int row = finestra.getTabella2().getSelectedRow();

				int quantita= (int)finestra.getTabella2().getValueAt(row, 5) - 1;

				if(quantita<=0){
					int x = JOptionPane.showConfirmDialog(finestra, "Quantita settata a zero: l'articolo verra rimosso dal carrello.\n\nContinuare?", "Avviso", JOptionPane.YES_NO_OPTION);
					if(x==JOptionPane.YES_OPTION)
						model2.removeRow(row);
				}

				else{
					finestra.getTabella2().setValueAt(quantita, row, 5);
					finestra.getTabella2().setRowSelectionInterval(row, row);
				}

				finestra.getTabella2().aggiornaSommario(finestra);

				finestra.revalidate();
				finestra.repaint();
			}
		}
		//listener aggiorna_____________________________________________________________________
		else if(e.getActionCommand().equals("27")){

			String progettoSelezionato = (String) finestra.getProgetto().getSelectedItem();

			if(!progettoSelezionato.equals("Seleziona il progetto...")){

				finestra.getStoricoTable().aggiornaStoricoordini_tabella(finestra, progettoSelezionato);
				int idProgetto=ProgettoBusiness.getInstance().caricaIdProgetto(progettoSelezionato);
				float budget = ProgettoBusiness.getInstance().caricaBudgetProgetto(idProgetto);
				float budgetSpesoDa = ProgettoBusiness.getInstance().caricaBudgetSpesoDa(idProgetto, Sessione.getInstance().getUtenteLoggato().getIdUtente());
				float budgetRimanente = ProgettoBusiness.getInstance().caricaBudgetRimanente(idProgetto);
				String text = "<html>STATO PROGETTO ("+progettoSelezionato+"):<br>-Budget progetto (EUR) : "+budget+"<br>"
						+ "-Budget speso da te (EUR) : "+budgetSpesoDa+"<br>-Budget disponibile (EUR) : "+budgetRimanente+"</html>";	

				finestra.getStatoProgetto().setText(text);
			}
			else{
				finestra.getStoricoTable().aggiornaStoricoordini_tabella(finestra, null);
				finestra.getStatoProgetto().setText("");
			}

			finestra.repaint();
			finestra.revalidate();			

		}
		//listener stampa_____________________________________________________________________
		else if(e.getActionCommand().equals("28")){

			if(finestra.getStoricoTable().getSelectedRowCount() != 0){
				JFileChooser fc = new JFileChooser();
				int ans = fc.showSaveDialog(finestra);

				if(ans==JFileChooser.APPROVE_OPTION){

					String path = fc.getSelectedFile().getPath();
					String nomeFilePDF = path+".pdf";
					int idOrdine = (int) finestra.getStoricoTable().getValueAt(finestra.getStoricoTable().getSelectedRow(), 0);
					Ordine ord = OrdineBusiness.getInstance().caricaOrdine(idOrdine);
					Utente utente = Sessione.getInstance().getUtenteLoggato();
					ArrayList<Contenuto> c = OrdineBusiness.getInstance().caricaContenuto(idOrdine);
					ArrayList<String> mag = OrdineBusiness.getInstance().getElencoMagazziniInteressati(c);
					try {
						StampaDistinta.getInstance().stampaDistinta(utente, ord, c, mag, nomeFilePDF,finestra);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}


		//listener visualizza note_____________________________________________________________________
		else if(e.getActionCommand().equals("29")){
			if(finestra.getStoricoTable().getSelectedRowCount() != 0)
				JOptionPane.showMessageDialog(finestra, finestra.getStoricoTable().getValueAt(finestra.getStoricoTable().getSelectedRow(), 4), "NOTE", JOptionPane.INFORMATION_MESSAGE);

		}
		//listener modifica note_____________________________________________________________________
		else if(e.getActionCommand().equals("30")){
			ModificaNoteWindow win;
			if(finestra.getStoricoTable().getSelectedRowCount() != 0)
				win = new ModificaNoteWindow(finestra);

		}
	}

}
