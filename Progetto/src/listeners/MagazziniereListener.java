package listeners;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import business.CatalogoBusiness;
import business.FornitoreBusiness;
import business.OrdineBusiness;
import business.Sessione;
import model.Articolo;
import model.Contenuto;
import model.Fornitore;
import model.Ordine;
import view.LoginWindow;
import view.MagazziniereView.*;

public class MagazziniereListener implements ActionListener {

	private MagazziniereWindow finestra;
	private static JLabel messaggio = new JLabel("Nessun articolo trovato.");

	public MagazziniereListener(MagazziniereWindow magazziniereWindow) {
		super();
		this.finestra = magazziniereWindow;
	}

	@Override

	public void actionPerformed(ActionEvent e) {

		DefaultTableModel model = (DefaultTableModel)finestra.getTabella().getModel();

		GridBagConstraints g = finestra.getG();
		g.fill = GridBagConstraints.BOTH;

		//LISTENER ORDINI
		if(e.getActionCommand().equals("1")){

			finestra.remove(finestra.getJpC3());
			finestra.remove(finestra.getJpS3());
			finestra.remove(finestra.getJpC2());
			finestra.remove(finestra.getJpS2());
			finestra.getOrdini().setEnabled(false);
			finestra.getRifornimento().setEnabled(true);
			finestra.getStorico().setEnabled(true);

			g.gridx = 0;
			g.gridy=1;
			g.gridwidth =1;

			finestra.add(finestra.getJpC1(), g);

			g.gridx = 0;
			g.gridy=2;
			g.gridwidth =6;
			finestra.add(finestra.getJpS1(), g);

			finestra.repaint();
			finestra.revalidate();

		}

		//LISTENER RIFORNIMENTO
		else if(e.getActionCommand().equals("2")){

			finestra.remove(finestra.getJpC1());
			finestra.remove(finestra.getJpS1());
			finestra.remove(finestra.getJpC3());
			finestra.remove(finestra.getJpS3());
			finestra.getOrdini().setEnabled(true);
			finestra.getRifornimento().setEnabled(false);
			finestra.getStorico().setEnabled(true);

			g.gridx = 0;
			g.gridy=1;
			g.gridwidth =1;

			finestra.add(finestra.getJpC2(), g);
			g.gridx = 0;
			g.gridy=2;
			g.gridwidth =6;

			finestra.add(finestra.getJpS2(), g);

			finestra.repaint();
			finestra.revalidate();
		}	

		//LISTENER STORICO ORDINE
		else if(e.getActionCommand().equals("3")){

			finestra.remove(finestra.getJpC1());
			finestra.remove(finestra.getJpS1());
			finestra.remove(finestra.getJpC2());
			finestra.remove(finestra.getJpS2());
			finestra.getOrdini().setEnabled(true);
			finestra.getRifornimento().setEnabled(true);
			finestra.getStorico().setEnabled(false);

			g.gridx = 0;
			g.gridy=1;
			g.gridwidth =1;

			finestra.add(finestra.getJpC3(), g);
			g.gridx = 0;
			g.gridy=2;
			g.gridwidth =6;

			finestra.add(finestra.getJpS3(), g);

			finestra.repaint();
			finestra.revalidate();

		}

		//LISTENER ANNULLA CERCA
		else if(e.getActionCommand().equals("15")){
			finestra.remove(finestra.getJpC3());						

			int righe=finestra.getTabella().getModel().getRowCount();
			for (int i = righe-1; i >= 0; i-- )
				model.removeRow(i);

			ArrayList<Ordine> elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

			Object[] rigaDati = new Object[4];

			int cont = 0;
			for( int i = 0 ; i<elencoOrdini.size(); i++){

				rigaDati[0]=elencoOrdini.get(i).getIdOrdine();
				rigaDati[1]=elencoOrdini.get(i).getData();
				rigaDati[2]=elencoOrdini.get(i).getDipendete();
				rigaDati[3]=elencoOrdini.get(i).getNote();

				ArrayList<Contenuto> con = OrdineBusiness.getInstance().caricaContenuto((int)rigaDati[0]);

				boolean x=false;

				for (Iterator iterator = con.iterator(); iterator.hasNext();) {
					Contenuto c = (Contenuto) iterator.next();
					if(!c.isEvaso() && c.getIdMag() == Sessione.getInstance().getUtenteLoggato().getMagazzino())
						x=true;
				}

				if(x)
					model.addRow(rigaDati);
			}

			JOptionPane.showMessageDialog(null, "Aggiornato!",null, JOptionPane.INFORMATION_MESSAGE);

			finestra.repaint();
			finestra.revalidate();
		}	

		//LISTENER CERCA
		else if(e.getActionCommand().equals("5")){
			finestra.remove(finestra.getJpC3());

			String data = ((JTextField)finestra.getData().getDateEditor().getUiComponent()).getText();

			if(data.equals("")){
				JOptionPane.showMessageDialog(finestra, "Data non inserita!", "Attenzione", JOptionPane.ERROR_MESSAGE);

			} else {

				int righe=finestra.getTabella().getModel().getRowCount();
				for (int i = righe-1; i >= 0; i-- )
					model.removeRow(i);


				ArrayList<Ordine> ordini= OrdineBusiness.getInstance().cercaData(data);

				int k=0;
				for(int h=0; h<ordini.size();h++){

					Object[] rigaDati = new Object[4];
					rigaDati[0]=ordini.get(h).getIdOrdine();
					rigaDati[1]=ordini.get(h).getData();
					rigaDati[2]=ordini.get(h).getDipendete();
					rigaDati[3]=ordini.get(h).getNote();

					k++;
					model.addRow(rigaDati);

				}

				if(k==0){
					JOptionPane.showMessageDialog(finestra, messaggio);

					ArrayList<Ordine> elencoOrdini = OrdineBusiness.getInstance().caricaOrdini();

					Object[] rigaDati = new Object[4];

					for( int i = 0 ; i<elencoOrdini.size(); i++){

						rigaDati[0]=elencoOrdini.get(i).getIdOrdine();
						rigaDati[1]=elencoOrdini.get(i).getData();
						rigaDati[2]=elencoOrdini.get(i).getDipendete();
						rigaDati[3]=elencoOrdini.get(i).getNote();

						ArrayList<Contenuto> con = OrdineBusiness.getInstance().caricaContenuto((int)rigaDati[0]);

						boolean x=false;

						for (Iterator iterator = con.iterator(); iterator.hasNext();) {
							Contenuto c = (Contenuto) iterator.next();
							if(!c.isEvaso() && c.getIdMag() == Sessione.getInstance().getUtenteLoggato().getMagazzino())
								x=true;
						}

						if(x)
							model.addRow(rigaDati);

					}
				}

				finestra.revalidate();
				finestra.repaint();
			}
		}

		//LISTENER EVADI ORDINE
		else if(e.getActionCommand().equals("8")){	
			finestra.remove(finestra.getJpC3());
			finestra.remove(finestra.getJpS3());
			
			if(finestra.getTabella().getSelectedRowCount()==0){			
				JOptionPane.showMessageDialog(finestra, "Nessuna riga seleionata!", "Attenzione", JOptionPane.ERROR_MESSAGE);
			} else {

				int idOrdine = (int) finestra.getTabella().getValueAt(finestra.getTabella().getSelectedRow(), 0);
				int Magazzino = Sessione.getInstance().getUtenteLoggato().getMagazzino();
				OrdineBusiness.getInstance().evadiOrdine(idOrdine, Magazzino);

				int[] righe = finestra.getTabella().getSelectedRows();

				if(righe.length>0){
					for(int i=righe.length-1;i>0;i--){
						model.removeRow(righe[i]);
					}
				}

				finestra.getTabella().AggiornaTabella(finestra);


			}
			
			
			finestra.revalidate();
			finestra.repaint();
		}


		//LISTENER NUOVO
		else if(e.getActionCommand().equals("12")){
			NuovoWindow nuovo = new NuovoWindow(finestra);
		}

		//LISTENER RECLAMO
		
		else if(e.getActionCommand().equals("13")){
			if(finestra.getTabella3().getSelectedRowCount() == 0){
				JOptionPane.showMessageDialog(finestra, "Selezionare una riga!", "Attenzione", JOptionPane.ERROR_MESSAGE);
			} else {	
				int idArticolo = (int) finestra.getTabella3().getValueAt(finestra.getTabella3().getSelectedRow(), 0);
				Fornitore fornitore = FornitoreBusiness.getInstance().caricaFornitore(idArticolo);
				SelezionaReclamoWindow reclamo = new SelezionaReclamoWindow(finestra);
			}
			
		}
		

		//LISTENER ANNULLA
		else if(e.getActionCommand().equals("11")){

			if(finestra.getTextField_1().getText().equals("") && finestra.getTextField_2().getText().equals("") && finestra.getTextField_3().getText().equals("") && finestra.getTextField_4().getText().equals("") && finestra.getTextArea().getText().equals("")){
				JOptionPane.showMessageDialog(null, "I campi per aggiornare i Prodotti sono gia vuoti!",null, JOptionPane.INFORMATION_MESSAGE);
			}else{

				finestra.getTextField_1().setText("");
				finestra.getTextField_2().setText("");
				finestra.getTextField_3().setText("");
				finestra.getTextField_4().setText("");
				finestra.getTextArea().setText("");
			}

		}

		//LISTENER APPLICA
		else if(e.getActionCommand().equals("10")){

			if(finestra.getTabella3().getSelectedRowCount() == 0){
				JOptionPane.showMessageDialog(finestra, "Nessuno valore inserito !", "Attenzione", JOptionPane.ERROR_MESSAGE);
			}else{

				try {

					String nome = finestra.getTextField_1().getText();
					float prezzo = Float.parseFloat(finestra.getTextField_2().getText());
					int quantita = Integer.parseInt(finestra.getTextField_3().getText());
					int qta_max = Integer.parseInt(finestra.getTextField_4().getText());
					String descrizione = finestra.getTextArea().getText();

					int idArt=0;
					int idProd=0;
					int cat =0;

					ArrayList<Articolo> elenco = CatalogoBusiness.getInstance().caricaArticoli(Sessione.getInstance().getUtenteLoggato().getMagazzino());

					for (Iterator x = elenco.iterator(); x.hasNext();) {
						Articolo articolo = (Articolo) x.next();

						if(articolo.getNome().equals(finestra.getTabella3().getValueAt(finestra.getTabella3().getSelectedRow(), 1))){
							idArt = articolo.getIdArticolo();
							idProd = articolo.getIdProdotto();
							cat = articolo.getCategoria();

						}
					}

					Articolo a = new Articolo(nome, descrizione, prezzo, quantita, qta_max, cat, idArt, idProd, 0); 

					CatalogoBusiness.getInstance().aggiornaRifornimento(a);

					finestra.getTabella3().aggiornaRifornimento_table(finestra);

					finestra.revalidate();
					finestra.repaint();

				} catch (NumberFormatException e2) {

					JOptionPane.showMessageDialog(finestra, "Valori inserito non permesso.\nPrego ricontrollare i parametri inseriti.", "Attenzione", JOptionPane.ERROR_MESSAGE);
					int i = finestra.getTabella3().getSelectedRow();

					finestra.getTextField_1().setText(finestra.getTabella3().getValueAt(i, 1).toString());
					finestra.getTextArea().setText(finestra.getTabella3().getValueAt(i, 2).toString());
					finestra.getTextField_2().setText(finestra.getTabella3().getValueAt(i, 3).toString());
					finestra.getTextField_3().setText(finestra.getTabella3().getValueAt(i, 4).toString());
					finestra.getTextField_4().setText(finestra.getTabella3().getValueAt(i, 5).toString());			

				}
			}
		}

		//LISTENER NOTE
		else if(e.getActionCommand().equals("9")){

			finestra.remove(finestra.getJpC3());
			finestra.remove(finestra.getJpS3());
			finestra.remove(finestra.getJpC2());
			finestra.remove(finestra.getJpS2());

			if(finestra.getTabella().getSelectedRowCount()==1){			
				JOptionPane.showMessageDialog(null, (String)finestra.getTabella().getValueAt(finestra.getTabella().getSelectedRow(), 3));
			} else {
				JOptionPane.showMessageDialog(finestra, "Nessuna riga selezzionata!", "Attenzione", JOptionPane.ERROR_MESSAGE);
			}
			finestra.revalidate();
			finestra.repaint();
		}

		//LISTENER AGGIORNA
		else if(e.getActionCommand().equals("14")){

			finestra.getTabella4().AggiornaTabellaMag(finestra);

			finestra.revalidate();
			finestra.repaint();
		}

		//LISTENER LOGOUT
		else if(e.getActionCommand().equals("4") || e.getActionCommand().equals("6") || e.getActionCommand().equals("7")){
			finestra.remove(finestra.getJpC3());

			int result = JOptionPane.showConfirmDialog(null,"Sei sicuro di voler uscire?" ,null, JOptionPane.YES_NO_OPTION );

			if(result == JOptionPane.YES_OPTION){
				new LoginWindow();
				finestra.setVisible(false);
				finestra.dispose();
			}
		}

	}
}