package listeners.constants;

public class MagazziniereConstants {

	//costanti finestra magazziniere
		public final static String ordini = "1";
		public final static String rifornimento = "2";
		public final static String storico = "3";
		public final static String logout = "4";
		public final static String cerca = "5";
		
		public final static String logout2 = "6";
		public final static String logout3 = "7";
		
		public final static String evasione = "8";
		public final static String note = "9";
		
		public final static String applica = "10";
		public final static String annulla = "11";
		public final static String nuovo = "12";
		public final static String reclamo = "13";
		
		public final static String aggiorna = "14";
		public final static String annulla_cer = "15";

}