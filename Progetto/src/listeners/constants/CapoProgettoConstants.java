package listeners.constants;

public class CapoProgettoConstants {

	public final static String rappSpese = "1";
	public final static String applica = "2";
	public final static String dettOrdine = "3";
	public final static String dettUtente = "4";
	public final static String logout = "5";
	public final static String dettProgetto = "6";

}
