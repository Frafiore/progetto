package listeners.constants;

public interface DipendenteConstants {

	//costanti finetra dipendente
	public final static String carrello = "1";
	public final static String catalogo = "2";
	public final static String storico = "3";
	
	//magazzini
	public final static String applica = "4";
	public final static String reset = "5";
	
	//bottoni di sotto
	public final static String logout="8";
	public final static String cerca = "9";
	public final static String addToCart = "10";
	public final static String logout2="20";
	public final static String logout3="21";
	public final static String delSel="22";
	public final static String svuota="23";
	public final static String confermaOrdine="24";
	
	//categorie
	public final static String chiavette= "11";
	public final static String cd ="12";
	public final static String cavistica = "13";
	public final static String risme ="14";
	public final static String matite = "15";
	public final static String penne="16";
	public final static String postit ="17";
	public final static String toner = "18";
	public final static String cartucce = "19";
	
	//piu e meno
	public final static String piu = "25";
	public final static String meno = "26";
	
	//storico buttons
	public final static String aggiorna="27";
	public final static String stampaDist="28";
	public final static String visNote="29";
	public final static String modNote="30";
}
