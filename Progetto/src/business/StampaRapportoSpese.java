package business;

import java.awt.Font;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import model.Ordine;
import model.Progetto;
import model.Utente;

public class StampaRapportoSpese {
	private static StampaRapportoSpese instance;

	//SINGLETON
	public static synchronized StampaRapportoSpese getInstance(){	
		if(instance == null)
			instance = new StampaRapportoSpese();
		return instance;
	}
	public void stampaRapporto(ArrayList<Ordine> elencoOrdini, boolean x, String nomeDistinta, JFrame finestra) throws IOException{

		String outputFileName = nomeDistinta;
		// Create a document and add a page to it

		// Create a new font object selecting one of the PDF base fonts
		PDFont grassetto = PDType1Font.HELVETICA_BOLD;
		PDFont normale = PDType1Font.HELVETICA;
		PDFont corsivo = PDType1Font.HELVETICA_OBLIQUE;

		PDDocument document = new PDDocument();
		PDPage page;
		PDRectangle rect;
		PDPageContentStream cos;
		int line = 0;

		page = new PDPage(PDRectangle.A4);
		// PDRectangle.LETTER and others are also possible
		rect = page.getMediaBox();
		// rect can be used to get the page width and height
		document.addPage(page);


		// Start a new content stream which will "hold" the to be created content
		cos = new PDPageContentStream(document, page);


		// Define a text content stream using the selected font, move the cursor and draw some text
		cos.beginText();
		cos.setFont(grassetto, 22);
		cos.newLineAtOffset(rect.getWidth()/2 - 130, rect.getHeight() - 30*(++line));
		cos.showText("RAPPORTO SPESE");
		cos.endText();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
		
		cos.beginText();
		cos.setFont(corsivo, 15);
		cos.newLineAtOffset(30, rect.getHeight() - 30*(++line));
		cos.showText("Aggiornato al "+sdf.format(Calendar.getInstance().getTime())+", ore " +sdf2.format(Calendar.getInstance().getTime()));
		cos.endText();

		//		se x == true allora raggruppo per dipendente
		if(x){
			cos.beginText();
			cos.setFont(grassetto, 15);
			cos.newLineAtOffset(30, rect.getHeight() - 30*(++line));
			cos.showText("Elenco dipendenti associati");
			cos.endText();
			
			cos.beginText();
			cos.setFont(grassetto, 15);
			cos.newLineAtOffset(30, rect.getHeight() - 30*(++line));
			cos.showText("_____________________________________________________________");
			cos.endText();

			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(40, rect.getHeight() - 30*(++line));
			cos.showText("Nome");
			cos.endText();

			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(190, rect.getHeight() - 30*(line));
			cos.showText("Progetto associato");
			cos.endText();

			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(390, rect.getHeight() - 30*(line));
			cos.showText("Budget speso (EUR)");
			cos.endText();
		
			cos.beginText();
			cos.setFont(grassetto, 15);
			cos.newLineAtOffset(30, rect.getHeight() - 26*(++line));
			cos.showText("_____________________________________________________________");
			cos.endText();
			
			ArrayList<Progetto> elencoProgetti = ProgettoBusiness.getInstance().elencoProgetti(Sessione.getInstance().getUtenteLoggato().getIdUtente());

			ArrayList<Utente> elencoDipendenti = UtenteBusiness.getInstance().caricaElencoDipendenti(Sessione.getInstance().getUtenteLoggato().getIdUtente());
			
			final DecimalFormat df = new DecimalFormat("0.00");
			df.setRoundingMode(RoundingMode.CEILING);
			
			
			for (Iterator iterator = elencoProgetti.iterator(); iterator.hasNext();) {
				Progetto p = (Progetto) iterator.next();
				
				for (Iterator i = elencoDipendenti.iterator(); i.hasNext();) {
					Utente u = (Utente) i.next();

					float totale = 0;
					
					if(u.lavoraAlProgetto(p.getIdProgetto())){
						
						cos.beginText();
						cos.setFont(normale, 11);
						cos.newLineAtOffset(50, rect.getHeight() - 30*(++line));
						cos.showText(u.getCognome() +" "+u.getNome());
						cos.endText();

						cos.beginText();
						cos.setFont(normale, 11);
						cos.newLineAtOffset(210, rect.getHeight() - 30*(line));
						cos.showText(""+p.getNome());
						cos.endText();
						
						
						for (Iterator iterator2 = elencoOrdini.iterator(); iterator2.hasNext();) {
							Ordine o = (Ordine) iterator2.next();
							
							if(o.getIdDip() == u.getIdUtente() && o.getProgetto() == p.getIdProgetto())
								totale+=o.getCosto_totale();
						}
						
						cos.beginText();
						cos.setFont(normale, 11);
						cos.newLineAtOffset(420, rect.getHeight() - 30*(line));
						cos.showText(""+totale);
						cos.endText();
					}

				}

			}
			
		}

		//		altrimenti per progetto
		else{
			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(30, rect.getHeight() - 30*(++line));
			cos.showText("Progetto");
			cos.endText();

			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(150, rect.getHeight() - 30*(line));
			cos.showText("Stato");
			cos.endText();

			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(250, rect.getHeight() - 30*(line));
			cos.showText("Budget");
			cos.endText();

			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(350, rect.getHeight() - 30*(line));
			cos.showText("Budget speso");
			cos.endText();

			cos.beginText();
			cos.setFont(grassetto, 13);
			cos.newLineAtOffset(450, rect.getHeight() - 30*(line));
			cos.showText("Budget disponibile");
			cos.endText();

			final DecimalFormat df = new DecimalFormat("0.00");
			df.setRoundingMode(RoundingMode.CEILING);

			ArrayList<Progetto> pro = ProgettoBusiness.getInstance().elencoProgetti(Sessione.getInstance().getUtenteLoggato().getIdUtente());

			for (Iterator iterator2 = pro.iterator(); iterator2.hasNext();) {
				Progetto p = (Progetto) iterator2.next();

				cos.beginText();
				cos.setFont(normale, 11);
				cos.newLineAtOffset(30, rect.getHeight() - 30*(++line));
				cos.showText(p.getNome());
				cos.endText();

				cos.beginText();
				cos.setFont(normale, 11);
				cos.newLineAtOffset(150, rect.getHeight() - 30*(line));
				if(p.isAttivo())
					cos.showText("Attivo");
				else
					cos.showText("Chiuso");
				cos.endText();

				cos.beginText();
				cos.setFont(normale, 11);
				cos.newLineAtOffset(250, rect.getHeight() - 30*(line));
				cos.showText(""+df.format(p.getBudget()));
				cos.endText();

				cos.beginText();
				cos.setFont(normale, 11);
				cos.newLineAtOffset(350, rect.getHeight() - 30*(line));
				cos.showText(""+df.format((float)p.getBudget() - (float)ProgettoBusiness.getInstance().caricaBudgetRimanente(p.getIdProgetto())));
				cos.endText();

				cos.beginText();
				cos.setFont(normale, 11);
				cos.newLineAtOffset(470, rect.getHeight() - 30*(line));
				cos.showText(""+df.format(ProgettoBusiness.getInstance().caricaBudgetRimanente(p.getIdProgetto())));
				cos.endText();

			}

		}


		cos.close();

		// Save the results and ensure that the document is properly closed:
		document.save(outputFileName);
		document.close();

		JLabel label = new JLabel("<html>Rapporto spese salvato in:<br>"+outputFileName+".</html");
		label.setFont(new Font("", Font.PLAIN, 20));
		JOptionPane.showMessageDialog(finestra,label );

	}
}
