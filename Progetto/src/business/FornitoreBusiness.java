package business;

import java.util.ArrayList;

import model.Fornitore;
import model.Ordine;
import model.Utente;

public class FornitoreBusiness {

	private static FornitoreBusiness instance;

	//SINGLETON
	public static synchronized FornitoreBusiness getInstance(){	
		if(instance == null)
			instance = new FornitoreBusiness();
		return instance;
	}

	public ArrayList<Fornitore> getFornitore(){
		Fornitore cat = new Fornitore();
		return cat.getFornitore();
	}
	
	Fornitore FonrLoggato;
	
	public Fornitore getFontinoreProva() {
		return FonrLoggato;
	}
	
	public Fornitore caricaFornitore(int idArticolo) {
		Fornitore f = new Fornitore();
		return f.caricaFornitore(idArticolo);
	}
	
	public ArrayList<Fornitore> caricaFornitori(){
		Fornitore o = new Fornitore();
		return o.caricaFornitori();
	}

}