package business;

import java.util.ArrayList;
import java.util.Iterator;

import model.Progetto;

public class ProgettoBusiness {
	private static ProgettoBusiness instance;

	//SINGLETON
	public static synchronized ProgettoBusiness getInstance(){	
		if(instance == null)
			instance = new ProgettoBusiness();
		return instance;
	}
	
	public ArrayList<Progetto> elencoProgetti(int idUtente){
		Progetto p = new Progetto();
		return p.elencoProgetti(idUtente);
	}
	public int caricaIdProgetto(String progetto){
		Progetto p = new Progetto();
		return p.caricaIdProgetto(progetto);
	}
	public String caricaNomeProgetto(int idProgetto){
		Progetto p = new Progetto();
		return p.caricaNomeProgetto(idProgetto);
	}
	public float caricaBudgetProgetto(int progetto){
		Progetto p = new Progetto();
		return p.caricaBudgetProgetto(progetto);
	}

	public float caricaBudgetRimanente(int progetto){
		Progetto p = new Progetto();
		return p.caricaBudgetDisponibile(progetto);
	}
	public float caricaBudgetSpesoDa(int progetto, int idUtente){
		Progetto p = new Progetto();
		return p.caricaBudgetSpesoDa(progetto, idUtente);
	}
	public Progetto caricaProgetto(String nome){
		Progetto p = new Progetto();
		ArrayList<Progetto> el = p.elencoProgetti(Sessione.getInstance().getUtenteLoggato().getIdUtente());
		
		Progetto progetto = new Progetto();
		
		for (Iterator iterator = el.iterator(); iterator.hasNext();) {
			Progetto pr = (Progetto) iterator.next();
			if(pr.getNome().equals(nome)){
				progetto=pr;
				break;
			}
		}
		return progetto;
	}
	public boolean updateProgetto(int idProgetto, String nome,String stato, String budget){
		Progetto p = new Progetto();
		return p.updateProgetto(idProgetto, nome, stato, budget);
	}

}
