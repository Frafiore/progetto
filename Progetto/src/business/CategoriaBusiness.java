package business;

import java.util.ArrayList;

import dao.CategoriaDAO;
import model.Categoria;

public class CategoriaBusiness {

	private static CategoriaBusiness instance;

	//SINGLETON
	public static synchronized CategoriaBusiness getInstance(){	
		if(instance == null)
			instance = new CategoriaBusiness();
		return instance;
	}

	public ArrayList<Categoria> getCategorie(){
		Categoria cat = new Categoria();
		return cat.getCategorie();
	}

	public int getIdCategoria(String nomeCategoria){

		Categoria cat = new Categoria();
		return cat.getIdCategoria(nomeCategoria);
	}
}
