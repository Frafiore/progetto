package business;

import java.util.Random;

public class RandomString {
	
	private static RandomString instance;
	
	//SINGLETON
	public static synchronized RandomString getInstance(){
	
		if(instance == null)
			instance = new RandomString();
		return instance;
	}
	
	public static String randomString (int length) {
		Random rnd = new Random ();
		char[] arr = new char[length];

		for (int i=0; i<length; i++) {
		int n = rnd.nextInt (36);
		arr[i] = (char) (n < 10 ? '0'+n : 'a'+n-10);
		}

		return new String (arr);
		}

}