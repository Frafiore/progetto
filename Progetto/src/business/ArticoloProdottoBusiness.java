package business;

import model.Articolo;

public class ArticoloProdottoBusiness {
	
	private static ArticoloProdottoBusiness instance;
	
	//SINGLETON
	public static synchronized ArticoloProdottoBusiness getInstance(){	
		if(instance == null)
			instance = new ArticoloProdottoBusiness();
		return instance;
	
	}
	
    public  boolean caricaNuovo(int idMagazzino, Articolo a,int idForn){
		return a.caricaNuovo(idMagazzino, a,idForn);
	}
    
}
