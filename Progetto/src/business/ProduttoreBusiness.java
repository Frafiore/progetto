package business;

import java.util.ArrayList;

import model.Categoria;
import model.Produttore;

public class ProduttoreBusiness {

	private static ProduttoreBusiness instance;

	//SINGLETON
	public static synchronized ProduttoreBusiness getInstance(){	
		if(instance == null)
			instance = new ProduttoreBusiness();
		return instance;
	}

	public ArrayList<Produttore> getProduttore(){
		Produttore p = new Produttore();
		return p.getProduttore();
	}
	public Produttore prodottoDa(int idArt){
		Produttore p = new Produttore();
		return p.prodottoDa(idArt);
	}

}
