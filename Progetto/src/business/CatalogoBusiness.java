package business;

import java.util.ArrayList;

import dao.CatalogoDAO;
import model.Articolo;
import model.Magazzino;
import model.Ordine;

public class CatalogoBusiness {

	private static CatalogoBusiness instance;

	//SINGLETON
	public static synchronized CatalogoBusiness getInstance(){	
		if(instance == null)
			instance = new CatalogoBusiness();
		return instance;
	}

	public ArrayList<Articolo> caricaArticoli(int idMag){

		return CatalogoDAO.getInstance().caricaDatiCatalogo(idMag);

	}

	public ArrayList<Articolo> caricaRifornimento(int idMag){

		return CatalogoDAO.getInstance().caricaRifornimento(idMag);

	}
	
	public boolean aggiornaRifornimento(Articolo a){
		Articolo o = new Articolo();
		return o.aggiornaRifornimento(a);

	}


}
