package business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JOptionPane;

import dao.OrdineDAO;
import dao.ProgettoDAO;
import model.Contenuto;
import model.Ordine;
import view.DipView.ConfermaOrdineWindow;
import view.tables.Carrello_table;

public class OrdineBusiness {

	private static OrdineBusiness instance;

	//SINGLETON
	public static synchronized OrdineBusiness getInstance(){	
		if(instance == null)
			instance = new OrdineBusiness();
		return instance;

	}

	public ArrayList<Ordine> caricaOrdini(){
		Ordine o = new Ordine();
		return o.caricaOrdini();
	}

	public ArrayList<Contenuto> caricaContenuto(int idOrdine){
		Ordine o = new Ordine(idOrdine); 	
		return o.caricaContenuto();		
	}

	public boolean inserisciOrdine(ConfermaOrdineWindow finestra, Carrello_table carrello){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		int idOrdine = caricaUltimoid();

		float prezzo = 	carrello.getSommaPrezzi();
		float spedizione = 	carrello.getSpedizione();
		float totale = 	carrello.getTotale();

		int dip = Sessione.getInstance().getUtenteLoggato().getIdUtente();

		String progetto = (String) finestra.getProgetto().getSelectedItem();
		int idProgetto = ProgettoDAO.getInstance().caricaIdProgetto(progetto);


		String note= finestra.getNote().getText();
		if(note.equals("Note..."))
			note="";

		Ordine ord = new Ordine(idOrdine,prezzo, spedizione, idProgetto, totale, note, dip);


		ArrayList<Contenuto> contenuto = new ArrayList<>();
		boolean flag = true;
		for(int i =0;i<carrello.getRowCount();i++){

			int idArticolo = (int)carrello.getValueAt(i, 0);
			int quantita = (int)carrello.getValueAt(i, 5);
			int evaso = 0;
			String nome = (String) carrello.getValueAt(i, 1);
			float prezzoPagato = (float)carrello.getValueAt(i, 3);

			Contenuto cont = new Contenuto(idArticolo,nome, quantita, evaso, idOrdine,prezzoPagato);
			
			
			int quantitaGiaOrdinata = controlloSuQuantita(idArticolo, sdf.format(Calendar.getInstance().getTime()));
			
			if(quantitaGiaOrdinata+quantita <= (int)carrello.getValueAt(i, 4)){
				contenuto.add(cont);
			}
			else{
				flag=false;
				break;
			}

		}
		if(flag==false)
			JOptionPane.showMessageDialog(finestra, "Oggi � gi� stata acquistata la quantita massima ordinabile di alcuni articoli inseriti nel carrello.","Attenzione",JOptionPane.WARNING_MESSAGE);
		
		if(contenuto.size()>0)
			return ord.inserisciOrdine(contenuto);
		else
			return false;
	}
	public Ordine caricaOrdine(int idOrdine){
		Ordine o = new Ordine();

		ArrayList<Ordine> elenco = o.caricaOrdini();
		for (Iterator iterator = elenco.iterator(); iterator.hasNext();) {
			Ordine ordine = (Ordine) iterator.next();
			if(ordine.getIdOrdine() == idOrdine){
				o = ordine;
				break;
			}
		}
		return o;
	}
	public int caricaUltimoid(){
		Ordine o = new Ordine();
		ArrayList<Ordine> x = o.caricaOrdini();

		if(x.size()==0)
			return 1;
		else
			return x.get(x.size()-1).getIdOrdine()+1;
	}

	public boolean evadiOrdine(int idOrdine, int idMagazzino){
		Ordine ord = new Ordine();
		return ord.evadiOrdine(idOrdine, idMagazzino);		
	}
	public ArrayList<String> getElencoMagazziniInteressati(ArrayList<Contenuto> cont){

		ArrayList<String> mag = new ArrayList<String>();
		//    	aggiungo i nomi all'arraylist
		for (Iterator iterator = cont.iterator(); iterator.hasNext();) {
			Contenuto riga = (Contenuto) iterator.next();
			mag.add(MagazzinoBusiness.getInstance().caricaDatiMagazzino(riga.getIdMag()).getNome());		
		}
		Set<String> elencoSenzaDuplicati = new HashSet<>();
		elencoSenzaDuplicati.addAll(mag);
		mag.clear();
		mag.addAll(elencoSenzaDuplicati);

		return mag;
	}

	public ArrayList<Ordine> cercaData(String data){
		Ordine o = new Ordine();
		return o.cercaData(data);
	}
	public  boolean modificaData(String nuovaNota, int idOrdine){
		Ordine o = new Ordine();
		return o.modificaNota(nuovaNota, idOrdine);
	}
	public ArrayList<Ordine> caricaElencoOrdiniPerCapoProgetto(int idCapo){
		Ordine o = new Ordine();
		return o.caricaordiniPerCapoProgetto(idCapo);
	}
	public int controlloSuQuantita(int idArticolo, String data){
		Ordine o = new Ordine();
		return o.controlloSuQuantita(idArticolo, data);
	}


}
