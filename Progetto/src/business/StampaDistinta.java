package business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import model.Contenuto;
import model.Ordine;
import model.Utente;

public class StampaDistinta {
	private static StampaDistinta instance;

	//SINGLETON
	public static synchronized StampaDistinta getInstance(){	
		if(instance == null)
			instance = new StampaDistinta();
		return instance;
	}
	public void stampaDistinta(Utente u, Ordine o, ArrayList<Contenuto> c, ArrayList<String> magazzini, String nomeDistinta, JFrame finestra) throws IOException{
		
		String outputFileName = nomeDistinta;
		// Create a document and add a page to it

		// Create a new font object selecting one of the PDF base fonts
		PDFont grassetto = PDType1Font.HELVETICA_BOLD;
		PDFont normale = PDType1Font.HELVETICA;
		PDFont corsivo = PDType1Font.HELVETICA_OBLIQUE;
		
		PDDocument document = new PDDocument();
		PDPage page;
		PDRectangle rect;
		PDPageContentStream cos;
		int line;
		for (Iterator iterator = magazzini.iterator(); iterator.hasNext();) {
			String mag = (String) iterator.next();
			
			page = new PDPage(PDRectangle.A4);
			// PDRectangle.LETTER and others are also possible
			rect = page.getMediaBox();
			// rect can be used to get the page width and height
			document.addPage(page);


			// Start a new content stream which will "hold" the to be created content
			cos = new PDPageContentStream(document, page);

			line = 0;

			// Define a text content stream using the selected font, move the cursor and draw some text
			cos.beginText();
			cos.setFont(grassetto, 25);
			cos.newLineAtOffset(rect.getWidth()/2 - 160, rect.getHeight() - 30*(++line));
			cos.showText("DISTINTA DI PAGAMENTO");
			cos.endText();

			cos.beginText();
			cos.setFont(corsivo, 20);
			cos.newLineAtOffset(30, rect.getHeight() - 30*(++line));
			cos.showText("DATI UTENTE");
			cos.endText();
			
			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 26*(++line));
			cos.showText("- Nome e cognome: "+u.getNome()+" "+u.getCognome()+"");
			cos.endText();
			
			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 24*(++line));
			cos.showText("- Codice Utente: "+u.getCodice_utente()+"");
			cos.endText();
			
			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 23*(++line));
			cos.showText("- Sede: "+Sessione.getInstance().getUtenteLoggato().getSede());
			cos.endText();

			cos.beginText();
			cos.setFont(corsivo, 20);
			cos.newLineAtOffset(30, rect.getHeight() - 23*(++line));
			cos.showText("DATI ORDINE");
			cos.endText();
			
			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 22*(++line));
			cos.showText("- Data: "+ o.getData()+"");
			cos.endText();

			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 22*(++line));
			cos.showText("- Numero ordine: "+o.getIdOrdine()+"");
			cos.endText();

			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 22*(++line));
			cos.showText("- Magazzino: "+mag);
			cos.endText();

			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 22*(++line));
			cos.showText("- Progetto associato: "+ ProgettoBusiness.getInstance().caricaNomeProgetto(o.getProgetto()));
			cos.endText();

			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 22*(++line));
			cos.showText("- Note: "+o.getNote());
			cos.endText();
			
			cos.beginText();
			cos.setFont(corsivo, 20);
			cos.newLineAtOffset(30, rect.getHeight() - 23*(++line));
			cos.showText("CONTENUTO ORDINE");
			cos.endText();
			//creare ciclo su contenuto per inserire tutto il contenuto
			for (Iterator iterator2 = c.iterator(); iterator2.hasNext();) {
				Contenuto rigaDiContenuto = (Contenuto) iterator2.next();
				if(MagazzinoBusiness.getInstance().caricaDatiMagazzino(rigaDiContenuto.getIdMag()).getNome().equals(mag)){

					cos.beginText();
					cos.setFont(normale, 12);
					cos.newLineAtOffset(30, rect.getHeight() - 23*(++line));
					cos.showText("- n� "+rigaDiContenuto.getQuantita()+" -- "+rigaDiContenuto.getNome()+";  Costo unitario (EUR): "+rigaDiContenuto.getPrezzo());
					cos.endText();
				}
				
			}
			
			cos.beginText();
			cos.setFont(normale, 12);
			cos.newLineAtOffset(30, rect.getHeight() - 22*(++line));
			cos.showText("__________________________________________________________________");
			cos.endText();
			
			cos.beginText();
			cos.setFont(normale, 15);
			cos.newLineAtOffset(rect.getWidth() - 230, rect.getHeight() - 23*(++line));
			cos.showText("Spedizione (EUR): "+o.getSpedizione());
			cos.endText();
			
			cos.beginText();
			cos.setFont(normale, 17);
			cos.newLineAtOffset(rect.getWidth() - 230, rect.getHeight() - 22*(++line));
			cos.showText("_______________________");
			cos.endText();
			
			cos.beginText();
			cos.setFont(grassetto, 15);
			cos.newLineAtOffset(rect.getWidth() - 230, rect.getHeight() - 22*(++line));
			cos.showText("Totale (EUR): "+o.getCosto_totale());
			cos.endText();
			
			// Make sure that the content stream is closed:
			cos.close();
			
		}
		
		// Save the results and ensure that the document is properly closed:
		document.save(outputFileName);
		document.close();
		
		JOptionPane.showMessageDialog(finestra, "Distinta di pagamento salvata in:\n"+outputFileName+".");

	}
}
