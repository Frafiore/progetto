package business;

import java.util.ArrayList;

import dao.UtenteDAO;
import model.Utente;

public class UtenteBusiness {

	private static UtenteBusiness instance;
	
	//SINGLETON
	public static synchronized UtenteBusiness getInstance(){	
		if(instance == null)
			instance = new UtenteBusiness();
		return instance;
	}
	
	public boolean verificaLogin(int codiceUtente, String password)
	{
		Utente p = new Utente(codiceUtente,password);
		return p.login();
	}
	
	public String getEmaiFromDataBase(int codiceUtente){
		
		Utente u = new Utente(codiceUtente);
		return u.getEmailFromDB();
	}
	public boolean changePassword(int codiceUtente, int idUtente, String newPwd){
		
		Utente u = new Utente(codiceUtente);
		return u.setPassword(newPwd, idUtente);
		
	}
	public int getIdFromDataBase(int codiceUtente){
		
		Utente u = new Utente(codiceUtente);
		return u.getIdFromDB();
	}
	public Utente caricaUtente(int idUtente){
		Utente u = new Utente(idUtente, 3, false);
		return u.caricaDatiUtente();
	}
	public ArrayList<Utente> caricaElencoDipendenti(int idCapo){
		Utente u = new Utente(1);
		return u.caricaElencoDipendenti(idCapo);
	}
}
