package business;


import dao.UtenteDAO;
import model.Utente;


public class Sessione {
	
	private static Sessione instance;
	
	
	public static synchronized Sessione getInstance(){
	
		if(instance==null)
		{	
			instance = new Sessione();
		}
		return instance;
	}
	
	Utente utenteLoggato;
	
	public void creaSessione(int codice_utente, String password){
		String[] dati_utente = UtenteDAO.getInstance().getUtenteLoggato(codice_utente,password);
		utenteLoggato = new Utente(dati_utente);
	}

	public Utente getUtenteLoggato() {
		return utenteLoggato;
	}
	
}
