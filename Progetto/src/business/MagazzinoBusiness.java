package business;

import java.util.ArrayList;
import model.Magazzino;

public class MagazzinoBusiness {
	private static MagazzinoBusiness instance;

	//SINGLETON
	public static synchronized MagazzinoBusiness getInstance(){	
		if(instance == null)
			instance = new MagazzinoBusiness();
		return instance;
	}
	public Magazzino caricaDatiMagazzino(int idMag){
		Magazzino m = new Magazzino();
		return m.caricaDatiMag(idMag);
	}
	public int caricaIdMag (String nome){
		Magazzino m = new Magazzino();
		return m.caricaIdMagazzino(nome);
	}
	public ArrayList<Magazzino> caricaElencoMagazzini(){
		Magazzino m = new Magazzino();
		return m.caricaElencoMagazzini();
	}
}
