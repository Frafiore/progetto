package business;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
/**
 * @author Crunchify.com
 * 
 */
 
public class EmailManager {
 
	private static EmailManager instance;
	
	//SINGLETON
	public static synchronized EmailManager getInstance(){
	
		if(instance == null)
			instance = new EmailManager();
		return instance;
	}
	
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	
	private String destinatario;
	private String testo;
	private boolean invioEseguito = false;
 
	public boolean sendEmail(String destinatario, String oggetto, String testo) throws AddressException, MessagingException {
		
		this.destinatario = destinatario;
		this.testo = testo;
		// Step1
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");
 
		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
		generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("progetto.alffra@gmail.com"));
		generateMailMessage.setSubject(oggetto);
		
		generateMailMessage.setContent(testo, "text/html");
		System.out.println("Mail Session has been created successfully..");
 
		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");
 
		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		transport.connect("smtp.gmail.com", "progetto.alffra@gmail.com", "alfonsofrancesco");
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
		this.invioEseguito = true;
		
		return invioEseguito;
	}
	
	
}
