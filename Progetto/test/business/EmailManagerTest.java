package business;

import static org.junit.Assert.*;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.junit.Before;
import org.junit.Test;

import business.EmailManager;

public class EmailManagerTest {

	EmailManager emailM;
	boolean result;
	
	@Before
	public void init(){
		emailM = new EmailManager();
		result = false;
	}
	
	@Test
	public void testSendEmail() throws AddressException, MessagingException {
		assertTrue(emailM.sendEmail("francy.fiore99@gmail.com", "ciao","58225"));
	}

}
